//
//  FeedbackVC.swift
//  CHOOCH
//
//  Created by des on 20/12/18.
//  Copyright © 2018 Vasundhara Vision. All rights reserved.
//

import UIKit
import AVFoundation
import AFNetworking
import BSImagePicker
import Photos
import ReverseExtension
import Pulsator
import FacebookLogin
import FBSDKLoginKit
import FacebookCore
import GoogleSignIn


class FeedbackVC: UIViewController ,UIImagePickerControllerDelegate ,UINavigationControllerDelegate ,UITextViewDelegate , UITextFieldDelegate , UITableViewDelegate , UITableViewDataSource {
    
    var Backimage = UIImage()
    var isDense = false
    
    //    var Predictarr = NSMutableArray()
    
    var ImagesArr = [UIImage]()
    var imageCutoutArr = [UIImage]()
    
    var isCapture = false
    var isCallApi = false
    
    
    @IBOutlet var CameraBtn: UIButton!
    
    @IBOutlet var GallaryBtn: UIButton!
    
    
    @IBOutlet var VideoView: UIView!
    
    @IBOutlet var SelectedImgView: UIImageView!
    @IBOutlet var blackshadowImgView: UIImageView!
    
    @IBOutlet var Intelligency_Scrollview: UIScrollView!
    @IBOutlet var InteligencyScrollHeightConst: NSLayoutConstraint!
    
    //    @IBOutlet var Feedback_MainView: UIView!
    
    @IBOutlet var LoaderView: UIView!
    
    @IBOutlet var TypingView: UIView!
    
    @IBOutlet var LoaderScreen_TitleLbl: UILabel!
    @IBOutlet var RecordingLbl: UILabel!
    
    //    @IBOutlet var Loader_ImgView: UIImageView!
    
    @IBOutlet var tblviewSuprView: UIView!
    @IBOutlet weak var tblChat: UITableView!
    @IBOutlet weak var txtTyping: UITextField!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var borderView: UIView!
    
    @IBOutlet var FeedbackDone_SuperView: UIView!
    
    @IBOutlet var tabicon_flash: UIImageView!
    @IBOutlet var tabicon_camera: UIImageView!
    @IBOutlet var tabicon_garbage: UIImageView!
    @IBOutlet var tabicon_feedback: UIImageView!
    @IBOutlet var tabicon_logout: UIImageView!
    
    @IBOutlet var TypingImgView: UIImageView!
    @IBOutlet var video_3dLoader: UIImageView!
    
    //    @IBOutlet weak var captureButton : SwiftyRecordButton!
    
    var cutoutTitle = String()
    var cutoutAnyotherStr = String()
    
    var ObjectTitle = String()
    var OtherfeedbackText = String()
    
    var videoCapture: VideoCapture!
    var device: MTLDevice!
    var commandQueue: MTLCommandQueue!
    
    var flashView = UIView()
    var chatMessageArray = [[String:Any]]()
    
    var zipPath: String?
    
    var whatsthisFlag = false
    var addvideoFlag = false
    var addotherPhotosFlag = false
    var anyfeedbackfornoInteligencyFlag = false
    var cutoutInteligencyFlag = false
    var anyotherFeedbackFlag = false
    var cutoutsFlag = false
    var cutoutVideoFlag = false
    var cutoutImageFlag = false
    var cutoutAskingTypeFlag = false
    var cutoutAnyOtherFlag = false
    var addAnyotherfeedbackFlag = false
    
    var QueTypeStr = String()
    
    var viewforScroll = UIView()
    
    var timer1 : Timer!
    
    var inteligencyScrollArr = NSMutableArray()
    
    var cutoutsArr = NSMutableArray()
    var cutoutIndex = 0
    
    var coordinatesArr = NSMutableArray()
    var isCallCutoutApi = false
    
    var imageCutout = UIImage()
    var DictCutout = NSDictionary()
    
    let myAttribute = [ NSAttributedString.Key.font: UIFont.init(name: CustomFontWeight.reguler, size: 20)!]
    let myAttributeBold = [ NSAttributedString.Key.font: UIFont.init(name: CustomFontWeight.bold, size: 20)!]
    
    let userId = "\(userDefault.value(forKey: custId) ?? "-1")"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SelectedImgView.image = Backimage
        ImagesArr.append(Backimage)
        
        self.LoaderView.isHidden = true
        //        self.Loader_ImgView.image = UIImage.gifImageWithName("Infinity-1")
        
        self.video_3dLoader.image = UIImage.gifImageWithName("mobile")
        
        self.TypingImgView.image = UIImage.gifImageWithName("dots")
        self.TypingView.isHidden = true
        
        VideoView.isHidden = true
        tblviewSuprView.isHidden = false
        
        device = MTLCreateSystemDefaultDevice()
        if device == nil {
            print("Error: this device does not support Metal")
            return
        }
        
        commandQueue = device.makeCommandQueue()
        
        videoCapture = VideoCapture(device: device)
        
        videoCapture.delegate = self
        
        videoCapture.setUp { success in
            // Add the video preview into the UI.
            if let previewLayer = self.videoCapture.previewLayer {
                self.VideoView.layer.addSublayer(previewLayer)
                self.resizePreviewLayer()
            }
        }
        
        flashView.frame = LoaderView.frame
        flashView.alpha = 0
        flashView.backgroundColor = UIColor.white
        LoaderView.insertSubview(flashView, belowSubview: GallaryBtn)
        
        Initialize()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ReloadIntelligecyScroll), name: NSNotification.Name(rawValue: "ReloadIntelligency"), object: nil)
        
        RecordingLbl.superview!.isHidden = true
        
        //        shouldPrompToAppSettings = true
        //        cameraDelegate = self
        //        maximumVideoDuration = 5.0
        //        shouldUseDeviceOrientation = true
        //        allowAutoRotate = true
        //        audioEnabled = false
        //        flashMode = .auto
        //        captureButton.buttonEnabled = false
        
    }
    
    func Initialize()
    {
        tblChat.re.dataSource = self
        
        let cellNIB = UINib(nibName: "tblChatReceiverCell", bundle: nil)
        tblChat.register(cellNIB, forCellReuseIdentifier: "tblChatReceiverCell")
        
        let cellNIB2 = UINib(nibName: "tblChatSenderCell", bundle: nil)
        tblChat.register(cellNIB2, forCellReuseIdentifier: "tblChatSenderCell")
        
        tblChat.re.delegate = self
        
        tblChat.re.scrollViewDidReachTop = { scrollView in
            //            print("scrollViewDidReachTop")
        }
        tblChat.re.scrollViewDidReachBottom = { scrollView in
            //            print("scrollViewDidReachBottom")
        }
        
        txtTyping.delegate = self
        txtTyping.enablesReturnKeyAutomatically = false
        txtTyping.layer.cornerRadius = 15.0
        txtTyping.layer.borderColor = UIColor.gray.cgColor
        txtTyping.layer.borderWidth = 1.0
        txtTyping.setLeftPaddingPoints(10)
        
        InitializePrimaryArray(isUser: false)
        
        SetInteligencyScrollView()
        
    }
    
    func SetInteligencyScrollView() {
        
        var x = 50
        
        for i in viewforScroll.subviews
        {
            i.removeFromSuperview()
        }
        
        if (!cutoutVideoFlag || !cutoutImageFlag) && predictionArray.count > 0 {
            predictionArray.removeAllObjects()
            predictionArray.add("Yes")
            predictionArray.add("No")
        }
        else if cutoutAnyOtherFlag
        {
            predictionArray.removeAllObjects()
            predictionArray.add("No")
        }
        else if cutoutImageFlag && cutoutVideoFlag
        {
            predictionArray.removeAllObjects()
        }
        
        
        if !cutoutAskingTypeFlag && predictionArray.count > 0 {
            predictionArray.removeAllObjects()
            predictionArray.add("Face")
            predictionArray.add("Object")
            predictionArray.add("Both")
        }
        else if cutoutInteligencyFlag && !whatsthisFlag
        {
            predictionArray.removeAllObjects()
            predictionArray = cutoutsArr.mutableCopy() as? NSMutableArray ?? NSMutableArray()
            let dict = ["classvalue" : "Skip"]
            predictionArray.insert(dict, at: 0)
            let dict1 = ["classvalue" : "Done"]
            predictionArray.insert(dict1, at: 1)
            for i in 0..<inteligencyScrollArr.count
            {
                let dict2 = ["classvalue" : "\(inteligencyScrollArr[i])"]
                predictionArray.insert(dict2, at: (i+2))
            }
            
        }
        else if cutoutsFlag && !whatsthisFlag
        {
            predictionArray.removeAllObjects()
            predictionArray = cutoutsArr.mutableCopy() as? NSMutableArray ?? NSMutableArray()
            let dict = ["classvalue" : "Skip"]
            predictionArray.insert(dict, at: 0)
            let dict1 = ["classvalue" : "Done"]
            predictionArray.insert(dict1, at: 1)
            for i in 0..<inteligencyScrollArr.count
            {
                let dict2 = ["classvalue" : "\(inteligencyScrollArr[i])"]
                predictionArray.insert(dict2, at: (i+2))
            }
        }
        else if whatsthisFlag {
            predictionArray.removeAllObjects()
            predictionArray.add("Yes")
            predictionArray.add("No")
            
            if addotherPhotosFlag
            {
                predictionArray.removeAllObjects()
                if anyfeedbackfornoInteligencyFlag && !anyotherFeedbackFlag
                {
                    predictionArray.add("No")
                }
            }
            
        }
        
        let imgchooch = UIImageView(image: UIImage.init(named: "logo"))
        imgchooch.frame = CGRect(x: 10, y: 5, width: 30, height: 30)
        imgchooch.contentMode = .scaleAspectFit
        imgchooch.backgroundColor = .clear
        viewforScroll.addSubview(imgchooch)
        
        if predictionArray.count != 0 && viewforScroll.subviews.count <= 1 {
            if predictionArray[0] is NSDictionary
            {
                let Predcount = predictionArray.count// < 5 ? predictionArray.count : 5
                
                for predic in 0..<Predcount {
                    
                    let title = String(describing: (predictionArray.object(at: predic) as AnyObject).value(forKey: "classvalue")!)
                    
                    let array = title.components(separatedBy: ",")
                    
                    if array.count > 1
                    {
                        for j in 0..<array.count
                        {
                            let btn = MyButton.init(frame: CGRect(x: x, y: 5, width: 0, height: 30))
                            
                            btn.setTitle("  \(array[j])  ", for: .normal)
                            
                            btn.isUserInteractionEnabled = true
                            
                            btn.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
                            btn.setTitleColor(UIColor.black, for: .normal)
                            btn.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 15)
                            btn.layer.cornerRadius = btn.Getheight / 2
                            
                            btn.layer.borderColor = UIColor.gray.cgColor
                            btn.layer.borderWidth = 1.0
                            
                            btn.sizeToFit()
                            
                            btn.addTarget(self, action: #selector(self.BtnPressed), for: .touchUpInside)
                            
                            x += Int(btn.Getwidth) + 10
                            
                            viewforScroll.addSubview(btn)
                        }
                    }
                    else
                    {
                        let btn = MyButton.init(frame: CGRect(x: x, y: 5, width: 0, height: 30))
                        
                        btn.setTitle("  \(title)  ", for: .normal)
                        
                        btn.isUserInteractionEnabled = true
                        
                        btn.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
                        btn.setTitleColor(UIColor.black, for: .normal)
                        
                        if btn.titleLabel?.text?.trim().lowercased() == "skip" || btn.titleLabel?.text?.trim().lowercased() == "done"
                        {
                            btn.backgroundColor = UIColor(red: 0, green: 0, blue: 1, alpha: 0.5)
                            btn.setTitleColor(.white, for: .normal)
                        }
                        
                        btn.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 15)
                        btn.layer.cornerRadius = btn.Getheight / 2
                        
                        btn.layer.borderColor = UIColor.gray.cgColor
                        btn.layer.borderWidth = 1.0
                        
                        btn.sizeToFit()
                        
                        btn.addTarget(self, action: #selector(self.BtnPressed), for: .touchUpInside)
                        
                        x += Int(btn.Getwidth) + 10
                        
                        viewforScroll.addSubview(btn)
                    }
                    
                }
            }
            else
            {
                for j in 0..<predictionArray.count
                {
                    let btn = MyButton.init(frame: CGRect(x: x, y: 5, width: 0, height: 30))
                    
                    btn.setTitle("  \(predictionArray[j] as? String ?? "")  ", for: .normal)
                    
                    btn.isUserInteractionEnabled = true
                    
                    btn.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
                    btn.setTitleColor(UIColor.black, for: .normal)
                    
                    if btn.titleLabel?.text?.trim().lowercased() == "skip" || btn.titleLabel?.text?.trim().lowercased() == "done"
                    {
                        btn.backgroundColor = UIColor(red: 0, green: 0, blue: 1, alpha: 0.5)
                        btn.setTitleColor(.white, for: .normal)
                    }
                    
                    btn.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 15)
                    btn.layer.cornerRadius = btn.Getheight / 2
                    
                    btn.layer.borderColor = UIColor.gray.cgColor
                    btn.layer.borderWidth = 1.0
                    
                    btn.sizeToFit()
                    
                    btn.addTarget(self, action: #selector(self.BtnPressed), for: .touchUpInside)
                    
                    x += Int(btn.Getwidth) + 10
                    
                    viewforScroll.addSubview(btn)
                }
                
            }
            
        }
        
        viewforScroll.frame = CGRect(x: 0.0, y: 0.0, width: CGFloat(x), height: 40.0)
        
        Intelligency_Scrollview.addSubview(viewforScroll)
        
        Intelligency_Scrollview.contentSize = viewforScroll.Getsize
        
        self.InteligencyScrollHeightConst.constant = viewforScroll.subviews.count > 1 ? 40 : 0
        self.view.layoutIfNeeded()
        
        
        
    }
    
    
    @objc func ReloadIntelligecyScroll()
    {
        if cutoutsArr.count <= 0 {
            cutoutsArr = predictionArray.mutableCopy() as? NSMutableArray ?? NSMutableArray()
        }
        cutoutInteligencyFlag = false
        
        
        if userDefault.value(forKey: "firsttimeFeedback") == nil
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                //                if self.chatMessageArray.count <= 1 {
                self.addcutOutsToTableview(index: self.cutoutIndex, isAnswered: self.isCallCutoutApi)
                userDefault.set(true, forKey: "firsttimeFeedback")
                //                }
            }
        }
        else
        {
            //            if chatMessageArray.count <= 0
            //            {
            self.addcutOutsToTableview(index: self.cutoutIndex, isAnswered: self.isCallCutoutApi)
            //            }
        }
        
    }
    
    
    @objc func BtnPressed(_ sender: MyButton) {
        
        txtTyping.text = sender.titleLabel!.text!.chopPrefix(2).chopSuffix(2)
        btnSend(sender)
        
    }
    
    func InitializePrimaryArray(isUser : Bool) {
        
        chatMessageArray.removeAll()
        
        self.TypingView.isHidden = false
        DispatchQueue.main.async {
            if userDefault.value(forKey: "firsttimeFeedback") == nil
            {
                self.AddToArrayMessage(msg: "Hi! Welcome to training feedback for Chooch! Here I will ask you questions about your most recent screen capture. Let’s start!", isUser: isUser)
            }
        }
        
    }
    
    func AddToArrayMessage(msg : String , isUser : Bool) {
        
        var tempDict = [String : Any]()
        
        tempDict["message"] = msg
        tempDict["isUser"] = isUser
        
        self.TypingView.isHidden = isUser ? false : true
        if !isUser
        {
            self.txtTyping.placeholder = msg
        }
        
        chatMessageArray.append(tempDict)
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
        //        if self.chatMessageArray.count != 1 {
        self.tblChat.beginUpdates()
        self.tblChat.re.insertRows(at: [IndexPath(row: self.chatMessageArray.count - 1, section: 0)], with: .automatic)
        self.tblChat.endUpdates()
        //        }
        //        }
        
        
    }
    
    func AddToArrayMessage(Attrmsg : NSAttributedString , isUser : Bool , isTransparentBack : Bool) {
        
        var tempDict = [String : Any]()
        
        tempDict["message"] = Attrmsg
        tempDict["isUser"] = isUser
        tempDict["isTransparent"] = isTransparentBack
        
        self.TypingView.isHidden = isUser ? false : true
        
        if !isUser
        {
            self.txtTyping.placeholder = Attrmsg.string
        }
        
        chatMessageArray.append(tempDict)
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
        //        if self.chatMessageArray.count != 1 {
        self.tblChat.beginUpdates()
        self.tblChat.re.insertRows(at: [IndexPath(row: self.chatMessageArray.count - 1, section: 0)], with: .automatic)
        self.tblChat.endUpdates()
        //        }
        
        //            self.tblChat.reloadData()
        //        }
        
        
    }
    
    func resizePreviewLayer() {
        
        videoCapture.previewLayer?.frame = VideoView.bounds
        
    }
    
    
    
    func CallFeedBackApi(IndexRow : Int) {
        
        tblviewSuprView.isHidden = false
        LoaderView.isHidden = false
        LoaderScreen_TitleLbl.superview!.isHidden = true
        RecordingLbl.superview!.isHidden = true
        FeedbackDone_SuperView.isHidden = true
        //        http://ec2-54-196-168-80.compute-1.amazonaws.com:8000/genapi/create_feedback?label_title=sdfds&comment=sdasd&device_id=123&apikey=12312
        if !inteligencyScrollArr.contains(ObjectTitle)
        {
            inteligencyScrollArr.add(ObjectTitle)
        }
        
        let lbl_title = "\(userId)-\(ObjectTitle)"//.stringByAddingPercentEncodingForRFC3986()!
        let strURL2 = "http://api.chooch.ai/genapi/feedback?label_title=\(lbl_title)&comment=\(OtherfeedbackText)&device_id=\(UIDevice.current.identifierForVendor!.uuidString)&apikey=\(apikey)"
        
        //        https://api.chooch.ai/genapi/feedback?label_title=&comment={commment}&device_id={device_id}&apikey=69c8f610-76b5-451e-81d6-3d92862d18d8
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: .allowFragments)
        manager.responseSerializer.acceptableContentTypes = ["text/html","application/json","text/plain"]
        
        let strQue = strURL2.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        //        let strQue = strURL2.stringByAddingPercentEncodingForRFC3986()
        
        manager.requestSerializer.timeoutInterval = 500.0
        manager.post(strQue!, parameters: nil, constructingBodyWith: { (formData) in
            
            let Image = self.ImagesArr[IndexRow]
            let imagedata = UIImageJPEGRepresentation(Image.compressImage()!, 1.0)
            
            formData.appendPart(withFileData: imagedata!, name: "image", fileName: "Image", mimeType: "image/jpeg")
            
        }, progress: { (progress123) in
            
            print("Progress is : \(progress123.fractionCompleted)")
            
            DispatchQueue.main.async {
                let indexPAth = IndexPath(row: self.chatMessageArray.count - 1, section: 0)
                let cell = self.tblChat.re.cellForRow(at: indexPAth) as! tblChatReceiverCell
                
                let fraction = progress123.fractionCompleted
                let ImagesArrCount = Double(self.ImagesArr.count)
                
                cell.PercentageLbl.text = "\(Int(((fraction) / ImagesArrCount * 100) + (100 / ImagesArrCount * Double(IndexRow))))%  "
                
                
            }
            
            
        }, success: { (task, responseObject) in
            
            if IndexRow < (self.ImagesArr.count - 1)
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.CallFeedBackApi(IndexRow: IndexRow+1)
                })
            }
            else
            {
                
                if responseObject == nil
                {
                    return
                }
                let predct = responseObject as! NSDictionary
                
                print(predct)
                
                if predct.value(forKey: "status") as! String == "ok"
                {
                    self.tblviewSuprView.isHidden = false
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        
                        if self.isCallApi {
                            startSession = false
                            self.dismiss(animated: true, completion: nil)
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
                        }
                        
                    }
                    
                    self.ImagesArr.removeAll()
                    
                }
            }
            
        }, failure: { (task, error) in
            print(error)
            
            if IndexRow < (self.ImagesArr.count - 1)
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.CallFeedBackApi(IndexRow: IndexRow+1)
                })
            }
            else
            {
                
                let alert = UIAlertController.init(title: "Poor Connection", message: "Please check your internet connection", preferredStyle: .alert)
                
                let ok = UIAlertAction.init(title: "OK", style: .cancel, handler: { (Action) in
                    startSession = false
                    self.dismiss(animated: true, completion: nil)
                })
                
                alert.addAction(ok)
                self.present(alert, animated: true, completion: nil)
                
                print(error)
                
            }
        })
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        //        super.viewDidAppear(animated)
        
        flashView.frame = LoaderView.frame
        //         captureButton.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
    }
    
    @objc func keyboardWillAppear(notification: NSNotification) {
        //Do something here
        
    }
    
    @objc func keyboardWillDisappear(notification: NSNotification) {
        //Do something here
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        self.viewforScroll.removeFromSuperview()
        
        NotificationCenter.default.removeObserver(self)
        
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        
        flashView.frame = LoaderView.frame
        
        self.VideoView.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height - 49)
        self.resizePreviewLayer()
        videoCapture.changeorietation(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height - 49))
        
    }
    
    
    func isReadyToSendComplain()-> Bool {
        print("a\(txtTyping.text ?? "")b")
        if txtTyping.text == "" || txtTyping.text == nil || txtTyping.text!.trim() == ""
        {
            
        }
        else{
            
            return true
        }
        
        return false
    }
    
    func Yesbtn_Pressed() {
        
        videoCapture.start()
        VideoView.isHidden = false
        
        tblviewSuprView.isHidden = true
        
        LoaderView.isHidden = false
        FeedbackDone_SuperView.isHidden = false
        
        VideoView.isHidden = false
        
        imageCutoutArr.removeAll()
        
        if addvideoFlag || cutoutImageFlag
        {
            CameraBtn.setImage(UIImage(named: "ic_camera"), for: .normal)
            GallaryBtn.isHidden = false
            video_3dLoader.isHidden = true
            RecordingLbl.superview!.isHidden = true
        }
        else
        {
            CameraBtn.setImage(UIImage(named: "recording-icon"), for: .normal)
            GallaryBtn.isHidden = true
            video_3dLoader.isHidden = false
            LoaderScreen_TitleLbl.text = "Take from different angles for best results"
            
            //            let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(LongPress(_:)))
            //            CameraBtn.addGestureRecognizer(longGesture)
            
        }
        
        CheckFlash()
        
        
    }
    
    @IBAction func Camera_Pressed(_ sender: UIButton) {
        
        if addvideoFlag || cutoutImageFlag {
            
            UIView.animate(withDuration: 0.1, delay: 0, options: .autoreverse, animations: {
                self.flashView.alpha = 1
            }) { (y) in
                self.flashView.alpha = 0
            }
            
            isCapture = true
            
        }
        else if !addvideoFlag || !cutoutVideoFlag {
            
            if CameraBtn.imageView?.image == UIImage(named: "recording-icon")
            {
                counter1 = 0.0
                timercount = 0.0
                self.timer1 =  Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.ClickImg(timer:)), userInfo: nil, repeats: true)
                
                RecordingLbl.superview!.isHidden = false
                CameraBtn.setImage(UIImage(named: "stop-recording"), for: .normal)
                print("Begin")
            }
            else
            {
                self.timer1.invalidate()
                RecordingLbl.superview!.isHidden = true
                CameraBtn.setImage(UIImage(named: "recording-icon"), for: .normal)
                print("ended")
            }
        }
        
        
        CheckFlash()
        
    }
    
    
    @IBAction func Gallary_Pick_Pressed(_ sender: UIButton) {
        
        videoCapture.stop()
        VideoView.isHidden = true
        
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization({status in
                if status == .authorized{
                    
                } else {
                    self.presentAlertWithTitle(title: "Attention", message: "Gallary access is required for add photos to feedback ", options: "Go to settings", completion: { (IndexAction) in
                        if IndexAction == 0
                        {
                            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                        }
                    })
                }
                
            })
        }
        
        let vc = BSImagePickerViewController()
        vc.maxNumberOfSelections = 6
        
        bs_presentImagePickerController(vc, animated: true,
                                        select: { (asset: PHAsset) -> Void in
                                            print("Selected: \(asset)")
        }, deselect: { (asset: PHAsset) -> Void in
            print("Deselected: \(asset)")
        }, cancel: { (assets: [PHAsset]) -> Void in
            
            DispatchQueue.main.async {
                self.videoCapture.start()
                self.VideoView.isHidden = false
            }
            
            print("Cancel: \(assets)")
        }, finish: { (assets: [PHAsset]) -> Void in
            print("Finish: \(assets)")
            
            DispatchQueue.main.async {
                
                for imageAsset in assets
                {
                    let Image123 = self.getUIImage(asset: imageAsset)
                    
                    if self.cutoutsFlag
                    {
                        self.ImagesArr.append((Image123!).compressImage()!)
                        
                    }
                    else
                    {
                        self.imageCutoutArr.append((Image123!).compressImage()!)
                    }
                    
                }
                
                self.LoaderView.isHidden = true
                
                let btn = UIButton()
                self.LoaderDone_Pressed(btn)
                
            }
            
            
        }, completion: nil)
        
        CheckFlash()
        
    }
    
    @IBAction func Nobtn_Pressed(_ sender: UIButton) {
        
        LoaderView.isHidden = true
        
        
        CheckFlash()
        
    }
    
    //    @IBAction func WhatsThisNext_Pressed(_ sender: UIButton) {
    //
    //        self.view.endEditing(true)
    //
    //        ObjectTitle = ObjectTitleTextField.text!
    //
    //        if (ObjectTitleTextField.text!.count) <= 0
    //        {
    //            ObjectTitleTextField.attributedPlaceholder = NSAttributedString(string: "*This is required!",
    //                                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor.red])
    //            return
    //        }
    //
    //        let myAttribute = [ NSAttributedString.Key.font: UIFont.init(name: CustomFontWeight.reguler, size: 21)!]
    //        let myAttributeBold = [ NSAttributedString.Key.font: UIFont.init(name: CustomFontWeight.bold, size: 21)!]
    //
    //        LoaderScreen_TitleLbl.superview!.backgroundColor = hexStringToUIColor(hex: "B7B7B7").withAlphaComponent(0.7)
    //        let myString2 = NSMutableAttributedString(string: "Adding Images for ", attributes: myAttribute)
    //        myString2.append(NSMutableAttributedString(string: "''\(ObjectTitle)''", attributes: myAttributeBold))
    //        myString2.append(NSMutableAttributedString(string: "...", attributes: myAttribute))
    //
    //        LoaderScreen_TitleLbl.attributedText = myString2
    ////        LoaderScreen_TitleLbl.text = "'\(ObjectTitle)' ..."
    ////        would_you_Lbl.text = "Would you like to add more image of '\(ObjectTitle)'?"
    //
    //        let myString = NSMutableAttributedString(string: "Would you like to add more image of ", attributes: myAttribute)
    //        myString.append(NSMutableAttributedString(string: "''\(ObjectTitle)''", attributes: myAttributeBold))
    //        myString.append(NSMutableAttributedString(string: "?", attributes: myAttribute))
    //
    //        would_you_Lbl.attributedText = myString
    //
    //
    //        CheckFlash()
    //
    //    }
    //
    
    @IBAction func OtherFeedbackNext_Pressed(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        CheckFlash()
        
    }
    
    @IBAction func Feedback_ok_Pressed(_ sender: UIButton) {
        
        
    }
    
    
    @IBAction func LoaderDone_Pressed(_ sender: UIButton) {
        
        if self.timer1 != nil
        {
            self.timer1.invalidate()
        }
        RecordingLbl.superview!.isHidden = true
        CameraBtn.setImage(UIImage(named: "recording-icon"), for: .normal)
        
        //        if isCallApi {
        //            startSession = false
        //            self.dismiss(animated: true, completion: nil)
        //             NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
        //        }
        
        videoCapture.stop()
        VideoView.isHidden = true
        
        LoaderView.isHidden = true
        video_3dLoader.isHidden = true
        
        tblviewSuprView.isHidden = false
        
        if !cutoutsFlag
        {
            // create an NSMutableAttributedString that we'll append everything to
            let fullString = NSMutableAttributedString(string: "")
            
            imageCutoutArr.append(self.imageCutout)
            
            for images in imageCutoutArr {
                
                // create our NSTextAttachment
                
                let image = (images.resize(targetSize: CGSize(width: self.view.Getwidth * 0.1, height: self.view.Getwidth * 0.1))).imageWithBorder(width: 1.0, color: .white, cornorRadius: 10.0)
                
                //            let roundedImg = makeRoundedImage(image, radius: 5)
                
                let image1Attachment = NSTextAttachment.getCenteredImageAttachment(with: image!, and: UIFont.init(name: CustomFontWeight.reguler, size: self.view.Getwidth * 0.1)!)
                
                // wrap the attachment in its own attributed string so we can append it
                let image1String = NSAttributedString(attachment: image1Attachment!)
                
                // add the NSTextAttachment wrapper to our full string, then add some more text.
                fullString.append(image1String)
                
                let myAttributeBold = [ NSAttributedString.Key.font: UIFont.init(name: CustomFontWeight.reguler, size: self.view.Getwidth * 0.1)!]
                
                fullString.append(NSAttributedString(string: " ", attributes: myAttributeBold))
                
            }
            
            
            fullString.deleteCharacters(in: NSRange(location:(fullString.length) - 1,length:1))
            
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.AddToArrayMessage(Attrmsg: fullString, isUser: true , isTransparentBack: true)
                
                if !self.cutoutAnyOtherFlag
                {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.AddToArrayMessage(msg: "Any other feedback?", isUser: false)
                        self.cutoutImageFlag = true
                        self.cutoutVideoFlag = true
                        self.cutoutAnyOtherFlag = true
                        self.SetInteligencyScrollView()
                    }
                }
                else
                {
                    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    
                    self.AddToArrayMessage(msg: "Thank you!", isUser: false)
                    self.TypingView.isHidden = false
                    if !DeviceType.IS_IPHONE_X
                    {
                        self.InteligencyScrollHeightConst.constant = 10.0
                        for view in self.viewforScroll.subviews
                        {
                            view.removeFromSuperview()
                        }
                        self.viewforScroll.backgroundColor = .clear
                        self.Intelligency_Scrollview.backgroundColor = .clear
                        self.view.layoutIfNeeded()
                    }
                    //                    }
                    
                    self.callCutoutAPI(params: self.DictCutout, cutImage: self.imageCutout)
                    
                    self.cutoutAnyOtherFlag = false
                    self.cutoutImageFlag = false
                    self.cutoutVideoFlag = false
                }
            }
        }
        else
        {
            // create an NSMutableAttributedString that we'll append everything to
            let fullString = NSMutableAttributedString(string: "")
            
            for images in ImagesArr {
                
                // create our NSTextAttachment
                
                let image = (images.resize(targetSize: CGSize(width: self.view.Getwidth * 0.1, height: self.view.Getwidth * 0.1))).imageWithBorder(width: 1.0, color: .white, cornorRadius: 10.0)
                
                //            let roundedImg = makeRoundedImage(image, radius: 5)
                
                let image1Attachment = NSTextAttachment.getCenteredImageAttachment(with: image!, and: UIFont.init(name: CustomFontWeight.reguler, size: self.view.Getwidth * 0.1)!)
                
                // wrap the attachment in its own attributed string so we can append it
                let image1String = NSAttributedString(attachment: image1Attachment!)
                
                // add the NSTextAttachment wrapper to our full string, then add some more text.
                fullString.append(image1String)
                
                let myAttributeBold = [ NSAttributedString.Key.font: UIFont.init(name: CustomFontWeight.reguler, size: self.view.Getwidth * 0.1)!]
                
                fullString.append(NSAttributedString(string: " ", attributes: myAttributeBold))
                
            }
            
            
            fullString.deleteCharacters(in: NSRange(location:(fullString.length) - 1,length:1))
            
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                self.AddToArrayMessage(Attrmsg: fullString, isUser: true , isTransparentBack: true)
                
                self.addvideoFlag = true
                self.addotherPhotosFlag = true
                
                self.SetInteligencyScrollView()
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    
                    self.AddToArrayMessage(msg: "Thanks", isUser: false)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.AddToArrayMessage(msg: "Any other feedback?", isUser: false)
                        self.cutoutInteligencyFlag = false
                        self.anyfeedbackfornoInteligencyFlag = true
                        self.SetInteligencyScrollView()
                    }
                    
                    //                self.addcutOutsToTableview(index: self.cutoutIndex, isAnswered: self.isCallCutoutApi)
                    
                }
            }
        }
        
        
        CheckFlash()
        
    }
    
    func callCutoutAPI(params : NSDictionary , cutImage : UIImage) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            
            self.AddToArrayMessage(msg: "Sending images and feedback...", isUser: false)
            
            if self.QueTypeStr.trim().lowercased() == "face" {
                self.callFaceApi(Index: 0)
            }
            else
            {
                if params["face_name"] != nil
                {
                    self.callFaceApi(Index: 0)
                }
                else
                {
                    self.callObjectsApi(ObjectIndex: 0)
                }
            }
        }
        
    }
    
    
    func callObjectsApi(ObjectIndex : Int) {
        
        if !inteligencyScrollArr.contains(cutoutTitle)
        {
            inteligencyScrollArr.add(cutoutTitle)
        }
        
        let lbl_title = "\(userId)-\(cutoutTitle)"//.stringByAddingPercentEncodingForRFC3986()!
        let ApiURL = "http://api.chooch.ai/genapi/feedback?label_title=\(lbl_title)&comment=\(cutoutAnyotherStr)&device_id=\(UIDevice.current.identifierForVendor!.uuidString)&apikey=\(apikey)"
        
        print("call API : \(ApiURL) \(ObjectIndex)")
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: .allowFragments)
        manager.responseSerializer.acceptableContentTypes = ["text/html","application/json","text/plain"]
        
        let strQue = ApiURL.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        //        let strQue = ApiURL.stringByAddingPercentEncodingForRFC3986()
        
        manager.requestSerializer.timeoutInterval = 500.0
        manager.post(strQue!, parameters: nil, constructingBodyWith: { (formData) in
            
            
            let Image = self.imageCutoutArr[ObjectIndex]
            let imagedata = UIImageJPEGRepresentation(Image, 1.0)
            
            formData.appendPart(withFileData: imagedata!, name: "image", fileName: "Image", mimeType: "image/jpeg")
            
        }, progress: { (progress123) in
            
            DispatchQueue.main.async {
                
                print("Progress is : \(progress123.fractionCompleted)")
                
                let fraction = progress123.fractionCompleted
                let ImagesArrCount = Double(self.imageCutoutArr.count)
                
                //                self.percentageCutoutLbl.text = "  Sending your images and feedback...\(Int(((fraction) / ImagesArrCount * 100) + (100 / ImagesArrCount * Double(ObjectIndex))))%  "
                
                let indexPAth = IndexPath(row: self.chatMessageArray.count - 1, section: 0)
                let cell = self.tblChat.re.cellForRow(at: indexPAth) as? tblChatReceiverCell
                
                cell?.PercentageLbl.text = "\(Int(((fraction) / ImagesArrCount * 100) + (100 / ImagesArrCount * Double(ObjectIndex))))%  "
                
            }
            
        }, success: { (task, responseObject) in
            
            if ObjectIndex < (self.imageCutoutArr.count - 1)
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.callObjectsApi(ObjectIndex: ObjectIndex+1)
                })
            }
            else
            {
                let indexPAth = IndexPath(row: self.chatMessageArray.count - 1, section: 0)
                let cell = self.tblChat.re.cellForRow(at: indexPAth) as? tblChatReceiverCell
                
                cell?.PercentageLbl.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                
                cell?.PercentageLbl.superview!.isHidden = true
                cell?.PercentageSuperViewConst.constant = 0
                
                self.viewforScroll.backgroundColor = hexStringToUIColor(hex: "F7F7F7")
                self.Intelligency_Scrollview.backgroundColor = hexStringToUIColor(hex: "F7F7F7")
                
                if responseObject == nil
                {
                    return
                }
                let predct = responseObject as! NSDictionary
                
                print(predct)
                
                self.cutoutVideoFlag = false
                self.cutoutImageFlag = false
                
                self.isCallCutoutApi = false
                self.cutoutIndex += 1
                self.addcutOutsToTableview(index: self.cutoutIndex, isAnswered: self.isCallCutoutApi)
                
                print(predct)
                
            }
        }, failure: { (task, error) in
            
            if ObjectIndex < (self.imageCutoutArr.count - 1)
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.callObjectsApi(ObjectIndex: ObjectIndex+1)
                })
            }
            else
            {
                let indexPAth = IndexPath(row: self.chatMessageArray.count - 1, section: 0)
                let cell = self.tblChat.re.cellForRow(at: indexPAth) as? tblChatReceiverCell
                
                cell?.PercentageLbl.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                
                cell?.PercentageLbl.superview!.isHidden = true
                cell?.PercentageSuperViewConst.constant = 0
                
                self.viewforScroll.backgroundColor = hexStringToUIColor(hex: "F7F7F7")
                self.Intelligency_Scrollview.backgroundColor = hexStringToUIColor(hex: "F7F7F7")
                
                self.cutoutVideoFlag = false
                self.cutoutImageFlag = false
                
                self.isCallCutoutApi = false
                self.cutoutIndex += 1
                self.addcutOutsToTableview(index: self.cutoutIndex, isAnswered: self.isCallCutoutApi)
                
                print(error)
            }
        })
    }
    
    
    func callFaceApi(Index : Int) {
        
        if !inteligencyScrollArr.contains(cutoutTitle)
        {
            inteligencyScrollArr.add(cutoutTitle)
        }
        
        let lbl_title = "\(userId)-\(cutoutTitle)"//.stringByAddingPercentEncodingForRFC3986()!
        let ApiURL = "https://api.chooch.ai/predict/face?key_id=\(lbl_title)&model_id=46&command=insert_person_image_key_id&apikey=\(apikey)&url=none"
        
        print("call API : \(ApiURL)")
        
        let manager = AFHTTPSessionManager()
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: .allowFragments)
        manager.responseSerializer.acceptableContentTypes = ["text/html","application/json","text/plain"]
        
        let strQue = ApiURL.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        //        let strQue = ApiURL.stringByAddingPercentEncodingForRFC3986()
        
        manager.requestSerializer.timeoutInterval = 500.0
        manager.post(strQue!, parameters: nil, constructingBodyWith: { (formData) in
            
            let Image = self.imageCutoutArr[Index]
            let imagedata = UIImageJPEGRepresentation(Image, 1.0)
            
            formData.appendPart(withFileData: imagedata!, name: "image", fileName: "Image", mimeType: "image/jpeg")
            
        }, progress: { (progress123) in
            
            DispatchQueue.main.async {
                
                print("Progress is : \(progress123.fractionCompleted)")
                
                let fraction = progress123.fractionCompleted
                let ImagesArrCount = Double(self.imageCutoutArr.count)
                
                let indexPAth = IndexPath(row: self.chatMessageArray.count - 1, section: 0)
                let cell = self.tblChat.re.cellForRow(at: indexPAth) as? tblChatReceiverCell
                
                cell?.PercentageLbl.text = "\(Int(((fraction) / ImagesArrCount * 100) + (100 / ImagesArrCount * Double(Index))))%  "
            }
            
        }, success: { (task, responseObject) in
            
            if Index < (self.imageCutoutArr.count - 1)
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.callFaceApi(Index: Index+1)
                })
            }
            else
            {
                let indexPAth = IndexPath(row: self.chatMessageArray.count - 1, section: 0)
                let cell = self.tblChat.re.cellForRow(at: indexPAth) as? tblChatReceiverCell
                
                cell?.PercentageLbl.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                
                cell?.PercentageLbl.superview!.isHidden = true
                cell?.PercentageSuperViewConst.constant = 0
                
                self.viewforScroll.backgroundColor = hexStringToUIColor(hex: "F7F7F7")
                self.Intelligency_Scrollview.backgroundColor = hexStringToUIColor(hex: "F7F7F7")
                
                if responseObject == nil
                {
                    return
                }
                let predct = responseObject as! NSDictionary
                
                print(predct)
                
                self.cutoutVideoFlag = false
                self.cutoutImageFlag = false
                
                self.isCallCutoutApi = false
                self.cutoutIndex += 1
                self.addcutOutsToTableview(index: self.cutoutIndex, isAnswered: self.isCallCutoutApi)
            }
            
            
        }, failure: { (task, error) in
            
            if Index < (self.imageCutoutArr.count - 1)
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.callFaceApi(Index: Index+1)
                })
            }
            else
            {
                let indexPAth = IndexPath(row: self.chatMessageArray.count - 1, section: 0)
                let cell = self.tblChat.re.cellForRow(at: indexPAth) as? tblChatReceiverCell
                
                cell?.PercentageLbl.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                
                cell?.PercentageLbl.superview!.isHidden = true
                cell?.PercentageSuperViewConst.constant = 0
                
                self.viewforScroll.backgroundColor = hexStringToUIColor(hex: "F7F7F7")
                self.Intelligency_Scrollview.backgroundColor = hexStringToUIColor(hex: "F7F7F7")
                
                self.cutoutVideoFlag = false
                self.cutoutImageFlag = false
                
                self.isCallCutoutApi = false
                self.cutoutIndex += 1
                self.addcutOutsToTableview(index: self.cutoutIndex, isAnswered: self.isCallCutoutApi)
            }
            
            print(error)
            
        })
    }
    
    
    func addcutOutsToTableview(index : Int , isAnswered : Bool)  {
        
        if isAnswered
        {
            cutoutInteligencyFlag = false
            
            
            if !cutoutVideoFlag {
                
                if isDense
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        
                        self.LoaderScreen_TitleLbl.superview!.backgroundColor = hexStringToUIColor(hex: "B7B7B7").withAlphaComponent(0.7)
                        let myattrString2 = NSMutableAttributedString(string: "Adding Images for ", attributes: self.myAttribute)
                        myattrString2.append(NSMutableAttributedString(string: "''\(self.cutoutTitle)''", attributes: self.myAttributeBold))
                        myattrString2.append(NSMutableAttributedString(string: "...", attributes: self.myAttribute))
                        
                        self.LoaderScreen_TitleLbl.attributedText = myattrString2
                        
                        let myString2 = NSMutableAttributedString(string: "Would you like to add more images of ", attributes: self.myAttribute)
                        if self.cutoutTitle.contains("looks like")
                        {
                            var removetitle = self.cutoutTitle
                            
                            if let range = removetitle.range(of: "looks like ") {
                                removetitle.removeSubrange(range)
                            }
                            
                            myString2.append(NSMutableAttributedString(string: "''\(removetitle)''?", attributes: self.myAttributeBold))
                        }
                        else
                        {
                            myString2.append(NSMutableAttributedString(string: "''\(self.cutoutTitle)''?", attributes: self.myAttributeBold))
                        }
                        
                        self.AddToArrayMessage(Attrmsg: myString2, isUser: false , isTransparentBack: false)
                        self.SetInteligencyScrollView()
                        self.cutoutVideoFlag = true
                        self.cutoutImageFlag = true
                        
                    }
                    
                }
                else
                {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        
                        self.LoaderScreen_TitleLbl.superview!.backgroundColor = hexStringToUIColor(hex: "B7B7B7").withAlphaComponent(0.7)
                        let myattrString2 = NSMutableAttributedString(string: "Adding Images for ", attributes: self.myAttribute)
                        myattrString2.append(NSMutableAttributedString(string: "''\(self.cutoutTitle)''", attributes: self.myAttributeBold))
                        myattrString2.append(NSMutableAttributedString(string: "...", attributes: self.myAttribute))
                        
                        self.LoaderScreen_TitleLbl.attributedText = myattrString2
                        
                        let myString2 = NSMutableAttributedString(string: "Can you take a video of ", attributes: self.myAttribute)
                        
                        if self.cutoutTitle.contains("looks like")
                        {
                            var removetitle = self.cutoutTitle
                            
                            if let range = removetitle.range(of: "looks like ") {
                                removetitle.removeSubrange(range)
                            }
                            
                            myString2.append(NSMutableAttributedString(string: "''\(removetitle)''?", attributes: self.myAttributeBold))
                        }
                        else
                        {
                            myString2.append(NSMutableAttributedString(string: "''\(self.cutoutTitle)''?", attributes: self.myAttributeBold))
                        }
                        
                        self.AddToArrayMessage(Attrmsg: myString2, isUser: false , isTransparentBack: false)
                        self.SetInteligencyScrollView()
                        self.cutoutVideoFlag = true
                        
                    }
                }
                
            }
            else
            {
                let yesNOmsg = self.txtTyping.text!.trim()
                
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    
                    if (((yesNOmsg.capitalized.prefix(1)) == "Y" || (yesNOmsg.capitalized.prefix(1)) == "N") && !self.cutoutAnyOtherFlag) || self.addAnyotherfeedbackFlag
                    {
                        if (yesNOmsg.capitalized.prefix(1)) == "Y"
                        {
                            self.txtTyping.endEditing(true)
                            self.Yesbtn_Pressed()
                        }
                        else if (yesNOmsg.capitalized.prefix(1)) == "N"
                        {
                            if self.cutoutImageFlag && self.cutoutVideoFlag
                            {
                                self.imageCutoutArr.append(self.imageCutout)
                                self.SetInteligencyScrollView()
                                
                                if !self.cutoutAnyOtherFlag
                                {
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                        self.AddToArrayMessage(msg: "Any other feedback?", isUser: false)
                                        self.cutoutAnyOtherFlag = true
                                        self.SetInteligencyScrollView()
                                    }
                                }
                                else
                                {
                                    
                                    self.AddToArrayMessage(msg: "Thank you!", isUser: false)
                                    self.TypingView.isHidden = false
                                    self.txtTyping.endEditing(true)
                                    if !DeviceType.IS_IPHONE_X
                                    {
                                        self.InteligencyScrollHeightConst.constant = 10.0
                                        for view in self.viewforScroll.subviews
                                        {
                                            view.removeFromSuperview()
                                        }
                                        self.viewforScroll.backgroundColor = .clear
                                        self.Intelligency_Scrollview.backgroundColor = .clear
                                        self.view.layoutIfNeeded()
                                    }
                                    
                                    self.callCutoutAPI(params: self.DictCutout, cutImage: self.imageCutout)
                                    
                                    self.cutoutAnyOtherFlag = false
                                    self.cutoutImageFlag = false
                                    self.cutoutVideoFlag = false
                                }
                                
                            }
                            else
                            {
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                    let myString2 = NSMutableAttributedString(string: "Would you like to add more images of ", attributes: self.myAttribute)
                                    if self.cutoutTitle.contains("looks like")
                                    {
                                        var removetitle = self.cutoutTitle
                                        
                                        if let range = removetitle.range(of: "looks like ") {
                                            removetitle.removeSubrange(range)
                                        }
                                        
                                        myString2.append(NSMutableAttributedString(string: "''\(removetitle)''?", attributes: self.myAttributeBold))
                                    }
                                    else
                                    {
                                        myString2.append(NSMutableAttributedString(string: "''\(self.cutoutTitle)''?", attributes: self.myAttributeBold))
                                    }
                                    
                                    self.AddToArrayMessage(Attrmsg: myString2, isUser: false , isTransparentBack: false)
                                    self.SetInteligencyScrollView()
                                    self.cutoutImageFlag = true
                                    
                                    
                                    
                                }
                            }
                        }
                        else
                        {
                            let myString2 = NSMutableAttributedString(string: "Please answer ", attributes: self.myAttribute)
                            myString2.append(NSMutableAttributedString(string: "Yes", attributes: self.myAttributeBold))
                            myString2.append(NSMutableAttributedString(string: " or ", attributes: self.myAttribute))
                            myString2.append(NSMutableAttributedString(string: "No", attributes: self.myAttributeBold))
                            myString2.append(NSMutableAttributedString(string: ".", attributes: self.myAttribute))
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                self.AddToArrayMessage(Attrmsg: myString2, isUser: false , isTransparentBack: false)
                                self.SetInteligencyScrollView()
                            }
                        }
                        
                    }
                    else if self.cutoutAnyOtherFlag
                    {
                        
                        self.AddToArrayMessage(msg: "Thank you!", isUser: false)
                        self.TypingView.isHidden = false
                        self.txtTyping.endEditing(true)
                        if !DeviceType.IS_IPHONE_X
                        {
                            
                            self.InteligencyScrollHeightConst.constant = 10.0
                            for view in self.viewforScroll.subviews
                            {
                                view.removeFromSuperview()
                            }
                            self.viewforScroll.backgroundColor = .clear
                            self.Intelligency_Scrollview.backgroundColor = .clear
                            self.view.layoutIfNeeded()
                        }
                        
                        
                        
                        self.callCutoutAPI(params: self.DictCutout, cutImage: self.imageCutout)
                        
                        self.cutoutAnyOtherFlag = false
                        self.cutoutImageFlag = false
                        self.cutoutVideoFlag = false
                    }
                    else
                    {
                        
                        let myString2 = NSMutableAttributedString(string: "Please answer ", attributes: self.myAttribute)
                        myString2.append(NSMutableAttributedString(string: "Yes", attributes: self.myAttributeBold))
                        myString2.append(NSMutableAttributedString(string: " or ", attributes: self.myAttribute))
                        myString2.append(NSMutableAttributedString(string: "No", attributes: self.myAttributeBold))
                        myString2.append(NSMutableAttributedString(string: ".", attributes: self.myAttribute))
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.AddToArrayMessage(Attrmsg: myString2, isUser: false , isTransparentBack: false)
                            self.SetInteligencyScrollView()
                        }
                        
                    }
                    
                }
            }
            
            
        }
        else
        {
            
            if !cutoutAskingTypeFlag
            {
                var faceFlag = false
                
                for dict in cutoutsArr
                {
                    let dict1 = dict as! NSDictionary
                    
                    if (dict1.object(forKey: "parent_object") as? String) == "face" {
                        self.AddToArrayMessage(msg: "Would you like to train face or object?", isUser: false)
                        self.SetInteligencyScrollView()
                        faceFlag = true
                        break
                    }
                }
                
                if !faceFlag
                {
                    cutoutInteligencyFlag = true
                    cutoutAskingTypeFlag = true
                    SubCutoutsMethod(index: index)
                }
                
            }
            else
            {
                cutoutInteligencyFlag = true
                SubCutoutsMethod(index: index)
            }
            
            
        }
        
        
    }
    
    func SubCutoutsMethod(index : Int) {
        
        if QueTypeStr.lowercased().trim() == "face"
        {
            
            if facesArray.count > 0 && facesArray.count > index
            {
                let dict = facesArray.object(at: index) as! NSDictionary
                print(dict)
                
                if let cordinate = dict.object(forKey: "coordinates") as? String
                {
                    let coordinateArr1 = cordinate.components(separatedBy: ",")
                    
                    var xAx1 = Double(coordinateArr1[0])
                    var width11 = Double(coordinateArr1[1])! - xAx1!
                    var yAx1 = Double(coordinateArr1[2])
                    var height11 = Double(coordinateArr1[3])! - yAx1!
                    
                    xAx1 = xAx1! - width11 * 0.25
                    width11 = width11 + width11 * 0.5
                    yAx1 = yAx1! - height11 * 0.25
                    height11 = height11 + height11 * 0.5
                    
                    width11 = (width11 + xAx1!) > Double(Backimage.size.width) ? (Double(Backimage.size.width) - xAx1!) : width11
                    height11 = (height11 + yAx1!) > Double(Backimage.size.height) ? (Double(Backimage.size.height) - yAx1!) : height11
                    
                    xAx1 = xAx1! < 0.0 ? 0.0 : xAx1
                    yAx1 = yAx1! < 0.0 ? 0.0 : yAx1
                    
                    let rect = CGRect(x: xAx1!, y: yAx1!, width: width11, height: height11)
                    
                    let image123 = imageWithImage(image: Backimage, croppedTo: rect)
                    
                    print(image123)
                    
                    AddToArrayMessage(msg: "Who is this?", isUser: false)
                    
                    let fullString = NSMutableAttributedString(string: "")
                    
                    let image = (image123.resize(targetSize: CGSize(width: self.view.Getwidth * 0.55, height: self.view.Getwidth * 0.55))).imageWithBorder(width: 1.0, color: .white, cornorRadius: 10.0)
                    
                    let image1Attachment = NSTextAttachment.getCenteredImageAttachment(with: image!, and: UIFont.init(name: CustomFontWeight.reguler, size: self.view.Getwidth * 0.1)!)
                    let image1String = NSAttributedString(attachment: image1Attachment!)
                    fullString.append(image1String)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        self.AddToArrayMessage(Attrmsg: fullString, isUser: false, isTransparentBack: true)
                        
                        self.DictCutout = dict
                        self.imageCutout = image123
                        
                        self.SetInteligencyScrollView()
                        self.cutoutAnyOtherFlag = true
                        self.addAnyotherfeedbackFlag = true
                        self.isCallCutoutApi = true
                    }
                }
                else
                {
                    cutoutIndex += 1
                    addcutOutsToTableview(index: cutoutIndex, isAnswered: false)
                }
            }
            else
            {
                
                startSession = false
                self.dismiss(animated: true, completion: nil)
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
                
            }
            
        }
        else
        {
            if cutoutsArr.count > 0 && (cutoutsArr.count > index) {
                
                let dict = cutoutsArr.object(at: index) as! NSDictionary
                print(dict)
                
                if let cordinate = dict.object(forKey: "coordinates") as? String
                {
                    let coordinateArr = cordinate.components(separatedBy: ",")
                    
                    var xAx = Double(coordinateArr[0])
                    var width1 = Double(coordinateArr[1])! - xAx!
                    var yAx = Double(coordinateArr[2])
                    var height1 = Double(coordinateArr[3])! - yAx!
                    
                    for coord in coordinatesArr
                    {
                        let coordinateArr1 = (coord as! String).components(separatedBy: ",")
                        
                        let xAx1 = Double(coordinateArr1[0])
                        let width11 = Double(coordinateArr1[1])! - xAx1!
                        let yAx1 = Double(coordinateArr1[2])
                        let height11 = Double(coordinateArr1[3])! - yAx1!
                        
                        let xyDiff = 0.15
                        let widhtheightDiff = 0.3
                        
                        if (xAx1! >= xAx! - (width1 * xyDiff)) && (xAx1! <= xAx! + (width1 * xyDiff)) {
                            if (yAx1! >= yAx! - (height1 * xyDiff)) && (yAx1! <= yAx! + (height1 * xyDiff)) {
                                if (width11 >= width1 - (width1 * widhtheightDiff)) && (width11 <= width1 + (width1 * widhtheightDiff)) {
                                    if (height11 >= height1 - (height1 * widhtheightDiff)) && (height11 <= height1 + (height1 * widhtheightDiff)) {
                                        
                                        if !coordinatesArr.contains(cordinate)
                                        {
                                            coordinatesArr.add(cordinate)
                                            break
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if !coordinatesArr.contains(cordinate)
                    {
                        coordinatesArr.add(cordinate)
                        
                        
                        if (QueTypeStr.lowercased().trim() == "face" && (dict.object(forKey: "parent_object") as? String) != "face") ||
                            (QueTypeStr.lowercased().trim() == "object" && (dict.object(forKey: "parent_object") as? String) == "face") {
                            
                            cutoutIndex += 1
                            addcutOutsToTableview(index: cutoutIndex, isAnswered: false)
                            return
                        }
                        
                        
                        if (dict.object(forKey: "parent_object") as? String) == "face"
                        {
                            if facesArray.count > 0 && facesArray.count > index
                            {
                                let dict = facesArray.object(at: index) as! NSDictionary
                                print(dict)
                                
                                if let cordinate = dict.object(forKey: "coordinates") as? String
                                {
                                    let coordinateArr1 = cordinate.components(separatedBy: ",")
                                    
                                    var xAx1 = Double(coordinateArr1[0])
                                    var width11 = Double(coordinateArr1[1])! - xAx1!
                                    var yAx1 = Double(coordinateArr1[2])
                                    var height11 = Double(coordinateArr1[3])! - yAx1!
                                    
                                    xAx1 = xAx1! - width11 * 0.25
                                    width11 = width11 + width11 * 0.5
                                    yAx1 = yAx1! - height11 * 0.25
                                    height11 = height11 + height11 * 0.5
                                    
                                    width11 = (width11 + xAx1!) > Double(Backimage.size.width) ? (Double(Backimage.size.width) - xAx1!) : width11
                                    height11 = (height11 + yAx1!) > Double(Backimage.size.height) ? (Double(Backimage.size.height) - yAx1!) : height11
                                    
                                    xAx1 = xAx1! < 0.0 ? 0.0 : xAx1
                                    yAx1 = yAx1! < 0.0 ? 0.0 : yAx1
                                    
                                    
                                    let rect = CGRect(x: xAx1!, y: yAx1!, width: width11, height: height11)
                                    
                                    let image123 = imageWithImage(image: Backimage, croppedTo: rect)
                                    
                                    print(image123)
                                    
                                    AddToArrayMessage(msg: "Who is this?", isUser: false)
                                    
                                    let fullString = NSMutableAttributedString(string: "")
                                    
                                    let image = (image123.resize(targetSize: CGSize(width: self.view.Getwidth * 0.55, height: self.view.Getwidth * 0.55))).imageWithBorder(width: 1.0, color: .white, cornorRadius: 10.0)
                                    
                                    let image1Attachment = NSTextAttachment.getCenteredImageAttachment(with: image!, and: UIFont.init(name: CustomFontWeight.reguler, size: self.view.Getwidth * 0.1)!)
                                    let image1String = NSAttributedString(attachment: image1Attachment!)
                                    fullString.append(image1String)
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                        self.AddToArrayMessage(Attrmsg: fullString, isUser: false, isTransparentBack: true)
                                        
                                        self.DictCutout = dict
                                        self.imageCutout = image123
                                        
                                        self.SetInteligencyScrollView()
                                        self.cutoutAnyOtherFlag = true
                                        self.addAnyotherfeedbackFlag = true
                                        self.isCallCutoutApi = true
                                    }
                                }
                                else
                                {
                                    cutoutIndex += 1
                                    addcutOutsToTableview(index: cutoutIndex, isAnswered: false)
                                }
                            }
                            else
                            {
                                cutoutIndex += 1
                                addcutOutsToTableview(index: cutoutIndex, isAnswered: false)
                            }
                            
                        }
                        else
                        {
                            let rect = CGRect(x: xAx!, y: yAx!, width: width1, height: height1)
                            
                            let image123 = imageWithImage(image: Backimage, croppedTo: rect)
                            
                            print(image123)
                            
                            (dict.object(forKey: "parent_object") as? String) == "face" ? AddToArrayMessage(msg: "Who is this?", isUser: false) : AddToArrayMessage(msg: "What is this?", isUser: false)
                            
                            
                            let fullString = NSMutableAttributedString(string: "")
                            
                            let image = (image123.resize(targetSize: CGSize(width: self.view.Getwidth * 0.55, height: self.view.Getwidth * 0.55))).imageWithBorder(width: 1.0, color: .white, cornorRadius: 10.0)
                            
                            let image1Attachment = NSTextAttachment.getCenteredImageAttachment(with: image!, and: UIFont.init(name: CustomFontWeight.reguler, size: self.view.Getwidth * 0.1)!)
                            let image1String = NSAttributedString(attachment: image1Attachment!)
                            fullString.append(image1String)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                self.AddToArrayMessage(Attrmsg: fullString, isUser: false, isTransparentBack: true)
                                
                                self.DictCutout = dict
                                self.imageCutout = image123
                                
                                self.SetInteligencyScrollView()
                                self.addAnyotherfeedbackFlag = false
                                self.cutoutAnyOtherFlag = false
                                self.isCallCutoutApi = true
                            }
                        }
                        
                        
                    }
                    else
                    {
                        cutoutIndex += 1
                        addcutOutsToTableview(index: cutoutIndex, isAnswered: false)
                    }
                }
                else
                {
                    cutoutIndex += 1
                    addcutOutsToTableview(index: cutoutIndex, isAnswered: false)
                }
            }
            else
            {
                
                if QueTypeStr.lowercased().trim() == "face"
                {
                    self.AddToArrayMessage(msg: "Thanks", isUser: false)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        
                        startSession = false
                        self.dismiss(animated: true, completion: nil)
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
                        
                    }
                    
                    return
                }
                
                
                for coord in coordinatesArr
                {
                    let xAx = 0.0
                    let width1 = Double(Backimage.size.width)
                    let yAx = 0.0
                    let height1 = Double(Backimage.size.height)
                    
                    let coordinateArr1 = (coord as! String).components(separatedBy: ",")
                    
                    let xAx1 = Double(coordinateArr1[0])
                    let width11 = Double(coordinateArr1[1])! - xAx1!
                    let yAx1 = Double(coordinateArr1[2])
                    let height11 = Double(coordinateArr1[3])! - yAx1!
                    
                    let xyDiff = 0.15
                    let widhtheightDiff = 0.3
                    
                    if (xAx1! >= xAx - (width1 * xyDiff)) && (xAx1! <= xAx + (width1 * xyDiff)) {
                        if (yAx1! >= yAx - (height1 * xyDiff)) && (yAx1! <= yAx + (height1 * xyDiff)) {
                            if (width11 >= width1 - (width1 * widhtheightDiff)) && (width11 <= width1 + (width1 * widhtheightDiff)) {
                                if (height11 >= height1 - (height1 * widhtheightDiff)) && (height11 <= height1 + (height1 * widhtheightDiff)) {
                                    
                                    self.AddToArrayMessage(msg: "Thanks", isUser: false)
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                        
                                        startSession = false
                                        self.dismiss(animated: true, completion: nil)
                                        
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
                                        
                                    }
                                    
                                    return
                                }
                            }
                        }
                    }
                }
                
                
                self.cutoutsFlag = true
                self.cutoutInteligencyFlag = false
                
                AddToArrayMessage(msg: "What is this?", isUser: false)
                
                
                let fullString = NSMutableAttributedString(string: "")
                
                let image = (Backimage.resize(targetSize: CGSize(width: self.view.Getwidth * 0.55, height: self.view.Getwidth * 0.55))).imageWithBorder(width: 1.0, color: .white, cornorRadius: 10.0)
                
                let image1Attachment = NSTextAttachment.getCenteredImageAttachment(with: image!, and: UIFont.init(name: CustomFontWeight.reguler, size: self.view.Getwidth * 0.1)!)
                
                let image1String = NSAttributedString(attachment: image1Attachment!)
                
                fullString.append(image1String)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.AddToArrayMessage(Attrmsg: fullString, isUser: false , isTransparentBack: true)
                    
                    self.SetInteligencyScrollView()
                }
            }
        }
        
    }
    
    
    
    // MARK: - Tableview Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatMessageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let messageDict = chatMessageArray[indexPath.row]
        
        if (messageDict["isUser"] as! Bool)
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "tblChatSenderCell", for: indexPath) as! tblChatSenderCell
            
            if messageDict["message"] is NSAttributedString
            {
                cell.lblMessage.attributedText = (messageDict["message"] as! NSAttributedString)
                cell.lblMessage.superview!.backgroundColor = (messageDict["isTransparent"] as! Bool) ? .clear : hexStringToUIColor(hex: "0096FF")
                cell.TickHeightConst.constant = 34.0
                cell.lblMessage.textAlignment = .left
            }
            else
            {
                cell.lblMessage.text = "\(messageDict["message"]!)"
                cell.lblMessage.superview!.backgroundColor = hexStringToUIColor(hex: "0096FF")
                cell.TickHeightConst.constant = 0.0
                cell.lblMessage.textAlignment = .center
                
            }
            
            cell.layoutIfNeeded()
            
            cell.lblMessage.superview!.addShadow()
            
            
            return cell
            
        }
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "tblChatReceiverCell", for: indexPath) as! tblChatReceiverCell
            
            cell.PercentageLbl.superview!.isHidden = (messageDict["message"] as? String == "Sending images and feedback...") ? false : true
            cell.PercentageSuperViewConst.constant = (messageDict["message"] as? String == "Sending images and feedback...") ? 50 : 0
            
            if messageDict["message"] is NSAttributedString
            {
                
                let attrText = messageDict["message"] as! NSAttributedString
                
                cell.lblMessage.attributedText = attrText
                
                cell.lblMessage.superview!.backgroundColor = (messageDict["isTransparent"] as! Bool) ? .clear : .white
                
            }
            else
            {
                cell.lblMessage.text = "\(messageDict["message"]!)"
                cell.lblMessage.superview!.backgroundColor = .white
            }
            
            if messageDict["message"] as? String == "Sending images and feedback..." && (indexPath.row == (chatMessageArray.count - 1))
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    
                    cell.PercentageLbl.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                    
                    cell.PercentageLbl.superview!.layer.masksToBounds = false
                    cell.PercentageLbl.layer.masksToBounds = false
                    
                    let pulsator = Pulsator()
                    pulsator.position = cell.PercentageLbl.center
                    
                    cell.layoutIfNeeded()
                    pulsator.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1).cgColor
                    
                    cell.PercentageLbl.layer.addSublayer(pulsator)
                    
                    pulsator.start()
                    
                })
                
            }
            else
            {
                cell.PercentageLbl.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                
                cell.PercentageLbl.superview!.isHidden = true
                cell.PercentageSuperViewConst.constant = 0
                
                //                cell.PercentageLbl.text = "100%"
            }
            
            return cell
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    // MARK: - Button Events
    
    @IBAction func btnSend(_ sender: UIButton) {
        
        if isReadyToSendComplain()
        {
            self.InteligencyScrollHeightConst.constant = 0.0
            self.view.layoutIfNeeded()
            
            if !cutoutAskingTypeFlag
            {
                
                cutoutAskingTypeFlag = true
                self.AddToArrayMessage(msg: "\(self.txtTyping.text!.trim())", isUser: true)
                QueTypeStr = self.txtTyping.text!.trim()
                self.addcutOutsToTableview(index: self.cutoutIndex, isAnswered: false)
                
            }
            else if !cutoutsFlag
            {
                if self.txtTyping.text!.lowercased() != "no" && self.txtTyping.text!.lowercased() != "yes" && self.txtTyping.text!.lowercased().trim() != "" && (!cutoutAnyOtherFlag || addAnyotherfeedbackFlag)
                {
                    cutoutTitle = self.txtTyping.text!.trim()
                    
                }
                cutoutAnyotherStr = self.txtTyping.text!.trim()
                
                if self.txtTyping.text!.lowercased() == "skip"
                {
                    cutoutIndex += 1
                    self.cutoutImageFlag = false
                    self.cutoutVideoFlag = false
                    self.addcutOutsToTableview(index: self.cutoutIndex, isAnswered: false)
                }
                else if self.txtTyping.text!.lowercased() == "done"
                {
                    self.AddToArrayMessage(msg: "Thank you!", isUser: false)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        
                        startSession = false
                        self.dismiss(animated: true, completion: nil)
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
                        
                    }
                    
                    return
                }
                else
                {
                    self.AddToArrayMessage(msg: "\(self.txtTyping.text!.trim())", isUser: true)
                    self.addcutOutsToTableview(index: self.cutoutIndex, isAnswered: self.isCallCutoutApi)
                }
                
                
            }
            else if !whatsthisFlag
            {
                ObjectTitle = txtTyping.text!.trim()
                
                if self.txtTyping.text!.lowercased() == "skip" || self.txtTyping.text!.lowercased() == "done"
                {
                    self.AddToArrayMessage(msg: "Thank you!", isUser: false)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        
                        startSession = false
                        self.dismiss(animated: true, completion: nil)
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
                        
                    }
                    
                    return
                }
                
                LoaderScreen_TitleLbl.superview!.backgroundColor = hexStringToUIColor(hex: "B7B7B7").withAlphaComponent(0.7)
                let myString2 = NSMutableAttributedString(string: "Adding Images for ", attributes: myAttribute)
                myString2.append(NSMutableAttributedString(string: "''\(ObjectTitle)''", attributes: myAttributeBold))
                myString2.append(NSMutableAttributedString(string: "...", attributes: myAttribute))
                
                LoaderScreen_TitleLbl.attributedText = myString2
                
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.AddToArrayMessage(msg: "\(self.ObjectTitle)", isUser: true)
                    
                    if self.isDense {
                        
                        self.addvideoFlag = true
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            let myString2 = NSMutableAttributedString(string: "Would you like to add more images of ", attributes: self.myAttribute)
                            myString2.append(NSMutableAttributedString(string: "''\(self.ObjectTitle)''?", attributes: self.myAttributeBold))
                            
                            self.AddToArrayMessage(Attrmsg: myString2, isUser: false , isTransparentBack: false)
                            self.whatsthisFlag = true
                            self.SetInteligencyScrollView()
                            
                        }
                    }
                    else
                    {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            
                            let myString2 = NSMutableAttributedString(string: "Can you take a video of ", attributes: self.myAttribute)
                            myString2.append(NSMutableAttributedString(string: "''\(self.ObjectTitle)''?", attributes: self.myAttributeBold))
                            
                            self.AddToArrayMessage(Attrmsg: myString2, isUser: false , isTransparentBack: false)
                            
                            self.whatsthisFlag = true
                            self.SetInteligencyScrollView()
                        }
                    }
                    
                    
                }
                
            }
            else if !addvideoFlag
            {
                let yesNOmsg = self.txtTyping.text!.trim()
                
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    
                    self.AddToArrayMessage(msg: "\(yesNOmsg)", isUser: true)
                    
                    if (yesNOmsg.capitalized.prefix(1)) == "Y" || (yesNOmsg.capitalized.prefix(1)) == "N"
                    {
                        if (yesNOmsg.capitalized.prefix(1)) == "Y"
                        {
                            self.txtTyping.endEditing(true)
                            self.Yesbtn_Pressed()
                        }
                        else
                        {
                            //                            self.SetInteligencyScrollView()
                            self.addvideoFlag = true
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                let myString2 = NSMutableAttributedString(string: "Would you like to add more images of ", attributes: self.myAttribute)
                                myString2.append(NSMutableAttributedString(string: "''\(self.ObjectTitle)''?", attributes: self.myAttributeBold))
                                
                                self.AddToArrayMessage(Attrmsg: myString2, isUser: false , isTransparentBack: false)
                                
                                self.SetInteligencyScrollView()
                                
                            }
                            
                        }
                        
                    }
                    else
                    {
                        
                        let myString2 = NSMutableAttributedString(string: "Please answer ", attributes: self.myAttribute)
                        myString2.append(NSMutableAttributedString(string: "Yes", attributes: self.myAttributeBold))
                        myString2.append(NSMutableAttributedString(string: " or ", attributes: self.myAttribute))
                        myString2.append(NSMutableAttributedString(string: "No", attributes: self.myAttributeBold))
                        myString2.append(NSMutableAttributedString(string: ".", attributes: self.myAttribute))
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.AddToArrayMessage(Attrmsg: myString2, isUser: false , isTransparentBack: false)
                            self.SetInteligencyScrollView()
                        }
                        
                    }
                    
                }
                
            }
            else if !addotherPhotosFlag
            {
                let yesNOmsg = self.txtTyping.text!.trim()
                
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    
                    self.AddToArrayMessage(msg: "\(yesNOmsg)", isUser: true)
                    
                    if (yesNOmsg.capitalized.prefix(1)) == "Y" || (yesNOmsg.capitalized.prefix(1)) == "N"
                    {
                        if (yesNOmsg.capitalized.prefix(1)) == "Y"
                        {
                            self.txtTyping.endEditing(true)
                            self.Yesbtn_Pressed()
                        }
                        else
                        {
                            self.addotherPhotosFlag = true
                            self.SetInteligencyScrollView()
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                
                                self.AddToArrayMessage(msg: "Thanks", isUser: false)
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                                    self.AddToArrayMessage(msg: "Any other feedback?", isUser: false)
                                    self.cutoutInteligencyFlag = false
                                    self.anyfeedbackfornoInteligencyFlag = true
                                    self.SetInteligencyScrollView()
                                }
                                
                            }
                            
                        }
                        
                    }
                    else
                    {
                        
                        let myString2 = NSMutableAttributedString(string: "Please answer ", attributes: self.myAttribute)
                        myString2.append(NSMutableAttributedString(string: "Yes", attributes: self.myAttributeBold))
                        myString2.append(NSMutableAttributedString(string: " or ", attributes: self.myAttribute))
                        myString2.append(NSMutableAttributedString(string: "No", attributes: self.myAttributeBold))
                        myString2.append(NSMutableAttributedString(string: ".", attributes: self.myAttribute))
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                            self.AddToArrayMessage(Attrmsg: myString2, isUser: false , isTransparentBack: false)
                            self.SetInteligencyScrollView()
                        }
                        
                    }
                    
                }
                
                
                
            }
            else if !anyotherFeedbackFlag
            {
                OtherfeedbackText = txtTyping.text!.trim()
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    
                    self.AddToArrayMessage(msg: "\(self.OtherfeedbackText)", isUser: true)
                    self.anyotherFeedbackFlag = true
                    
                    self.SetInteligencyScrollView()
                    
                    self.CameraBtn.isHidden = true
                    self.GallaryBtn.isHidden = true
                    
                    self.txtTyping.endEditing(true)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        
                        self.AddToArrayMessage(msg: "Thank you!", isUser: false)
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            
                            self.isCallApi = true
                            
                            self.AddToArrayMessage(msg: "Sending images and feedback...", isUser: false)
                            
                            self.CallFeedBackApi(IndexRow: 0)
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
        
        txtTyping.text = ""
        video_3dLoader.isHidden = true
        
    }
    
    @IBAction func FeedBack_Pressed(_ sender: UIButton) {
        
        startSession = false
        
        CheckFlash()
        
        self.viewforScroll.removeFromSuperview()
        
        self.dismiss(animated: true, completion: nil)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
        
    }
    
    @IBAction func ClearPredictions_Pressed(_ sender: UIButton) {
        
        
        
    }
    
    
    @IBAction func logOut_Pressed(_ sender: UIButton) {
        

//        let profileNav = mainStoryBoard.instantiateViewController(withIdentifier: "profileNavController") as! UINavigationController
//        self.present(profileNav, animated: true, completion: nil)
        
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "openMenu"), object: nil)
        }
        
        
//        self.presentAlertWithTitle(title: "Do you want to log out?", message: "", options: "Yes","No", completion: { (Index) in
//            
//            if Index == 0
//            {
//                userDefault.removeObject(forKey: custToken)
//                userDefault.removeObject(forKey: custUserame)
//                userDefault.removeObject(forKey: custId)
//                
//                let loginNC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginNavigationController") as! UINavigationController
//                
//                let manager = LoginManager()
//                manager.logOut()
//                
//                GIDSignIn.sharedInstance()?.signOut()
//                
//                window?.rootViewController = loginNC
//                window?.makeKeyAndVisible()
//            }
//            
//        })
        
    }
    
    @IBAction func Flash_Pressed(_ sender: UIButton) {
        
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    let torchOn = !device.isTorchActive
                    try device.setTorchModeOn(level: 1.0)
                    device.torchMode = torchOn ? AVCaptureDevice.TorchMode.on : AVCaptureDevice.TorchMode.off
                    device.unlockForConfiguration()
                    self.tabicon_flash.image = torchOn ? UIImage(named: "ic_flash_on") : UIImage(named: "ic_flash")
                } catch {
                    print(error.localizedDescription)
                }
            }
            else
            {
                presentAlertWithTitle(title: "Your device doesn't have flash to use", message: "", options: "OK") { (buttonIndex) in
                    
                }
            }
        }
        
    }
    
    @IBAction func Gallary_Pressed(_ sender: UIButton) {
        
        startSession = false
        
        self.viewforScroll.removeFromSuperview()
        
        self.dismiss(animated: true, completion: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
        
    }
    
    func tempZipPath() -> String {
        var path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        path += "/Images.zip"
        return path
    }
    
    func GetPathOfFolderForZip() -> String {
        var path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        path += "/MakeZip"
        return path
    }
    
    
    func clearAllFile() {
        let fileManager = FileManager.default
        let myDocuments = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        let removeDir = myDocuments.appendingPathComponent("MakeZip")
        do {
            try fileManager.removeItem(at: removeDir)
        } catch {
            return
        }
    }
    
    func getUIImage(asset: PHAsset) -> UIImage? {
        
        var img: UIImage?
        let manager = PHImageManager.default()
        let options = PHImageRequestOptions()
        options.version = .original
        options.isSynchronous = true
        manager.requestImageData(for: asset, options: options) { data, _, _, _ in
            
            if let data = data {
                img = UIImage(data: data)
            }
        }
        return img
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    var counter1 = 0.0
    var timercount = 0.0
    
    @objc func ClickImg(timer: Timer!) {
        print("Changed")
        
        counter1 += 0.5
        timercount += 0.5
        //        print(timer)
        
        RecordingLbl.text = "00:00:0\(Int(timercount))"
        
        if counter1 < 5.0 && ImagesArr.count <= 11
        {
            isCapture = true
        }
        else
        {
            self.timer1.invalidate()
            
            RecordingLbl.isHidden = true
            RecordingLbl.text = "00:00:00"
            let btn = UIButton()
            LoaderDone_Pressed(btn)
        }
        
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: - TextView Delegates
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
            textView.font = UIFont.init(name: CustomFontWeight.reguler, size: 14)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == "" {
            
            textView.text = "Start Typing...."
            textView.textColor = UIColor.lightGray
            textView.font = UIFont.init(name: CustomFontWeight.MyriadItalic, size: 16)
        }
    }
    
    
    func CheckFlash()  {
        
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    let torchOn = device.isTorchActive
                    self.tabicon_flash.image = torchOn ? UIImage(named: "ic_flash_on") : UIImage(named: "ic_flash")
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        
    }
    
}

extension FeedbackVC: VideoCaptureDelegate {
    func videoCapture(_ capture: VideoCapture, didCaptureVideoTexture texture: MTLTexture?, timestamp: CMTime) {
        
        if texture != nil {
        }
    }
    
    func videoCapture(_ capture: VideoCapture, didCapturePhotoTexture texture: MTLTexture?, previewImage: UIImage?) {
        
        DispatchQueue.main.async {
            self.CheckFlash()
            if self.isCapture
            {
                if self.cutoutsFlag
                {
                    self.ImagesArr.append(previewImage!)
                    self.isCapture = false
                }
                else
                {
                    self.imageCutoutArr.append(previewImage!)
                    self.isCapture = false
                }
                
            }
            
        }
    }
}

extension ViewController: UITableViewDelegate {
    //ReverseExtension also supports handling UITableViewDelegate.
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollView.contentOffset.y =", scrollView.contentOffset.y)
    }
}
