//
//  ViewController.swift
//  CHOOCH
//
//  Created by Vasundhara Vision on 27/09/17.
//  Copyright © 2017 Vasundhara Vision. All rights reserved.
//

import UIKit
import CoreMedia
import AFNetworking
import AVFoundation
import Foundation
import CoreLocation
import MZFormSheetPresentationController
import Toast_Swift
import FacebookLogin
import FBSDKLoginKit
import FacebookCore
import GoogleSignIn
import SDWebImage
import AVKit


var startSession = true


var predictionArray = NSMutableArray()
var facesArray = NSMutableArray()

class ViewController: UIViewController , CLLocationManagerDelegate , UIImagePickerControllerDelegate , UINavigationControllerDelegate , UIGestureRecognizerDelegate {
    
    // MARK: - * Outlates *
    
    @IBOutlet weak var videoPreview: UIView!
    
    @IBOutlet var tabicon_flash: UIImageView!
    @IBOutlet var tabicon_camera: UIImageView!
    @IBOutlet var tabicon_garbage: UIImageView!
    @IBOutlet var tabicon_feedback: UIImageView!
    @IBOutlet var tabicon_logout: UIImageView!
    
    @IBOutlet var Tabbar_FlashBtn: UIButton!
    @IBOutlet var Tabbar_FeedbackBtn: UIButton!
    @IBOutlet var Tabbar_LogoutBtn: UIButton!
    
    @IBOutlet var Gallary_ImgView: UIImageView!
    
    @IBOutlet var loaderImgView: UIImageView!
    
    @IBOutlet var SigninView: UIView!
    
    @IBOutlet var Gradientvyu: UIView!
    // MARK: - * Variables *
    
    var lastimage = UIImage()
    
    var isGallary = false
    
    var videoCapture: VideoCapture!
    var device: MTLDevice!
    var commandQueue: MTLCommandQueue!
    var startupGroup = DispatchGroup()
    let fpsCounter = FPSCounter()
    
    var y = CGFloat()
    var x = CGFloat()
    
    let scroll1 = UIScrollView()
    let viewforScroll = UIView()
    
    
    var currentLat = String()
    var currentLong = String()
    
    var locationManager:CLLocationManager!
    
    var predictionContainer = UIView()
    
    //    var cameraSwitchButton = UIButton()
    
    var metadataDict = NSMutableDictionary()
    
    var tasks = [URLSession]()
    
    var ToastIMg = UIImageView(image: UIImage(named: "PoorConnection"))
    
    var poorconnectionToastLbl = UILabel ()
    
    //    var oldversionBtn = UIButton(type: UIButtonType.custom)
    
    var isRecording = true
    var istorchon = false
    
    let userId = "\(userDefault.value(forKey: custId) ?? "-1")"
    
    // MARK: - * Controller Methods *
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        
        for subview in predictionContainer.subviews
        {
            subview.removeFromSuperview()
        }
        for subview in viewforScroll.subviews
        {
            subview.removeFromSuperview()
        }
        
        if (isGallary && !Gallary_ImgView.isHidden) || !isRecording
        {
            
            //            scroll1.Getheight = (DeviceType.IS_IPHONE_X ? Gallary_ImgView.Getheight - screenStatusBarHeight - 20 : Gallary_ImgView.Getheight - screenStatusBarHeight)
            viewforScroll.Getheight = 0
            viewforScroll.Getwidth = scroll1.Getwidth
            
            y = 0
            
            self.setButtonPredictScrollview(myImage: self.lastimage)
        }
        else
        {
            setupPredictionView()
        }
        
        self.videoPreview.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height - 49)
        self.resizePreviewLayer()
        videoCapture.changeorietation(frame: self.view.bounds)
        
        
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            
            if fromInterfaceOrientation.isLandscape
            {
                //                self.cameraSwitchButton.frame = CGRect(x: UIScreen.main.bounds.size.width * 0.88, y: 44, width: 40, height: 40)
//                poorconnectionToastLbl.frame = CGRect(x: 0.0, y: (self.Gradientvyu.Gettop - self.view.Getwidth * 0.1), width: self.view.Getwidth, height: self.view.Getwidth * 0.1)
                
                //                oldversionBtn.frame = CGRect(x: 0.0, y: self.view.Getheight * 0.1, width: self.view.Getwidth * 0.95, height: 150)
                
            } else
            {
                //                self.cameraSwitchButton.frame = CGRect(x: UIScreen.main.bounds.size.width * 0.92, y: 20, width: 40, height: 40)
//                poorconnectionToastLbl.frame = CGRect(x: 0.0, y: (self.Gradientvyu.Gettop - self.view.Getheight * 0.1), width: self.view.Getwidth, height: self.view.Getheight * 0.1)
                
                //                oldversionBtn.frame = CGRect(x: 0.0, y: self.view.Getheight * 0.1, width: self.view.Getwidth * 0.6, height: 150)
            }
            
        }
        else
        {
            
            if fromInterfaceOrientation.isLandscape
            {
                //                self.cameraSwitchButton.frame = CGRect(x: UIScreen.main.bounds.size.width * 0.92, y: 20, width: 50, height: 50)
//                poorconnectionToastLbl.frame = CGRect(x: 0.0, y: (self.Gradientvyu.Gettop - self.view.Getwidth * 0.1), width: self.view.Getwidth, height: self.view.Getwidth * 0.1)
                //                oldversionBtn.frame = CGRect(x: 0.0, y: self.view.Getheight * 0.1, width: self.view.Getwidth * 0.75, height: 150)
            }else
            {
                //                self.cameraSwitchButton.frame = CGRect(x: UIScreen.main.bounds.size.width * 0.92, y: 44, width: 50, height: 50)
//                poorconnectionToastLbl.frame = CGRect(x: 0.0, y: (self.Gradientvyu.Gettop - self.view.Getheight * 0.1), width: self.view.Getwidth, height: self.view.Getheight * 0.1)
                //                oldversionBtn.frame = CGRect(x: 0.0, y: self.view.Getheight * 0.1, width: self.view.Getwidth * 0.6, height: 150)
            }
            
        }
        
//        poorconnectionToastLbl.center.x = self.view.center.x
//        poorconnectionToastLbl.font = UIFont.init(name: CustomFontWeight.bold, size: poorconnectionToastLbl.Getheight * 0.5)
        //        oldversionBtn.center.x = self.view.center.x
        
        
    }
    
    // MARK: - * ViewController delegate Methods *
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        (UIApplication.shared.delegate as! AppDelegate).restrictRotation = .all
        appDelegate.restrictRotation = .all
        determineMyCurrentLocation()
        
        Gallary_ImgView.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(StartRecording), name: NSNotification.Name(rawValue: "load"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(returnImagePicker), name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ChangeOrientation), name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(becameactive), name: NSNotification.Name(rawValue: "becameActive"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openMenu), name: NSNotification.Name(rawValue: "openMenu"), object: nil)
        
        device = MTLCreateSystemDefaultDevice()
        if device == nil {
            print("Error: this device does not support Metal")
            return
            
        }
        commandQueue = device.makeCommandQueue()
        // NOTE: At this point you'd disable the UI and show a spinner.
        videoCapture = VideoCapture(device: device)
        
        //        ZoomImageView
        
        videoCapture.delegate = self
        
        // Initialize the camera.
        startupGroup.enter()
        
        videoCapture.setUp { success in
            // Add the video preview into the UI.
            if let previewLayer = self.videoCapture.previewLayer {
                self.videoPreview.layer.addSublayer(previewLayer)
                self.videoPreview.frame = self.view.bounds
                self.resizePreviewLayer()
            }
            self.startupGroup.leave()
        }
        
        startupGroup.notify(queue: .main)
        {
            self.fpsCounter.start()
            self.videoCapture.start()
        }
        
        if UIDevice().userInterfaceIdiom == .phone {
            if DeviceType.IS_IPHONE_X
            {
                print("iPhone X")
                self.predictionContainer = UIView.init(frame: CGRect(x: 0, y: self.view.Getwidth - 44, width: self.view.Getwidth, height: 20))
                self.predictionContainer.clipsToBounds = true
            }
            else
            {
                print("unknown")
                self.predictionContainer = UIView.init(frame: CGRect(x: 0, y: self.view.Getwidth - 20, width: self.view.Getwidth, height: 20))
                self.predictionContainer.clipsToBounds = true
            }
        }
        else
        {
            self.predictionContainer = UIView.init(frame: CGRect(x: 0, y: self.view.Getwidth - 44, width: self.view.Getwidth, height: 20))
            self.predictionContainer.clipsToBounds = true
        }

        self.view.insertSubview(self.poorconnectionToastLbl, aboveSubview: self.videoPreview)
        
        //        cameraSwitchButton.addTarget(self, action: #selector(switchButtonAction(_:)), for: .touchUpInside)
        //        cameraSwitchButton.setBackgroundImage(#imageLiteral(resourceName: "ic_switch_camera"), for: .normal)//setImage(#imageLiteral(resourceName: "ic_switch_camera"), for: .normal)
        //        cameraSwitchButton.layer.cornerRadius = cameraSwitchButton.Getheight/2
        //        cameraSwitchButton.clipsToBounds = true
        
        
        ToastIMg.contentMode = .scaleAspectFit
        ToastIMg.center.x = videoPreview.center.x
        ToastIMg.alpha = 0.0
        
        self.view.insertSubview(ToastIMg, aboveSubview: videoPreview)
        
        
        loaderImgView.center.x = videoPreview.center.x
        let image = UIImage.gifImageWithName("Infinity-1")
        loaderImgView.image = image
        loaderImgView.isHidden = true
        
        //        oldversionBtn.setImage(UIImage(named: "OldVersionMsg"), for: .normal)
        //        oldversionBtn.addTarget(self, action: #selector(self.openAppstore), for: .touchUpInside)
        //        oldversionBtn.imageView?.contentMode = .scaleAspectFit
        //        oldversionBtn.center.x = videoPreview.center.x
        //        oldversionBtn.alpha = 0.0
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(playAndPause(_:)))
        let tap1 = UITapGestureRecognizer.init(target: self, action: #selector(playAndPause(_:)))
        videoPreview.addGestureRecognizer(tap)
        predictionContainer.addGestureRecognizer(tap1)
        
        let pinch = UIPinchGestureRecognizer.init(target: self, action: #selector(zoom_camera(_:)))
        let pinch2 = UIPinchGestureRecognizer.init(target: self, action: #selector(zoom_camera(_:)))
        let pinch3 = UIPinchGestureRecognizer.init(target: self, action: #selector(pinchHandler(_:)))
        let pinch4 = UIPinchGestureRecognizer.init(target: self, action: #selector(pinchHandler(_:)))
        let pan5 = UIPanGestureRecognizer.init(target: self, action: #selector(GallaryPanHandler(_:)))
        pan5.minimumNumberOfTouches = 2
        let pan6 = UIPanGestureRecognizer.init(target: self, action: #selector(GallaryPanHandler(_:)))
        pan6.minimumNumberOfTouches = 2
        
        videoPreview.addGestureRecognizer(pinch)
        predictionContainer.addGestureRecognizer(pinch2)
        Gallary_ImgView.addGestureRecognizer(pinch3)
        scroll1.addGestureRecognizer(pinch4)
        Gallary_ImgView.addGestureRecognizer(pan5)
        scroll1.addGestureRecognizer(pan6)
        
        videoPreview.isUserInteractionEnabled = true
        predictionContainer.isUserInteractionEnabled = true
        Gallary_ImgView.isUserInteractionEnabled = true
        scroll1.isUserInteractionEnabled = true
        
        self.view.addSubview(predictionContainer)
        
        
        scroll1.frame = self.view.frame
        scroll1.Getheight = 0.0
        scroll1.Gety = screenStatusBarHeight
        self.view.insertSubview(scroll1, aboveSubview: predictionContainer)
        
        viewforScroll.frame = CGRect(x: 0, y: 0, width: self.view.Getwidth, height: 0)
        scroll1.addSubview(viewforScroll)
        
        self.Tabbar_FeedbackBtn.backgroundColor = self.userId.trim() != "-1" ? .clear : hexStringToUIColor(hex: "202020").withAlphaComponent(0.75)
        
        counter = true
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        appDelegate.restrictRotation = .all
        
        if Reachability123.isConnectedToNetwork()
        {
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
                if response {
                    //access granted
                } else {
                    self.presentAlertWithTitle(title: "Attention!", message: "Camera usage is required to get predictions", options: "Go to settings", completion: { (IndexAction) in
                        if IndexAction == 0 {
                            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!)
                        }
                    })
                    
                }
            }
        }
        else
        {
            
            self.poorconnectionToastLbl.text = "no connection"
            self.poorconnectionToastLbl.fadeIn()
        }
    }
    
    override func viewDidLayoutSubviews() {
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            
            if UIApplication.shared.statusBarOrientation.isLandscape
            {
                //                cameraSwitchButton = UIButton.init(frame: CGRect(x: self.view.Getwidth - 50, y: 20, width: 40, height: 40))
                self.poorconnectionToastLbl.frame = CGRect(x: 0.0, y: (self.Gradientvyu.Gettop - self.view.Getheight * 0.1), width: self.view.Getwidth, height: self.view.Getheight * 0.1)
                //                oldversionBtn.frame = CGRect(x: 0.0, y: self.view.Getheight * 0.15, width: self.view.Getwidth * 0.6, height: 150)
            }
            else
            {
                //                cameraSwitchButton = UIButton.init(frame: CGRect(x: self.view.Getwidth - 50, y: 44, width: 40, height: 40))
                self.poorconnectionToastLbl.frame = CGRect(x: 0.0, y: (self.Gradientvyu.Gettop - self.view.Getwidth * 0.1), width: self.view.Getwidth, height: self.view.Getwidth * 0.1)
                //                oldversionBtn.frame = CGRect(x: 0.0, y: self.view.Getheight * 0.15, width: self.view.Getwidth * 0.95, height: 150)
            }
            
        }
        else
        {
            if UIApplication.shared.statusBarOrientation.isLandscape
            {
                //                cameraSwitchButton = UIButton.init(frame: CGRect(x: self.view.Getwidth - 60, y: 20, width: 50, height: 50))
                self.poorconnectionToastLbl.frame = CGRect(x: 0.0, y: (self.Gradientvyu.Gettop - self.view.Getheight * 0.1), width: self.view.Getwidth, height: self.view.Getheight * 0.1)
                //                oldversionBtn.frame = CGRect(x: 0.0, y: self.view.Getheight * 0.15, width: self.view.Getwidth * 0.6, height: 150)
            }
            else
            {
                //                cameraSwitchButton = UIButton.init(frame: CGRect(x: self.view.Getwidth - 60, y: 44, width: 50, height: 50))
                self.poorconnectionToastLbl.frame = CGRect(x: 0.0, y: (self.Gradientvyu.Gettop - self.view.Getwidth * 0.1), width: self.view.Getwidth, height: self.view.Getwidth * 0.1)
                //                oldversionBtn.frame = CGRect(x: 0.0, y: self.view.Getheight * 0.15, width: self.view.Getwidth * 0.75, height: 150)
            }
        }
        
        self.poorconnectionToastLbl.font = UIFont.init(name: CustomFontWeight.bold, size: self.poorconnectionToastLbl.Getheight * 0.5)
        self.poorconnectionToastLbl.textAlignment = .center
        self.poorconnectionToastLbl.center.x = self.view.center.x
        self.poorconnectionToastLbl.alpha = 0.0
        //        self.poorconnectionToastLbl.layer.cornerRadius = 10
        self.poorconnectionToastLbl.layer.masksToBounds = true
        self.poorconnectionToastLbl.backgroundColor = UIColor.white.withAlphaComponent(0.75)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //        guard let path = Bundle.main.path(forResource: "ChoochLogo", ofType: "mov") else {
        //            return
        //        }
        //        let videoURL = NSURL(fileURLWithPath: path)
        //
        //        // Create an AVPlayer, passing it the local video url path
        //        let player = AVPlayer(url: videoURL as URL)
        //        let controller = AVPlayerViewController()
        //        controller.player = player
        //        self.present(controller, animated: true) {
        //            player.play()
        //        }
        
        if !startSession
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                self.isRecording = true
                self.isGallary = false
                self.videoCapture.start()
                counter = true
            }
        }
        
        
    }
    
    
    
    @objc func switchButtonAction(_ sender: UIButton) {
        
        if !usingFrontCamera
        {
            
            usingFrontCamera = true
            device = MTLCreateSystemDefaultDevice()
            if device == nil {
                print("Error: this device does not support Metal")
                return
                
            }
            commandQueue = device.makeCommandQueue()
            // NOTE: At this point you'd disable the UI and show a spinner.
            videoCapture = VideoCapture(device: device)
            
            videoCapture.delegate = self as VideoCaptureDelegate
            
            // Initialize the camera.
            startupGroup.enter()
            
            videoCapture.setUp { success in
                // Add the video preview into the UI.
                if let previewLayer = self.videoCapture.previewLayer {
                    self.videoPreview.layer.addSublayer(previewLayer)
                    self.videoPreview.frame = self.view.bounds
                    self.resizePreviewLayer()
                }
                self.startupGroup.leave()
            }
            
            startupGroup.notify(queue: .main)
            {
                self.fpsCounter.start()
                self.videoCapture.start()
            }
            
        }
        else
        {
            usingFrontCamera = false
            device = MTLCreateSystemDefaultDevice()
            if device == nil {
                print("Error: this device does not support Metal")
                return
                
            }
            commandQueue = device.makeCommandQueue()
            // NOTE: At this point you'd disable the UI and show a spinner.
            videoCapture = VideoCapture(device: device)
            
            
            videoCapture.delegate = self as  VideoCaptureDelegate
            
            // Initialize the camera.
            startupGroup.enter()
            
            videoCapture.setUp { success in
                // Add the video preview into the UI.
                if let previewLayer = self.videoCapture.previewLayer {
                    self.videoPreview.layer.addSublayer(previewLayer)
                    self.videoPreview.frame = self.view.bounds
                    self.resizePreviewLayer()
                }
                self.startupGroup.leave()
            }
            
            startupGroup.notify(queue: .main)
            {
                self.fpsCounter.start()
                self.videoCapture.start()
            }
        }
    }
    
    // MARK: - * Gesture Handler *
    
    
    @objc func playAndPause(_ sender: UITapGestureRecognizer)
    {
        if !isGallary /* && oldversionBtn.alpha != 1.0 */ {
            
            let tap1 = UITapGestureRecognizer.init(target: self, action: #selector(playAndPause(_:)))
            
            if isRecording
            {
                let PausedImg = UIImageView(frame: CGRect(x: 0.0, y: self.view.Getheight * 0.25, width: 180, height: 100))
                PausedImg.contentMode = .scaleAspectFit
                PausedImg.center.x = videoPreview.center.x
                PausedImg.image = UIImage(named: "PauseBtn")
                videoPreview.addSubview(PausedImg)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0)
                {
                    PausedImg.fadeOut()
                }
                
                if let device1 = AVCaptureDevice.default(for: AVMediaType.video) {
                    
                    if (device1.hasTorch) {
                        istorchon = device1.isTorchActive
                    }
                }
                
                isRecording = false
                videoCapture.stop()
                scroll1.addGestureRecognizer(tap1)
                counter = true
                self.view.bringSubview(toFront: predictionContainer)
                callAPi(image: lastimage, tag: "isRecording")
            }
            else
            {
                for subview in predictionContainer.subviews
                {
                    subview.removeFromSuperview()
                }
                for buttons in viewforScroll.subviews {
                    buttons.removeFromSuperview()
                }
                y = 0
                scroll1.Getheight = 0
                
                isGallary = false
                isRecording = true
                counter = true
                videoCapture.start()
                scroll1.removeGestureRecognizer(tap1)
                if istorchon
                {
                    if let device = AVCaptureDevice.default(for: AVMediaType.video) {
                        
                        if (device.hasTorch) {
                            do {
                                try device.lockForConfiguration()
                                device.torchMode = AVCaptureDevice.TorchMode.on
                                device.unlockForConfiguration()
                                istorchon = false
                            } catch {
                                print(error.localizedDescription)
                            }
                        }
                    }
                    
                }
            }
        }
        
    }
    
    
    @objc private func pinchHandler(_ gesture: UIPinchGestureRecognizer)
    {
        
        switch gesture.state {
        case .began:
            lastScale = gesture.scale
            print("began")
            predictionArray.removeAllObjects()
            for subview in predictionContainer.subviews
            {
                
                subview.fadeOut()
            }
            for buttons in viewforScroll.subviews {
                buttons.fadeOut()
            }
            y = 0
            
            
        case .changed:
            
            if Gallary_ImgView.Getx > self.view.Getx || Gallary_ImgView.Gety > self.view.Gety || Gallary_ImgView.Getright < self.view.Getright || Gallary_ImgView.Getbottom < self.view.Getbottom - 50
            {
                UIView.animate(withDuration: 0.2, animations: {
                    self.Gallary_ImgView.transform = CGAffineTransform.identity
                    self.Gallary_ImgView.Getx = 0.0
                    self.Gallary_ImgView.Gety = 0.0
                })
                return
            }
            
            let currentScale = Gallary_ImgView.layer.value(forKeyPath:"transform.scale")! as! CGFloat
            
            let kMaxScale:CGFloat = 3.0
            let kMinScale:CGFloat = 1.0
            
            var newScale = 1 -  (lastScale - gesture.scale)
            newScale = min(newScale, kMaxScale / currentScale)
            newScale = max(newScale, kMinScale / currentScale)
            
            let pinchCenter = CGPoint(x: gesture.location(in: gesture.view).x - gesture.view!.bounds.midX,
                                      y: gesture.location(in: gesture.view).y - gesture.view!.bounds.midY)
            
            let transform = Gallary_ImgView.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y)
                .scaledBy(x: newScale, y: newScale)
                .translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
            
            Gallary_ImgView.transform = transform
            
            //                gesture.scale = 1
            lastScale = gesture.scale
            
        case .ended:
            print("Ended")
            
            counter = true
            self.lastimage = cropVisiblePortionOf(Gallary_ImgView)!//(Gallary_ImgView.image?.crop(to: CGSize(width: 200, height: 300)))!
            callAPi(image: self.lastimage, tag: "PinchedImage")
            
        default:
            return
        }
    }
    
    
    @objc private func GallaryPanHandler(_ sender: UIPanGestureRecognizer)
    {
        
        switch sender.state {
        case .began:
            print("Begun")
            predictionArray.removeAllObjects()
            for subview in predictionContainer.subviews
            {
                
                subview.fadeOut()
            }
            for buttons in viewforScroll.subviews {
                buttons.fadeOut()
            }
            y = 0
            
        case .changed:
            print("Changed")
            
            let translation = sender.translation(in: self.view)
            Gallary_ImgView!.center = CGPoint(x: Gallary_ImgView!.center.x + translation.x, y: Gallary_ImgView!.center.y + translation.y)
            sender.setTranslation(CGPoint.zero, in: self.view)
            
        case .ended:
            print("Ended")
            counter = true
            self.lastimage = cropVisiblePortionOf(Gallary_ImgView)!//(Gallary_ImgView.image?.crop(to: CGSize(width: 200, height: 300)))!
            callAPi(image: self.lastimage, tag: "PannedImage")
        default:
            return
        }
    }
    
    
    
    @objc func zoom_camera(_ sender:UIPinchGestureRecognizer)
    {
        
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Error: no video devices available")
            return
        }
        
        let device = captureDevice
        let format = device.activeFormat as AVCaptureDevice.Format
        var _: CGFloat? = format.videoMaxZoomFactor
        _ = device.formats
        let pinchVelocityDividerFactor: CGFloat = 10.0
        if sender.state == .changed || sender.state == .began {
            let error: Error? = nil
            if try! device.lockForConfiguration() != nil {
                let desiredZoomFactor: CGFloat = (device.videoZoomFactor) + CGFloat(atan2f(Float(sender.velocity), Float(pinchVelocityDividerFactor)))
                device.videoZoomFactor = max(1.0, min(desiredZoomFactor, (device.activeFormat.videoMaxZoomFactor)))
                device.unlockForConfiguration()
            }
            else {
                print("error: \(String(describing: error))")
            }
        }
    }
    
    @objc func longPress(gesture: UILongPressGestureRecognizer) {
        if gesture.state == UIGestureRecognizerState.began {
            
            let btn = gesture.view as! MyButton
            
            copyToClipBoard(textToCopy: (btn.titleLabel?.text!)!, btn: btn)
        }
    }
    
    @objc func removetapped(gesture: UITapGestureRecognizer) {
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        // Prevent subviews of a specific view to send touch events to the view's gesture recognizers.
        if let touchedView = touch.view, let gestureView = gestureRecognizer.view, touchedView.isDescendant(of: gestureView), touchedView !== gestureView {
            return false
        }
        return true
    }
    
    @objc func openMenu()
    {
        let btn = UIButton()
        self.logOut_Pressed(btn)
        
    }
    
    @objc func becameactive()
    {
        self.tabicon_flash.image = UIImage(named: "ic_flash")
        poorconnectionToastLbl.fadeOutPoorConnection()
        
    }
    
    @objc func ChangeOrientation()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            self.videoPreview.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height - 49)
            self.resizePreviewLayer()
            self.videoCapture.changeorietation(frame: self.view.bounds)
        }
    }
    
    @objc func StartRecording()
    {
        if !startSession
        {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
            {
                
                self.isRecording = true
                self.isGallary = false
                self.videoCapture.start()
                counter = true
            }
        }
    }
    
    @objc func returnImagePicker()
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            for subview in self.predictionContainer.subviews
            {
                subview.removeFromSuperview()
            }
            for subview in self.viewforScroll.subviews
            {
                subview.removeFromSuperview()
            }
            
            self.scroll1.Getheight = 0
            
            
        }
        
    }
    
    
    func resizePreviewLayer() {
        videoCapture.previewLayer?.frame = videoPreview.bounds
    }
    
    
    
    func getDocumentsDirectory() -> URL {
        let paths = URL(fileURLWithPath: NSTemporaryDirectory())
        return paths
    }
    
    func setupPredictionView() {
        if predictionContainer.Gety == 0 {
            return
        }
        for i in 0..<predictionArray.count {
            //            print("\(String(describing: (predictionArray.object(at: i) as AnyObject).value(forKey: "classvalue")))")
            if ((predictionArray.object(at: i) as AnyObject).value(forKey: "classvalue") as AnyObject).isEqual("") {
                
            }
            else {
                
                let classValue = (predictionArray[i] as AnyObject).value(forKey: "classvalue") as? String
                
                let array = classValue?.components(separatedBy: ",")
                
                if array!.count > 1
                {
                    let x1: CGFloat = view.Getwidth - (view.Getwidth / 2)
                    let width1 = (view.Getwidth / 2) - 60
                    
                    
                    var row_width = CGFloat()
                    var newX = view.Getwidth
                    
                    var isusernameAdded = false
                    
                    for j in 0..<array!.count
                    {
                        
                        let button = MyButton.init(frame: CGRect(x: x1, y: y, width: width1, height: 40))
                        button.setTitle("  \(array![j])  ", for: .normal)
                        
                        let strclass = (predictionArray.object(at: i) as! NSDictionary).value(forKey: "classid") as! String
                        
                        button.id = strclass.replacingOccurrences(of: " ", with: "")
                        button.params = (predictionArray.object(at: i) as! NSDictionary)
                        
                        button.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.5)
                        button.setTitleColor(UIColor.white, for: .normal)
                        button.layer.cornerRadius = button.Getheight / 2
                        if !isGallary
                        {
                            predictionContainer.addSubview(button)
                        }
                        else
                        {
                            viewforScroll.addSubview(button)
                        }
                        button.isUserInteractionEnabled = true
                        button.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 16)
                        
                        button.sizeToFit()
                        
                        
                        var button_width = CGFloat()
                        if button.Getwidth < 100
                        {
                            button_width = 100;
                        }
                        else
                        {
                            button_width = button.Getwidth
                        }
                        
                        if (button_width + 10) > (predictionContainer.Getwidth - row_width) && j == (array?.count)! - 1
                        {
                            y = y + button.Getheight + 20
                            newX = view.Getwidth
                            button.frame = CGRect(x: newX - (button_width + 10), y: y, width: button_width, height: 40)
                            newX = button.Getx
                            y = y + button.Getheight + 10
                            row_width = 0
                            row_width = row_width + button_width + 10
                            
                        }
                        else if (button_width + 10) > (predictionContainer.Getwidth - row_width)
                        {
                            y = y + button.Getheight + 20
                            newX = view.Getwidth
                            button.frame = CGRect(x: newX - (button_width + 10), y: y, width: button_width, height: 40)
                            newX = button.Getx
                            row_width = 0
                            row_width = row_width + button_width + 10
                        }
                        else if j == array!.count - 1 {
                            
                            button.frame = CGRect(x: newX - (button_width + 10), y: y, width: button_width, height: 40)
                            
                            newX = button.Getx
                            y = y + button.Getheight + 10
                            
                            row_width = row_width + button_width + 10
                        }
                        else
                        {
                            button.frame = CGRect(x: newX - (button_width + 10), y: y, width: button_width, height: 40)
                            newX = button.Getx //button.Getx - 10
                            //                            print(newX)
                            row_width = row_width + button_width + 10
                        }
                        
                        if !isusernameAdded
                        {
                            isusernameAdded = self.addUsernameTag(sender: button, isdense: false)
                        }
                        else
                        {
                            print(self.addUsernameTag(sender: button, isdense: false))
                        }
                        
                        button.transform =  CGAffineTransform(scaleX: 0.0, y: 0.0)
                        
                        
                        UIView.animate(withDuration: 1.0,
                                       delay: 0,
                                       usingSpringWithDamping: 0.5,
                                       initialSpringVelocity: 1.0,
                                       options: .allowUserInteraction,
                                       animations: { [weak self] in
                                        
                                        button.transform = .identity
                            },
                                       completion: nil)
                        
                        button.addTarget(self, action: #selector(self.openInsafari), for: .touchUpInside)
                        
                        //                        print("Y :--------------------> \(y)")
                        
                        if UIDevice().userInterfaceIdiom == .phone {
                            
                            if DeviceType.IS_IPHONE_X
                            {
                                print("iPhone X")
                                predictionContainer.frame = CGRect(x: 0, y: view.Getheight - (y+34) - 49, width: view.Getwidth, height: y)
                                viewforScroll.frame = CGRect(x: 0, y: 0, width: view.Getwidth, height: y)
                            }
                            else
                            {
                                print("unknown")
                                predictionContainer.frame = CGRect(x: 0, y: view.Getheight - y - 49, width: view.Getwidth, height: y)
                                viewforScroll.frame = CGRect(x: 0, y: 0, width: view.Getwidth, height: y)
                            }
                            
                        }
                        else
                        {
                            predictionContainer.frame = CGRect(x: 0, y: view.Getheight - y - 49, width: view.Getwidth, height: y)
                            viewforScroll.frame = CGRect(x: 0, y: 0, width: view.Getwidth, height: y)
                        }
                        
                        
                        
                    }
                    
                    y += (isusernameAdded ? 8 : 0) //uncomment this for username tag
                    
                }
                else
                {
                    //                    print("Y :--------------------> \(y)")
                    let button = MyButton.init(frame: CGRect(x: view.Getwidth - ((view.Getwidth - 10) / 2), y: y, width: (view.Getwidth / 2) - 60, height: 40))
                    
                    let title = String(describing: (predictionArray.object(at: i) as AnyObject).value(forKey: "classvalue")!)
                    
                    button.setTitle("  \(title)  ", for: .normal)
                    //                    button.setTitle("  dsfsfgkmrg jaerkltj gkldfgj  ", for: .normal)
                    
                    let strclass = (predictionArray.object(at: i) as! NSDictionary).value(forKey: "classid") as! String
                    
                    button.id = strclass.replacingOccurrences(of: " ", with: "")
                    button.params = (predictionArray.object(at: i) as! NSDictionary)
                    
                    button.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.5)
                    button.setTitleColor(UIColor.white, for: .normal)
                    button.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 16)
                    button.layer.cornerRadius = button.Getheight / 2
                    if !isGallary
                    {
                        predictionContainer.addSubview(button)
                    }
                    else
                    {
                        viewforScroll.addSubview(button)
                    }
                    button.isUserInteractionEnabled = true
                    button.addTarget(self, action: #selector(openInsafari(_:)), for: .touchUpInside)
                    button.sizeToFit()
                    
                    var button_width = CGFloat()
                    if button.Getwidth < 100
                    {
                        button_width = 100;
                    }
                    else
                    {
                        button_width = button.Getwidth
                    }
                    button.frame = CGRect(x: view.Getwidth - (button_width + 10), y: y, width: button_width, height: 40)
                    
                    let isusername = self.addUsernameTag(sender: button, isdense: false)
                    
                    button.transform =  CGAffineTransform(scaleX: 0.0, y: 0.0)
                    
                    UIView.animate(withDuration: 1.0,
                                   delay: 0,
                                   usingSpringWithDamping: 0.5,
                                   initialSpringVelocity: 1.0,
                                   options: .allowUserInteraction,
                                   animations: { [weak self] in
                                    
                                    button.transform = .identity
                        },
                                   completion: nil)
                    
                    y = y + button.Getheight + (isusername ? 18 : 10) //uncomment this for username tag
                    
                    if UIDevice().userInterfaceIdiom == .phone {
                        
                        if DeviceType.IS_IPHONE_X
                        {
                            print("iPhone X")
                            predictionContainer.frame = CGRect(x: 0, y: view.Getheight - (y+34) - 49, width: view.Getwidth, height: y)
                            viewforScroll.frame = CGRect(x: 0, y: 0, width: view.Getwidth, height: y)
                        }
                        else
                        {
                            print("unknown")
                            predictionContainer.frame = CGRect(x: 0, y: view.Getheight - y - 49, width: view.Getwidth, height: y)
                            viewforScroll.frame = CGRect(x: 0, y: 0, width: view.Getwidth, height: y)
                        }
                        
                        
                    }
                    else
                    {
                        predictionContainer.frame = CGRect(x: 0, y: view.Getheight - y - 49, width: view.Getwidth, height: y)
                        viewforScroll.frame = CGRect(x: 0, y: 0, width: view.Getwidth, height: y)
                    }
                }
                
                
                if predictionContainer.Gety <= (DeviceType.IS_IPHONE_X ? self.view.Getheight * 0.89 : self.view.Getheight * 0.95)
                {
                    var previous_btn_y = CGFloat()
                    
                    
                    if !isGallary
                    {
                        for button in predictionContainer.subviews
                        {
                            
                            if button.Gety < predictionContainer.Getheight - (DeviceType.IS_IPHONE_X ? self.view.Getheight * 0.89 : self.view.Getheight - 62)
                            {
                                if previous_btn_y != button.Gety
                                {
                                    previous_btn_y = button.Gety
                                    button.removeFromSuperview()
                                }
                                else
                                {
                                    button.removeFromSuperview()
                                }
                            }
                            
                        }
                        
                    }
                    else if isGallary && !Gallary_ImgView.isHidden
                    {
                        
                        scroll1.Getwidth = Gallary_ImgView.bounds.width
                        scroll1.Getheight = (UIDevice.current.orientation.isLandscape ? Gallary_ImgView.bounds.height : Gallary_ImgView.bounds.height - screenStatusBarHeight)
                        
                        if DeviceType.IS_IPHONE_X
                        {
                            scroll1.Getheight = (UIDevice.current.orientation.isLandscape ? Gallary_ImgView.bounds.height - 20 : Gallary_ImgView.bounds.height - screenStatusBarHeight - 34)
                        }
                        
                        scroll1.Gety = screenStatusBarHeight
                        
                        if viewforScroll.Getheight < scroll1.Getheight
                        {
                            viewforScroll.Gety = scroll1.Getheight - viewforScroll.Getheight
                        }
                        scroll1.contentSize = viewforScroll.Getsize
                        
                    }
                    
                    
                }
                
            }
        }
    }
    
    func addUsernameTag(sender : MyButton , isdense : Bool) -> Bool {
        
        
        
        if let user_name = sender.params!.object(forKey: "trained_by") as? String
        {
            let button = MyButton.init(frame: CGRect(x: 0, y: 32, width: sender.Getwidth, height: 15))
            
            sender.userName = (user_name.trim().count != 0) ? user_name : "Unknown"
            
            let title = "by \(sender.userName ?? "Unknown")"
            //            let title = "by dsfsfgkmrg jaerkltj fs"
            button.setTitle("\(title)", for: .normal)
            
            //            button.Getwidth = sender.Getwidth
            
            //            if sender.Getwidth > (isdense ? 126.0 : 100.0)
            //            {
            if (button.titleLabel!.text!.width(withConstrainedHeight: 15, font: UIFont.init(name: CustomFontWeight.bold, size: 8)!)) < sender.Getwidth
            {
                button.Getwidth = button.titleLabel!.text!.width(withConstrainedHeight: 15, font: UIFont.init(name: CustomFontWeight.bold, size: 8)!) + 16.0
                button.center.x = sender.Getwidth * 0.5
            }
            else
            {
                button.Getwidth = sender.Getwidth
            }
            //            }
            
            button.backgroundColor = UIColor.white.withAlphaComponent(1.0)
            button.setTitleColor(.black, for: .normal)
            button.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 8)
            button.layer.cornerRadius = button.Getheight / 2
            button.titleLabel?.lineBreakMode = .byTruncatingTail
            button.contentEdgeInsets =  UIEdgeInsetsMake(8, 8, 8, 8)
            
            sender.addSubview(button)
            
            if isdense
            {
                button.Gety = 35
                sender.superview!.viewWithTag(900)?.backgroundColor = UIColor.red.withAlphaComponent(0.5)
                sender.backgroundColor = .clear
            }
            else
            {
                sender.backgroundColor = UIColor.red.withAlphaComponent(0.5)
            }
            
            
            button.isUserInteractionEnabled = true
            
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(usernameLongPressed(gesture:)))
            longPress.minimumPressDuration = 1.0
            button.addGestureRecognizer(longPress)
            
            return true
        }
        
        return false
    }
    
    
    func setButtonPredictScrollview(myImage : UIImage) {
        
        var BtnY : CGFloat = 10.0
        
        for i in viewforScroll.subviews
        {
            i.removeFromSuperview()
        }
        
        scroll1.Getwidth = self.view.frame.width
        scroll1.Getheight = (UIDevice.current.orientation.isLandscape ? Gallary_ImgView.bounds.height : Gallary_ImgView.bounds.height - screenStatusBarHeight)
        
        if predictionArray.count != 0 && viewforScroll.subviews.count <= 1 {
            if predictionArray[0] is NSDictionary
            {
                let Predcount = predictionArray.count
                
                for predic in 0..<Predcount {
                    
                    let dict = predictionArray[predic] as! NSDictionary
                    
                    if let cordinate = dict.object(forKey: "coordinates") as? String
                    {
                        
                        var strTitle : String?
                        
                        if let str = dict.object(forKey: "classvalue") as? String
                        {
                            strTitle = str
                        }
                        
                        let classValue = dict.value(forKey: "classvalue") as? String
                        
                        let array = classValue?.components(separatedBy: ",")
                        
                        if array!.count > 1
                        {
                            for j in 0..<array!.count
                            {
                                strTitle = array![j]
                                
                                var widthStr : CGFloat = 0.0
                                let font = UIFont.init(name: CustomFontWeight.bold, size: 15)!
                                
                                if strTitle != nil
                                {
                                    if strTitle!.last == " " {
                                        strTitle = strTitle!.chopSuffix(1)
                                    }
                                    if strTitle!.first == " " {
                                        strTitle = strTitle!.chopPrefix(1)
                                    }
                                    widthStr = strTitle!.width(withConstrainedHeight: 34.0, font: font)
                                }
                                
                                if widthStr < 84.0
                                {
                                    widthStr = 84
                                }
                                
                                let coordinateArr = cordinate.components(separatedBy: ",")
                                
                                let xAx = Double(coordinateArr[0])
                                let width1 = Double(coordinateArr[1])! - xAx!
                                let yAx = Double(coordinateArr[2])
                                let height1 = Double(coordinateArr[3])! - yAx!
                                
                                let rect = CGRect(x: xAx!, y: yAx!, width: width1, height: height1)
                                
                                let image123 = imageWithImage(image: myImage, croppedTo: rect)
                                
                                print(image123)
                                
                                let viewMain = UIView.init(frame: CGRect.init(x: scroll1.Getwidth - widthStr - 66.0, y: BtnY, width: widthStr + 62.0, height: 46.0))
                                viewMain.backgroundColor = .clear
                                
                                let viewSub = UIView.init(frame: CGRect.init(x: 3.0, y: 3.0, width: viewMain.Getwidth - 6.0, height: 40.0))
                                viewSub.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.5)
                                viewSub.tag = 900
                                viewSub.layer.cornerRadius = viewSub.Getheight / 2
                                
                                
                                let imgbtn = MyButton.init(frame: CGRect.init(x: 0.0, y: 0.0, width: 46.0, height: 46.0))
                                imgbtn.setImage(image123, for: .normal)
                                imgbtn.id = strTitle
                                imgbtn.imageView!.contentMode = .scaleAspectFill
                                imgbtn.addTarget(self, action: #selector(self.btnThumbnailTapped(_:)), for: .touchUpInside)
                                imgbtn.layer.cornerRadius = imgbtn.Getheight / 2
                                imgbtn.layer.borderColor = UIColor.white.cgColor
                                imgbtn.layer.borderWidth = 1.0
                                imgbtn.clipsToBounds = true
                                
                                
                                let button = MyButton.init(frame: CGRect.init(x: 43.0, y: 0.0, width: viewMain.Getwidth - 46, height: viewMain.Getheight))
                                button.backgroundColor = .clear
                                button.id = dict["classid"] as? String
                                button.params = dict
                                //                        button.tag = predic + 1
                                button.setTitle(strTitle, for: .normal)
                                button.titleLabel?.textAlignment = .center
                                button.titleLabel?.textColor = .white
                                button.titleLabel?.font = font
                                button.tag = 145
                                button.contentEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 8)
                                
                                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
                                longPress.minimumPressDuration = 1.0
                                button.addGestureRecognizer(longPress)
                                
                                //                    let strclass = dict["classid"] as! String
                                //                    button.id = strclass.replacingOccurrences(of: " ", with: "")
                                button.addTarget(self, action: #selector(self.openInsafari(_:)), for: .touchUpInside)
                                
                                viewforScroll.addSubview(viewMain)
                                viewMain.addSubview(viewSub)
                                viewMain.addSubview(imgbtn)
                                viewMain.addSubview(button)
                                
                                let isusername = self.addUsernameTag(sender: button, isdense: true)
                                BtnY += viewMain.Getheight + (isusername ? 15.0 : 7.0)
                                
                                viewMain.transform =  CGAffineTransform(scaleX: 0.0, y: 0.0)
                                
                                UIView.animate(withDuration: 1.0,
                                               delay: 0,
                                               usingSpringWithDamping: 0.5,
                                               initialSpringVelocity: 1.0,
                                               options: .allowUserInteraction,
                                               animations: { [weak self] in
                                                
                                                viewMain.transform = .identity
                                    },
                                               completion: nil)
                                
                                
                            }
                            
                        }
                        else{
                            
                            var widthStr : CGFloat = 0.0
                            let font = UIFont.init(name: CustomFontWeight.bold, size: 15)!
                            
                            if strTitle != nil
                            {
                                if strTitle!.last == " " {
                                    strTitle = strTitle!.chopSuffix(1)
                                }
                                if strTitle!.first == " " {
                                    strTitle = strTitle!.chopPrefix(1)
                                }
                                widthStr = strTitle!.width(withConstrainedHeight: 34.0, font: font)
                            }
                            
                            if widthStr < 84.0
                            {
                                widthStr = 84
                            }
                            
                            let coordinateArr = cordinate.components(separatedBy: ",")
                            
                            let xAx = Double(coordinateArr[0])
                            let width1 = Double(coordinateArr[1])! - xAx!
                            let yAx = Double(coordinateArr[2])
                            let height1 = Double(coordinateArr[3])! - yAx!
                            
                            let rect = CGRect(x: xAx!, y: yAx!, width: width1, height: height1)
                            
                            let image123 = imageWithImage(image: myImage, croppedTo: rect)
                            
                            print(image123)
                            
                            let viewMain = UIView.init(frame: CGRect.init(x: scroll1.Getwidth - widthStr - 66.0, y: BtnY, width: widthStr + 62.0, height: 46.0))
                            viewMain.backgroundColor = .clear
                            
                            let viewSub = UIView.init(frame: CGRect.init(x: 3.0, y: 3.0, width: viewMain.Getwidth - 6.0, height: 40.0))
                            viewSub.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.5)
                            viewSub.tag = 900
                            viewSub.layer.cornerRadius = viewSub.Getheight / 2
                            
                            
                            let imgbtn = MyButton.init(frame: CGRect.init(x: 0.0, y: 0.0, width: 46.0, height: 46.0))
                            imgbtn.setImage(image123, for: .normal)
                            imgbtn.id = strTitle
                            imgbtn.imageView!.contentMode = .scaleAspectFill
                            imgbtn.addTarget(self, action: #selector(self.btnThumbnailTapped(_:)), for: .touchUpInside)
                            imgbtn.layer.cornerRadius = imgbtn.Getheight / 2
                            imgbtn.layer.borderColor = UIColor.white.cgColor
                            imgbtn.layer.borderWidth = 1.0
                            imgbtn.clipsToBounds = true
                            
                            let button = MyButton.init(frame: CGRect.init(x: 43.0, y: 0.0, width: viewMain.Getwidth - 46, height: viewMain.Getheight))
                            button.backgroundColor = .clear
                            button.id = dict["classid"] as? String
                            button.params = dict
                            //                        button.tag = predic + 1
                            button.setTitle(strTitle, for: .normal)
                            button.titleLabel?.textAlignment = .center
                            button.titleLabel?.textColor = .white
                            button.titleLabel?.font = font
                            button.tag = 145
                            button.contentEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 8)
                            
                            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
                            longPress.minimumPressDuration = 1.0
                            button.addGestureRecognizer(longPress)
                            
                            //                    let strclass = dict["classid"] as! String
                            //                    button.id = strclass.replacingOccurrences(of: " ", with: "")
                            button.addTarget(self, action: #selector(self.openInsafari(_:)), for: .touchUpInside)
                            
                            viewforScroll.addSubview(viewMain)
                            viewMain.addSubview(viewSub)
                            viewMain.addSubview(imgbtn)
                            viewMain.addSubview(button)
                            
                            let isusername = self.addUsernameTag(sender: button, isdense: true)
                            BtnY += viewMain.Getheight + (isusername ? 15.0 : 7.0)
                            
                            viewMain.transform =  CGAffineTransform(scaleX: 0.0, y: 0.0)
                            
                            UIView.animate(withDuration: 1.0,
                                           delay: 0,
                                           usingSpringWithDamping: 0.5,
                                           initialSpringVelocity: 1.0,
                                           options: .allowUserInteraction,
                                           animations: { [weak self] in
                                            
                                            viewMain.transform = .identity
                                },
                                           completion: nil)
                        }
                    }
                    else if (dict.object(forKey: "classvalue") as? String) != "" {
                        
                        let classValue = dict.value(forKey: "classvalue") as? String
                        
                        let array = classValue?.components(separatedBy: ",")
                        
                        if array!.count > 1
                        {
                            for j in 0..<array!.count
                            {
                                var strTitle = array![j]
                                
                                var widthStr : CGFloat = 0.0
                                let font = UIFont.init(name: CustomFontWeight.bold, size: 15)!
                                
                                if strTitle.last == " " {
                                    strTitle = strTitle.chopSuffix(1)
                                }
                                if strTitle.first == " " {
                                    strTitle = strTitle.chopPrefix(1)
                                }
                                
                                widthStr = strTitle.width(withConstrainedHeight: 34.0, font: font)
                                
                                if widthStr < 84.0
                                {
                                    widthStr = 84
                                }
                                
                                let viewMain = UIView.init(frame: CGRect.init(x: scroll1.Getwidth - widthStr - 20.0, y: BtnY, width: widthStr + 16.0, height: 46.0))
                                viewMain.backgroundColor = .clear
                                
                                let viewSub = UIView.init(frame: CGRect.init(x: 0.0, y: 3.0, width: viewMain.Getwidth, height: 40.0))
                                viewSub.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.5)
                                viewSub.tag = 900
                                viewSub.layer.cornerRadius = viewSub.Getheight / 2
                                
                                let button = MyButton.init(frame: CGRect.init(x: 0.0, y: 0.0, width: viewMain.Getwidth, height: viewMain.Getheight))
                                button.backgroundColor = .clear
                                button.id = dict["classid"] as? String
                                button.params = dict
                                button.setTitle(strTitle, for: .normal)
                                button.titleLabel?.textAlignment = .center
                                button.titleLabel?.textColor = .white
                                button.titleLabel?.font = font
                                button.tag = predic + 1
                                button.contentEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 8)
                                
                                button.addTarget(self, action: #selector(self.openInsafari(_:)), for: .touchUpInside)
                                
                                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
                                longPress.minimumPressDuration = 1.0
                                button.addGestureRecognizer(longPress)
                                
                                viewforScroll.addSubview(viewMain)
                                viewMain.addSubview(viewSub)
                                viewMain.addSubview(button)
                                
                                let isusername = self.addUsernameTag(sender: button, isdense: true)
                                BtnY += viewMain.Getheight + (isusername ? 15.0 : 7.0)
                                
                                viewMain.transform =  CGAffineTransform(scaleX: 0.0, y: 0.0)
                                
                                UIView.animate(withDuration: 1.0,
                                               delay: 0,
                                               usingSpringWithDamping: 0.5,
                                               initialSpringVelocity: 1.0,
                                               options: .allowUserInteraction,
                                               animations: { [weak self] in
                                                
                                                viewMain.transform = .identity
                                    },
                                               completion: nil)
                            }
                            
                        }
                        else
                        {
                            var strTitle = dict.value(forKey: "classvalue") as? String
                            
                            var widthStr : CGFloat = 0.0
                            let font = UIFont.init(name: CustomFontWeight.bold, size: 15)!
                            
                            if strTitle != nil
                            {
                                if strTitle!.last == " " {
                                    strTitle = strTitle!.chopSuffix(1)
                                }
                                if strTitle!.first == " " {
                                    strTitle = strTitle!.chopPrefix(1)
                                }
                                widthStr = strTitle!.width(withConstrainedHeight: 34.0, font: font)
                            }
                            
                            //                            widthStr = strTitle!.width(withConstrainedHeight: 34.0, font: font)
                            
                            if widthStr < 84.0
                            {
                                widthStr = 84
                            }
                            
                            let viewMain = UIView.init(frame: CGRect.init(x: scroll1.Getwidth - widthStr - 20.0, y: BtnY, width: widthStr + 16, height: 46.0))
                            viewMain.backgroundColor = .clear
                            
                            let viewSub = UIView.init(frame: CGRect.init(x: 0.0, y: 3.0, width: viewMain.Getwidth, height: 40.0))
                            viewSub.backgroundColor = UIColor(red: 1, green: 0, blue: 0, alpha: 0.5)
                            viewSub.tag = 900
                            viewSub.layer.cornerRadius = viewSub.Getheight / 2
                            
                            
                            let button = MyButton.init(frame: CGRect.init(x: 0.0, y: 0.0, width: viewMain.Getwidth, height: viewMain.Getheight))
                            button.setTitle(strTitle, for: .normal)
                            button.titleLabel?.textColor = .white
                            button.titleLabel?.textAlignment = .center
                            button.backgroundColor = .clear
                            button.titleLabel?.font = font
                            button.contentEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 8)
                            
                            button.id = dict["classid"] as? String
                            button.params = dict
                            button.tag = predic + 1
                            
                            button.addTarget(self, action: #selector(self.openInsafari(_:)), for: .touchUpInside)
                            
                            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
                            longPress.minimumPressDuration = 1.0
                            button.addGestureRecognizer(longPress)
                            
                            viewforScroll.addSubview(viewMain)
                            viewMain.addSubview(viewSub)
                            viewMain.addSubview(button)
                            
                            let isusername = self.addUsernameTag(sender: button, isdense: true)
                            BtnY += viewMain.Getheight + (isusername ? 15.0 : 7.0)
                            
                            viewMain.transform =  CGAffineTransform(scaleX: 0.0, y: 0.0)
                            
                            UIView.animate(withDuration: 1.0,
                                           delay: 0,
                                           usingSpringWithDamping: 0.5,
                                           initialSpringVelocity: 1.0,
                                           options: .allowUserInteraction,
                                           animations: { [weak self] in
                                            
                                            viewMain.transform = .identity
                                },
                                           completion: nil)
                        }
                        
                        
                    }
                    
                    
                    
                }
            }
            
        }
        
        
        
        
        viewforScroll.frame = CGRect(x: 0.0, y: 0.0, width: scroll1.Getwidth, height: CGFloat(BtnY))
        
        scroll1.contentSize = viewforScroll.Getsize
        
        
        if DeviceType.IS_IPHONE_X
        {
            scroll1.Getheight = (UIDevice.current.orientation.isLandscape ? Gallary_ImgView.bounds.height - 20 : Gallary_ImgView.bounds.height - screenStatusBarHeight - 34)
        }
        
        scroll1.Gety = screenStatusBarHeight
        
        if viewforScroll.Getheight < scroll1.Getheight
        {
            viewforScroll.Gety = scroll1.Getheight - viewforScroll.Getheight
        }
        
    }
    
    //    @objc func btnTapped(_ sender: MyButton)
    //    {
    //        if let imageUrl = sender.params!["image_url"] as? String
    //        {
    //            if sender.tag == 877
    //            {
    //                sender.superview!.viewWithTag(900)?.backgroundColor = .red
    //
    //                var image_class = String()
    //
    //                if sender.params?.count == 0
    //                {
    //                    let arr = sender.id!.components(separatedBy: "-")
    //                    for i in 0..<arr.count
    //                    {
    //                        if i > 2 && arr[i].trim().count > 0
    //                        {
    //                            if image_class.count == 0
    //                            {
    //                                image_class.append(arr[i])
    //                            }
    //                            else
    //                            {
    //                                image_class.append(" ")
    //                                image_class.append(arr[i])
    //                            }
    //
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    image_class = sender.id!
    //                }
    //
    //                if image_class == "face"
    //                {
    //                    let nameOfPerson = sender.titleLabel?.text!.replacingOccurrences(of: " ", with: "")
    //                    image_class = "face-\(nameOfPerson!)"
    //                }
    //
    //                //        let url = URL(string: "http://demo.choochco.com/ChoochClick.aspx?ClassId=\(String(describing: image_class))")
    //
    //                let url = URL(string: "https://api.chooch.ai/mobile/ic2_click?class_id=\(image_class.stringByAddingPercentEncodingForRFC3986()!)")
    //                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    //
    //            }
    //            else
    //            {
    //                let viewforPopup = UIView()
    //
    //                viewforPopup.backgroundColor = .clear
    //                viewforPopup.tag = 558
    //
    //                let newImageView = UIImageView()
    //
    //
    //                if UIDevice.current.orientation.isLandscape
    //                {
    //                    viewforPopup.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getheight * 0.8, height: self.view.Getheight * 0.8 + 50)
    //                    newImageView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getheight * 0.8, height: self.view.Getheight * 0.8)
    //                }
    //                else
    //                {
    //                    viewforPopup.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getwidth * 0.8, height: self.view.Getwidth * 0.8 + 50)
    //                    newImageView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getwidth * 0.8, height: self.view.Getwidth * 0.8)
    //                }
    //
    //                newImageView.backgroundColor = .white
    //                newImageView.layer.cornerRadius = 15.0
    //                newImageView.layer.borderColor = UIColor.white.cgColor
    //                newImageView.layer.borderWidth = 1.0
    //                newImageView.clipsToBounds = true
    //                newImageView.isUserInteractionEnabled = true
    //                newImageView.tag = 525
    //                newImageView.contentMode = .scaleAspectFit
    //                viewforPopup.addSubview(newImageView)
    //
    //                let TFPopup = MZFormSheetPresentationViewController.init(contentView: viewforPopup)
    //                //        TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth*0.8, height: self.view.Getwidth*0.8 + 50)
    //                TFPopup.presentationController?.shouldCenterVertically = true
    //                TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
    //                TFPopup.contentViewControllerTransitionStyle = .bounce
    //                TFPopup.presentationController?.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    //                //            TFPopup.didDismissContentViewControllerHandler = {(view) in
    //                //                self.scroll1.isHidden = false
    //                //            }
    //
    //                self.present(TFPopup, animated: true)
    //
    //                newImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
    //                newImageView.sd_setImage(with: URL(string: imageUrl)) { (image, error, cacheType, urls) in
    //
    //                    newImageView.backgroundColor = .clear
    //
    //                    if let image = newImageView.image {
    //                        let ratio = image.size.width / image.size.height
    //                        if image.size.width > image.size.height {
    //                            let newHeight = newImageView.frame.width / ratio
    //                            newImageView.frame.size = CGSize(width: newImageView.frame.width, height: newHeight)
    //                        }
    //                        else{
    //                            let newWidth = newImageView.frame.height * ratio
    //                            newImageView.frame.size = CGSize(width: newWidth, height: newImageView.frame.height)
    //                        }
    //                    }
    //
    //                    newImageView.center = viewforPopup.center
    //                    newImageView.Gety = 0.0
    //
    //                    let textcopyLbl = UILabel ()
    //                    textcopyLbl.frame = CGRect(x: viewforPopup.Getwidth * 0.25, y: 10, width: viewforPopup.Getwidth * 0.5, height: viewforPopup.Getwidth * 0.12)
    //                    textcopyLbl.font = UIFont.init(name: CustomFontWeight.bold, size: self.poorconnectionToastLbl.Getheight * 0.5)
    //                    textcopyLbl.textAlignment = .center
    //                    textcopyLbl.alpha = 0.0
    //                    textcopyLbl.tag = 557
    //                    textcopyLbl.layer.cornerRadius = 10
    //                    textcopyLbl.layer.masksToBounds = true
    //                    textcopyLbl.backgroundColor = UIColor.white.withAlphaComponent(0.75)
    //                    viewforPopup.insertSubview(textcopyLbl, aboveSubview: newImageView)
    //
    //                    let button1 = MyButton.init(frame: CGRect(x: 0.0, y: 0.0, width: sender.Getwidth, height: 40))
    //                    button1.backgroundColor = UIColor.red.withAlphaComponent(0.5)
    //                    button1.cornerRadius = 20.0
    //                    button1.setTitle(sender.titleLabel?.text!, for: .normal)
    //                    button1.id = sender.id
    //                    button1.params = sender.params
    //                    button1.tag = 877
    //                    button1.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 15)!
    //                    viewforPopup.addSubview(button1)
    //
    //                    let removegest = UITapGestureRecognizer(target: self, action: #selector(self.removetapped(gesture:)))
    //                    removegest.delegate = self
    //                    viewforPopup.addGestureRecognizer(removegest)
    //
    //                    button1.center = viewforPopup.center
    //                    button1.Gety = newImageView.Getbottom + 10
    //                    button1.addTarget(self, action: #selector(self.btnTapped(_:)), for: .touchUpInside)
    //
    //                    let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(gesture:)))
    //                    longPress.minimumPressDuration = 1.0
    //                    button1.addGestureRecognizer(longPress)
    //
    //                    viewforPopup.Getheight = button1.Getbottom
    //
    //                }
    //
    //
    //            }
    //
    //
    //        }
    //        else
    //        {
    //
    //            sender.superview!.viewWithTag(900)?.backgroundColor = .red
    //
    //            var image_class = String()
    //
    //            if sender.params?.count == 0
    //            {
    //                let arr = sender.id!.components(separatedBy: "-")
    //                for i in 0..<arr.count
    //                {
    //                    if i > 2 && arr[i].trim().count > 0
    //                    {
    //                        if image_class.count == 0
    //                        {
    //                            image_class.append(arr[i])
    //                        }
    //                        else
    //                        {
    //                            image_class.append(" ")
    //                            image_class.append(arr[i])
    //                        }
    //
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                image_class = sender.id!
    //            }
    //
    //            if image_class == "face"
    //            {
    //                let nameOfPerson = sender.titleLabel?.text!.replacingOccurrences(of: " ", with: "")
    //                image_class = "face-\(nameOfPerson!)"
    //            }
    //
    //            //        let url = URL(string: "http://demo.choochco.com/ChoochClick.aspx?ClassId=\(String(describing: image_class))")
    //
    //            let url = URL(string: "https://api.chooch.ai/mobile/ic2_click?class_id=\(image_class.stringByAddingPercentEncodingForRFC3986()!)")
    //            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    //
    //        }
    //
    //    }
    
    @objc func btnThumbnailTapped(_ sender: MyButton)
    {
        
        scroll1.isHidden = true
        
        let viewforPopup = UIView()
        
        viewforPopup.backgroundColor = .clear
        viewforPopup.tag = 558
        
        let imageView = sender.imageView!
        let newImageView = UIImageView(image: imageView.image)
        
        if UIDevice.current.orientation.isLandscape
        {
            viewforPopup.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getheight * 0.8, height: self.view.Getheight * 0.8 + 50)
            newImageView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getheight * 0.8, height: self.view.Getheight * 0.8)
        }
        else
        {
            viewforPopup.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getwidth * 0.8, height: self.view.Getwidth * 0.8 + 50)
            newImageView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getwidth * 0.8, height: self.view.Getwidth * 0.8)
        }
        
        if let image = imageView.image {
            let ratio = image.size.width / image.size.height
            if image.size.width > image.size.height {
                let newHeight = newImageView.frame.width / ratio
                newImageView.frame.size = CGSize(width: newImageView.frame.width, height: newHeight)
            }
            else{
                let newWidth = newImageView.frame.height * ratio
                newImageView.frame.size = CGSize(width: newWidth, height: newImageView.frame.height)
            }
        }
        
        newImageView.center = viewforPopup.center
        newImageView.Gety = 0.0
        
        //        newImageView.sizeToFit()
        newImageView.layer.cornerRadius = 15.0
        newImageView.layer.borderColor = UIColor.white.cgColor
        newImageView.layer.borderWidth = 1.0
        newImageView.clipsToBounds = true
        newImageView.isUserInteractionEnabled = true
        newImageView.tag = 525
        newImageView.contentMode = .scaleAspectFit
        viewforPopup.addSubview(newImageView)
        
        let textcopyLbl = UILabel ()
        textcopyLbl.frame = CGRect(x: viewforPopup.Getwidth * 0.25, y: 10, width: viewforPopup.Getwidth * 0.5, height: viewforPopup.Getwidth * 0.12)
        textcopyLbl.font = UIFont.init(name: CustomFontWeight.bold, size: poorconnectionToastLbl.Getheight * 0.5)
        textcopyLbl.textAlignment = .center
        textcopyLbl.alpha = 0.0
        textcopyLbl.tag = 557
        textcopyLbl.layer.cornerRadius = 10
        textcopyLbl.layer.masksToBounds = true
        textcopyLbl.backgroundColor = UIColor.white.withAlphaComponent(0.75)
        viewforPopup.insertSubview(textcopyLbl, aboveSubview: newImageView)
        
        let button1 = MyButton.init(frame: CGRect(x: 0.0, y: 0.0, width: sender.superview!.Getwidth - 40, height: 40))
        button1.backgroundColor = UIColor.red.withAlphaComponent(0.5)
        button1.cornerRadius = 20.0
        button1.setTitle(sender.id!, for: .normal)
        button1.id = (sender.superview!.viewWithTag(145) as! MyButton).id
        button1.params = (sender.superview!.viewWithTag(145) as! MyButton).params
        button1.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 15)!
        viewforPopup.addSubview(button1)
        
        let removegest = UITapGestureRecognizer(target: self, action: #selector(removetapped(gesture:)))
        removegest.delegate = self
        viewforPopup.addGestureRecognizer(removegest)
        
        button1.center = viewforPopup.center
        button1.Gety = newImageView.Getbottom + 10
        button1.addTarget(self, action: #selector(self.openInsafari(_:)), for: .touchUpInside)
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
        longPress.minimumPressDuration = 1.0
        button1.addGestureRecognizer(longPress)
        
        viewforPopup.Getheight = button1.Getbottom
        
        
        
        let TFPopup = MZFormSheetPresentationViewController.init(contentView: viewforPopup)
        //        TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth*0.8, height: self.view.Getwidth*0.8 + 50)
        TFPopup.presentationController?.shouldCenterVertically = true
        TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
        TFPopup.contentViewControllerTransitionStyle = .bounce
        TFPopup.presentationController?.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        TFPopup.didDismissContentViewControllerHandler = {(view) in
            self.scroll1.isHidden = false
        }
        
        self.present(TFPopup, animated: true)
        
    }
    
    
    
    private func copyToClipBoard(textToCopy: String , btn : MyButton) {
        let pasteBoard = UIPasteboard.general
        pasteBoard.string = textToCopy
        
        if topMostViewController() is MZFormSheetPresentationViewController
        {
            let txtcopyLbl = btn.superview!.viewWithTag(557) as! UILabel
            txtcopyLbl.text = "text copied"
            txtcopyLbl.fadeIn()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                txtcopyLbl.fadeOutPoorConnection()
            }
        }
        else
        {
            
            let textcopyLbl = UILabel ()
            if UIDevice.current.orientation.isLandscape
            {
                textcopyLbl.frame = CGRect(x: 0.0, y: self.view.Getheight * 0.25, width: self.view.Getheight * 0.5, height: self.view.Getheight * 0.12)
                
            }
            else
            {
                textcopyLbl.frame = CGRect(x: 0.0, y: self.view.Getheight * 0.25, width: self.view.Getwidth * 0.5, height: self.view.Getwidth * 0.12)
            }
            textcopyLbl.font = UIFont.init(name: CustomFontWeight.bold, size: poorconnectionToastLbl.Getheight * 0.5)
            textcopyLbl.text = "text copied"
            textcopyLbl.textAlignment = .center
            textcopyLbl.center.x = self.view.center.x
            textcopyLbl.alpha = 0.0
            textcopyLbl.tag = 557
            textcopyLbl.layer.cornerRadius = 10
            textcopyLbl.layer.masksToBounds = true
            textcopyLbl.backgroundColor = UIColor.white.withAlphaComponent(0.75)
            self.view.addSubview(textcopyLbl)
            self.view.bringSubview(toFront: textcopyLbl)
            textcopyLbl.fadeIn()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                textcopyLbl.fadeOut()
            }
        }
        
    }
    
    
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation:CLLocation = locations[0] as CLLocation
        
        manager.stopUpdatingLocation()
        
        currentLat = "\(userLocation.coordinate.latitude)"
        currentLong = "\(userLocation.coordinate.longitude)"
        
    }
    
    
    var timer1 : Timer!
    
    func upload_image(image:UIImage ,tag : String) {
        
        let manager = AFHTTPSessionManager(sessionConfiguration: URLSessionConfiguration.default)
        
        var dict = NSMutableDictionary()
        var strURL = String()
        //        strURL = "http://apiv2.choochco.com/predict/"
        
        predictionArray.removeAllObjects()
        
        if appIsOpen
        {
            appIsOpen = false
            
            dict = [
                "gps":"\(self.currentLat),\(self.currentLong)",
                "deviceid":"\(UIDevice.current.identifierForVendor!.uuidString)"
            ]
            
            if isRecording
            {
                strURL = "http://api.chooch.ai/predict/?Apikey=\(apikey)&user_id=\(userId)&paused=0"
            }
            else
            {
                strURL = "http://api.chooch.ai/predict/?Apikey=\(apikey)&user_id=\(userId)&paused=1"
            }
        }
        else
        {
            if isRecording
            {
                strURL = "http://api.chooch.ai/predict/?Apikey=\(apikey)&user_id=\(userId)&paused=0"
            }
            else
            {
                strURL = "http://api.chooch.ai/predict/?Apikey=\(apikey)&user_id=\(userId)&paused=1"
            }
        }
        
        //  https://api.chooch.ai/predict/image?url=none&predicttype=dense&apikey=69c8f610-76b5-451e-81d6-3d92862d18d8&req_type=mobile&mobile_gallery=1
        
        
        
        //        print(dict)
        
        var myImage = UIImage()
        
        //        let image123 = image1(with: videoPreview.getImage(scale: 5.0), scaledTo: CGSize(width: 1188, height: 2064))
        //        let view = myImage
        //
        //        let imagefromLayer = UIImage.image(from: self.videoCapture.previewLayer!)
        
        
        myImage = image
        
        var imageData : Data!
        
        if isGallary || !isRecording
        {
            
            y = 0
            strURL = "https://api.chooch.ai/predict/image?url=none&predicttype=dense&apikey=\(apikey)&user_id=\(userId)&req_type=mobile&mobile_gallery=1"
            
            //            strURL = "http://ec2-54-196-168-80.compute-1.amazonaws.com/predict/image?url=none&predicttype=dense&apikey=69c8f610-76b5-451e-81d6-3d92862d18d8&req_type=mobile&mobile_gallery=1"
            
            myImage = myImage.compressImage()!
            
            imageData = UIImageJPEGRepresentation(myImage, 1.0)
            
            
        }
        else
        {
            if !isRecording
            {
                imageData = UIImageJPEGRepresentation(myImage, 0.25)
            }
            else
            {
                imageData = UIImageJPEGRepresentation(myImage, 0.1)
            }
        }
        
        DispatchQueue.main.async {
            self.timer1 =  Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.delayedAction(timer:)), userInfo: nil, repeats: false)
        }
        
        print("Called API")
        
        lastimage = myImage
        
        manager.post(strURL, parameters: dict, constructingBodyWith: { (formData) in
            var str = ProcessInfo.processInfo.globallyUniqueString
            str = str.appending(".jpg")
            
            formData.appendPart(withFileData: imageData!, name: "image", fileName: str, mimeType: "image/jpeg")
            if self.isGallary
            {
                //                print(self.metadataDict)
                //                print((self.metadataDict as! [String:Any]).jsonwithRawdata)
                if !(self.metadataDict.count <= 0)
                {
                    formData.appendPart(withForm: ((self.metadataDict as! [String:Any]).jsonwithRawdata).data(using: .utf8)!, name: "meta_data")
                }
                //                formData.appendPart(withForm: (NSKeyedArchiver.archivedData(withRootObject: self.metadataDict) as NSData) as Data, name: "meta_data")
            }
            
            if self.tasks.count > 0
            {
                for manager in self.tasks
                {
                    manager.invalidateAndCancel()
                }
            }
            
            self.tasks.removeAll()
            self.tasks.append(manager.session)
            
        }, progress: { (uploadProgress) in
            
            
        }, success: { (task, responseObject) in
            
            //            print("Tag = \(tag)")
            
            //            self.ToastIMg.fadeOutPoorConnection()
            self.timer1.invalidate()
            self.poorconnectionToastLbl.fadeOutPoorConnection()
            self.loaderImgView.isHidden = true
            
            let predct = responseObject as! NSDictionary
            
            print(predct)
            
            let newary = predct.value(forKey: "predictions")
            let set1 = NSSet.init(array: predictionArray as! [Any])
            let set2 = NSSet.init(array: newary as! [Any])
            if set1.isEqual(set2)
            {
                // equal
            }
            else
            {
                predictionArray = NSMutableArray(array: predct.value(forKey: "predictions") as! NSArray)
                if predct.value(forKey: "faces") != nil
                {
                    facesArray = NSMutableArray(array: predct.value(forKeyPath: "faces.predictions") as! NSArray)
                }
                
                if self.isGallary || !self.isRecording
                {
                    
                    if ((predictionArray.value(forKeyPath: "classvalue") as! NSArray).componentsJoined(by: ",").components(separatedBy: ",").count) > 0 {
                        
                        let count = (predictionArray.value(forKeyPath: "classvalue") as! NSArray).componentsJoined(by: ",").components(separatedBy: ",").count
                        
                        UIView.animate(withDuration: 0.1, animations: {
                            
                            self.predictionContainer.frame = CGRect(x: 0, y: self.predictionContainer.Gety - CGFloat(count * 53), width: self.predictionContainer.Getwidth, height: self.predictionContainer.Getheight)
                            
                        }, completion: { (whaever) in
                            
                            if (self.predictionContainer.Getbottom < 0.0) {
                                self.view.sendSubview(toBack: self.predictionContainer)
                                
                                self.predictionContainer.frame = CGRect(x: 0, y: self.predictionContainer.Gety + CGFloat(count * 53), width: self.predictionContainer.Getwidth, height: self.predictionContainer.Getheight)
                            }
                            
                        })
                    }
                    
                    self.lastimage = myImage
                    
                    self.setButtonPredictScrollview(myImage: self.lastimage)
                }
                else
                {
                    
                    self.view.bringSubview(toFront: self.predictionContainer)
                    self.setupPredictionView()
                }
                
                if tag == "FeedbackCall"
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadIntelligency"), object: nil)
                }
                
            }
            
            
            if self.isRecording
            {
                counter = true
            }
            
            
            
        }, failure: { (task, error) in
            
            
            counter = true
            
            
            
            if error.localizedDescription != "cancelled"
            {
                if error.localizedDescription != "Software caused connection abort"
                {
                    self.timer1.invalidate()
                    self.loaderImgView.isHidden = true
                    self.poorconnectionToastLbl.text = "no connection"
                    self.poorconnectionToastLbl.fadeIn()
                }
                
                if self.isGallary || self.isRecording
                {
                    NetworkManager.sharedInstance.reachability.whenReachable = { reachability in
                        counter = true
                        self.callAPi(image: self.lastimage, tag: "RapidlyCall")
                    }
                }
                
            }
            else
            {
                
                self.timer1.invalidate()
                self.poorconnectionToastLbl.fadeOutPoorConnection()
                self.loaderImgView.isHidden = true
            }
            
            
            print(error)
            
        })
        
    }
    
    func image1(with image: UIImage, scaledTo newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage ?? UIImage()
    }
    
    func callAPi(image:UIImage , tag : String){
        
        
        //        oldversionBtn.fadeOutPoorConnection()
        if counter
        {
            if Reachability123.isConnectedToNetwork()
            {
                upload_image(image: image, tag: tag)
                counter = false
            }
            else if isGallary
            {
                upload_image(image: image, tag: tag)
                counter = false
            }
            else
            {
                DispatchQueue.main.async {
                    self.poorconnectionToastLbl.text = "no connection"
                    self.poorconnectionToastLbl.fadeIn()
                }
            }
        }
        
    }
    
    @objc func delayedAction(timer: Timer!) {
        DispatchQueue.main.async {
            self.timer1.invalidate()
            
            
            print("Connecting Again")
            
            self.loaderImgView.isHidden = false
            
            self.timer1 =  Timer.scheduledTimer(timeInterval: 8.0, target: self, selector: #selector(self.delayedAction10secs(timer:)), userInfo: nil, repeats: false)
        }
    }
    
    @objc func delayedAction10secs(timer: Timer!) {
        DispatchQueue.main.async {
            
            self.loaderImgView.isHidden = true
            
            self.poorconnectionToastLbl.text = "no connection"
            self.poorconnectionToastLbl.fadeIn()
        }
    }
    
    // MARK: - Button Events
    
    @IBAction func SignIn_Pressed(_ sender: UIButton) {
        
        userDefault.removeObject(forKey: custToken)
        userDefault.removeObject(forKey: custUserame)
        userDefault.removeObject(forKey: custId)
        
        let manager = LoginManager()
        manager.logOut()
        
        GIDSignIn.sharedInstance()?.signOut()
        
        let loginNC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginNavigationController") as! UINavigationController
        
        window?.rootViewController = loginNC
        window?.makeKeyAndVisible()
        
        
    }
    @IBAction func FeedBack_Pressed(_ sender: UIButton)
    {
        if userId.trim() == "-1" {
            
            let TFPopup = MZFormSheetPresentationViewController.init(contentView: SigninView)
            //            TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth*0.8, height: self.view.Getwidth*0.8 + 50)
            TFPopup.presentationController?.shouldCenterVertically = true
            TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
            TFPopup.contentViewControllerTransitionStyle = .bounce
            TFPopup.presentationController?.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            //            TFPopup.didDismissContentViewControllerHandler = {(view) in
            //                self.scroll1.isHidden = false
            //            }
            
            self.present(TFPopup, animated: true)
            
            return
        }
        
        isRecording = false
        startSession = true
        videoCapture.stop()
        
        for subview in predictionContainer.subviews
        {
            subview.removeFromSuperview()
        }
        for buttons in viewforScroll.subviews {
            buttons.removeFromSuperview()
        }
        y = 0
        scroll1.Getheight = 0
        
        counter = true
        callAPi(image: lastimage, tag: "FeedbackCall")
        
        let feedbkVC = mainStoryBoard.instantiateViewController(withIdentifier: "FeedbackVC") as! FeedbackVC
        feedbkVC.Backimage = lastimage
        feedbkVC.isDense = Gallary_ImgView.isHidden ? false : true
        self.present(feedbkVC, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func logOut_Pressed(_ sender: UIButton) {
        
        if userId.trim() == "-1" {
            
            let TFPopup = MZFormSheetPresentationViewController.init(contentView: SigninView)
            //            TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth*0.8, height: self.view.Getwidth*0.8 + 50)
            TFPopup.presentationController?.shouldCenterVertically = true
            TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
            TFPopup.contentViewControllerTransitionStyle = .bounce
            TFPopup.presentationController?.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            //            TFPopup.didDismissContentViewControllerHandler = {(view) in
            //                self.scroll1.isHidden = false
            //            }
            
            self.present(TFPopup, animated: true)
            
            return
        }
        
        isRecording = false
        startSession = true
        videoCapture.stop()

        for subview in predictionContainer.subviews
        {
            subview.removeFromSuperview()
        }
        for buttons in viewforScroll.subviews {
            buttons.removeFromSuperview()
        }
        y = 0
        scroll1.Getheight = 0

        let profileNav = mainStoryBoard.instantiateViewController(withIdentifier: "profileNavController") as! UINavigationController
        self.present(profileNav, animated: true, completion: nil)
        
//                self.presentAlertWithTitle(title: "Do you want to log out?", message: "", options: "Yes","No", completion: { (Index) in
//
//                    if Index == 0
//                    {
//                        userDefault.removeObject(forKey: custToken)
//                        userDefault.removeObject(forKey: custUserame)
//                        userDefault.removeObject(forKey: custId)
//
//                        let manager = LoginManager()
//                        manager.logOut()
//
//                        GIDSignIn.sharedInstance()?.signOut()
//
//                        let loginNC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginNavigationController") as! UINavigationController
//
//                        window?.rootViewController = loginNC
//                        window?.makeKeyAndVisible()
//
//                    }
//
//                })
        
    }
    
    
    @IBAction func ClearPredictions_Pressed(_ sender: UIButton)
    {
        
        predictionArray.removeAllObjects()
        for subview in predictionContainer.subviews
        {
            
            subview.fadeOut()
        }
        for buttons in viewforScroll.subviews {
            buttons.fadeOut()
        }
        y = 0
        //        scroll1.Getheight = 0
        
    }
    
    @IBAction func Flash_Pressed(_ sender: UIButton) {
        
        if let device = AVCaptureDevice.default(for: AVMediaType.video) {
            
            if (device.hasTorch) {
                do {
                    try device.lockForConfiguration()
                    let torchOn = !device.isTorchActive
                    try device.setTorchModeOn(level: 1.0)
                    device.torchMode = torchOn ? AVCaptureDevice.TorchMode.on : AVCaptureDevice.TorchMode.off
                    device.unlockForConfiguration()
                    self.tabicon_flash.image = torchOn ? UIImage(named: "ic_flash_on") : UIImage(named: "ic_flash")
                } catch {
                    print(error.localizedDescription)
                }
            }
            else
            {
                presentAlertWithTitle(title: "Your device doesn't have flash to use", message: "", options: "OK") { (buttonIndex) in
                    
                }
            }
        }
    }
    
    @IBAction func Gallary_Pressed(_ sender: UIButton) {
        
        tabicon_camera.image = Gallary_ImgView.isHidden ? UIImage(named: "ic_photo_camera") : UIImage(named: "ic_picture")
        Gallary_ImgView.image = nil
        
        poorconnectionToastLbl.fadeOutPoorConnection()
        self.loaderImgView.isHidden = true
        
        let transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        Gallary_ImgView.transform = transform
        
        self.tabicon_flash.image = UIImage(named: "ic_flash")
        
        if let device = AVCaptureDevice.default(for: AVMediaType.video)
        {
            
            if (device.hasTorch)
            {
                do {
                    try device.lockForConfiguration()
                    //                        try device.setTorchModeOn(level: 0.0)
                    device.torchMode = AVCaptureDevice.TorchMode.off
                    device.unlockForConfiguration()
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        
        
        for subview in predictionContainer.subviews
        {
            subview.removeFromSuperview()
        }
        for buttons in viewforScroll.subviews {
            buttons.removeFromSuperview()
        }
        y = 0
        scroll1.Getheight = 0
        
        if Gallary_ImgView.isHidden {
            
            videoPreview.isHidden = true
            predictionContainer.isHidden = true
            //            cameraSwitchButton.alpha = 0.0
            isRecording = false
            videoCapture.stop()
            Gallary_ImgView.isHidden = false
            
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.modalPresentationStyle = .overCurrentContext
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            
        }
        else
        {
            startSession = false
            counter = true
            isRecording = true
            self.isGallary = false
            videoCapture.start()
            //            cameraSwitchButton.alpha = 1.0
            //            self.view.bringSubview(toFront: cameraSwitchButton)
            videoPreview.isHidden = false
            predictionContainer.isHidden = false
            Gallary_ImgView.isHidden = true
            
        }
        
    }
    
    
    @objc func openAppstore(_ sender: UIButton) {
        
        if let url = URL(string: "https://itunes.apple.com/us/app/chooch-ic2/id1304120928?mt=8"),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func usernameLongPressed(gesture: UILongPressGestureRecognizer)
    {
        //        print()
        
        
        let button1 = MyButton.init(frame: CGRect(x: 0.0, y: -40.0, width: 0, height: 40))
        button1.backgroundColor = .white
        button1.setTitleColor(.black, for: .normal)
        button1.cornerRadius = 20.0
        button1.setTitle((gesture.view as? MyButton)!.titleLabel!.text, for: .normal)
        button1.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 15)!
        button1.Getwidth = button1.titleLabel!.text!.width(withConstrainedHeight: 40, font: UIFont.init(name: CustomFontWeight.bold, size: 15)!) + 16.0
        button1.center.x = self.view.Getwidth * 0.5
        button1.tag = 5004
        button1.contentEdgeInsets =  UIEdgeInsetsMake(8, 8, 8, 8)
        
        
        if gesture.state == .began {
            
            self.view.insertSubview(button1, aboveSubview: loaderImgView)
            
            UIView.animate(withDuration: 1.0, animations: {
                
                if #available(iOS 11.0, *) {
                    button1.Gety = self.view.safeAreaInsets.top
                } else {
                    // Fallback on earlier versions
                    button1.Gety = 0.0
                }
                
            }) { (Yes) in
                
            }
            
        }
        if gesture.state == .ended || gesture.state == .failed || gesture.state == .cancelled {
            
            UIView.animate(withDuration: 1.0, animations: {
                self.view.viewWithTag(5004)!.Gety = -40.0
            }) { (Yes) in
                self.view.viewWithTag(5004)!.removeFromSuperview()
            }
            
        }
        
        
        
        
    }
    
    @objc func openInsafari(_ sender: MyButton) {
        
        //        print(sender.params)
        
        if let imageUrl = sender.params!["image_url"] as? String
        {
            
            let viewforPopup = UIView()
            
            viewforPopup.backgroundColor = .clear
            viewforPopup.tag = 558
            
            let newImageView = UIImageView()
            
            
            if UIDevice.current.orientation.isLandscape
            {
                viewforPopup.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getheight * 0.8, height: self.view.Getheight * 0.8 + 97)
                newImageView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getheight * 0.8, height: self.view.Getheight * 0.8)
            }
            else
            {
                viewforPopup.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getwidth * 0.8, height: self.view.Getwidth * 0.8 + 97)
                newImageView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getwidth * 0.8, height: self.view.Getwidth * 0.8)
            }
            
            newImageView.backgroundColor = .white
            newImageView.layer.cornerRadius = 15.0
            newImageView.layer.borderColor = UIColor.white.cgColor
            newImageView.layer.borderWidth = 1.0
            newImageView.clipsToBounds = true
            newImageView.isUserInteractionEnabled = true
            newImageView.tag = 525
            newImageView.contentMode = .scaleAspectFit
            viewforPopup.addSubview(newImageView)
            
            let TFPopup = MZFormSheetPresentationViewController.init(contentView: viewforPopup)
            //        TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth*0.8, height: self.view.Getwidth*0.8 + 50)
            TFPopup.presentationController?.shouldCenterVertically = true
            TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
            TFPopup.contentViewControllerTransitionStyle = .bounce
            TFPopup.presentationController?.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            //            TFPopup.didDismissContentViewControllerHandler = {(view) in
            //                self.scroll1.isHidden = false
            //            }
            
            self.present(TFPopup, animated: true)
            
            newImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            newImageView.sd_setImage(with: URL(string: imageUrl)) { (image, error, cacheType, urls) in
                
                newImageView.backgroundColor = .clear
                
                if let image = newImageView.image {
                    let ratio = image.size.width / image.size.height
                    if image.size.width > image.size.height {
                        let newHeight = newImageView.frame.width / ratio
                        newImageView.frame.size = CGSize(width: newImageView.frame.width, height: newHeight)
                    }
                    else{
                        let newWidth = newImageView.frame.height * ratio
                        newImageView.frame.size = CGSize(width: newWidth, height: newImageView.frame.height)
                    }
                }
                
                newImageView.center = viewforPopup.center
                newImageView.Gety = 0.0
                
                let textcopyLbl = UILabel()
                textcopyLbl.frame = CGRect(x: viewforPopup.Getwidth * 0.25, y: 10, width: viewforPopup.Getwidth * 0.5, height: viewforPopup.Getwidth * 0.12)
                textcopyLbl.font = UIFont.init(name: CustomFontWeight.bold, size: self.poorconnectionToastLbl.Getheight * 0.5)
                textcopyLbl.textAlignment = .center
                textcopyLbl.alpha = 0.0
                textcopyLbl.tag = 557
                textcopyLbl.layer.cornerRadius = 10
                textcopyLbl.layer.masksToBounds = true
                textcopyLbl.backgroundColor = UIColor.white.withAlphaComponent(0.75)
                viewforPopup.insertSubview(textcopyLbl, aboveSubview: newImageView)
                
                let button1 = MyButton.init(frame: CGRect(x: 0.0, y: 0.0, width: sender.Getwidth, height: 40))
                button1.backgroundColor = UIColor.red.withAlphaComponent(0.5)
                button1.cornerRadius = 20.0
                button1.setTitle(sender.titleLabel?.text!, for: .normal)
                button1.id = sender.id
                button1.params = NSDictionary()
                button1.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 15)!
                viewforPopup.addSubview(button1)
                
                let removegest = UITapGestureRecognizer(target: self, action: #selector(self.removetapped(gesture:)))
                removegest.delegate = self
                viewforPopup.addGestureRecognizer(removegest)
                
                button1.center = viewforPopup.center
                button1.Gety = newImageView.Getbottom + 10
                button1.addTarget(self, action: #selector(self.openInsafari(_:)), for: .touchUpInside)
                
                let usernameBtn = MyButton.init(frame: CGRect(x: 0.0, y: 0.0, width: sender.Getwidth, height: 40))
                usernameBtn.backgroundColor = UIColor.red.withAlphaComponent(0.5)
                usernameBtn.cornerRadius = 20.0
                usernameBtn.setTitle("by \(sender.userName ?? "No username")", for: .normal)
                usernameBtn.titleLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: 15)!
                usernameBtn.titleLabel?.lineBreakMode = .byTruncatingTail
                usernameBtn.contentEdgeInsets =  UIEdgeInsetsMake(8, 8, 8, 8)
                viewforPopup.addSubview(usernameBtn)
                usernameBtn.Getwidth = usernameBtn.titleLabel!.text!.width(withConstrainedHeight: 15, font: usernameBtn.titleLabel!.font) + 16.0
                usernameBtn.center = viewforPopup.center
                usernameBtn.Gety = button1.Getbottom + 7
                
                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPress(gesture:)))
                longPress.minimumPressDuration = 1.0
                button1.addGestureRecognizer(longPress)
                
                viewforPopup.Getheight = usernameBtn.Getbottom
                
            }
        }
        else
        {
            //            sender.backgroundColor = UIColor.red
            
            var image_class = String()
            
            if sender.params!.count == 0
            {
                let arr = sender.id!.components(separatedBy: "-")
                for i in 0..<arr.count
                {
                    if i > 2 && arr[i].trim().count > 0
                    {
                        if image_class.count == 0
                        {
                            image_class.append(arr[i])
                        }
                        else
                        {
                            image_class.append(" ")
                            image_class.append(arr[i])
                        }
                        
                    }
                }
            }
            else
            {
                if let parentObj = sender.params!["trained_by"] as? String
                {
                    print(parentObj)
                    let arr = sender.id!.components(separatedBy: "-")
                    for i in 0..<arr.count
                    {
                        if i > 1 && arr[i].trim().count > 0
                        {
                            if image_class.count == 0
                            {
                                image_class.append(arr[i])
                            }
                            else
                            {
                                image_class.append(" ")
                                image_class.append(arr[i])
                            }
                            
                        }
                    }
                }
                else {
                    image_class = sender.id!
                }
                
            }
            
            if image_class == "face"
            {
                let nameOfPerson = sender.titleLabel?.text!.replacingOccurrences(of: " ", with: "")
                image_class = "face-\(nameOfPerson!)"
            }
            
            //        let url = URL(string: "http://demo.choochco.com/ChoochClick.aspx?ClassId=\(String(describing: image_class))")
            
            let url = URL(string: "https://api.chooch.ai/mobile/ic2_click?class_id=\(image_class.stringByAddingPercentEncodingForRFC3986()!)")
            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
        }
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        if #available(iOS 11.0, *) {
            if let img = CIImage(contentsOf: info[UIImagePickerControllerImageURL] as! URL) {
                let metadata = img.properties as NSDictionary
                
                metadataDict.removeAllObjects()
                metadataDict.addEntries(from: metadata as! [AnyHashable : Any])
                //                print(metadataDict)
            }
        }
        
        startSession = true
        isGallary = true
        
        Gallary_ImgView.image = (info[UIImagePickerControllerOriginalImage] as? UIImage)!//.resizeWithPercent(percentage: 0.5)!
        lastimage = (info[UIImagePickerControllerOriginalImage] as? UIImage)!//.resizeWithPercent(percentage: 0.5)!
        
        self.Gallary_ImgView.transform = CGAffineTransform.identity
        self.Gallary_ImgView.Getx = 0.0
        self.Gallary_ImgView.Gety = 0.0
        
        for subview in predictionContainer.subviews
        {
            subview.removeFromSuperview()
        }
        for buttons in viewforScroll.subviews {
            buttons.removeFromSuperview()
        }
        scroll1.Getheight = 0
        
        counter = true
        
        self.Tabbar_FlashBtn.isEnabled = false
        self.Tabbar_FlashBtn.backgroundColor = hexStringToUIColor(hex: "202020").withAlphaComponent(0.75)
        
        self.callAPi(image: self.lastimage, tag: "GallaryApi")
        
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        isRecording = true
        isGallary = false
        videoCapture.start()
        counter = true
        //        cameraSwitchButton.isHidden = false
        videoPreview.isHidden = false
        predictionContainer.isHidden = false
        Gallary_ImgView.isHidden = true
        self.Tabbar_FlashBtn.isEnabled = true
        self.Tabbar_FlashBtn.backgroundColor = .clear
        
        for subview in predictionContainer.subviews
        {
            subview.removeFromSuperview()
        }
        for buttons in viewforScroll.subviews {
            buttons.removeFromSuperview()
        }
        scroll1.Getheight = 0
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    var vZoomFactor = CGFloat()
    var lastScale : CGFloat!
    
    func screenshot() -> UIImage {
        let imageSize = Gallary_ImgView.bounds.size as CGSize;
        UIGraphicsBeginImageContextWithOptions(imageSize, false, 0)
        let context = UIGraphicsGetCurrentContext()
        for obj : AnyObject in UIApplication.shared.windows {
            if let window = obj as? UIWindow {
                if window.responds(to: #selector(getter: UIWindow.screen)) || window.screen == UIScreen.main {
                    // so we must first apply the layer's geometry to the graphics context
                    context!.saveGState();
                    // Center the context around the window's anchor point
                    context!.translateBy(x: window.center.x, y: window.center
                        .y);
                    // Apply the window's transform about the anchor point
                    context!.concatenate(Gallary_ImgView.transform);
                    // Offset by the portion of the bounds left of and above the anchor point
                    context!.translateBy(x: -Gallary_ImgView.bounds.size.width * Gallary_ImgView.layer.anchorPoint.x,
                                         y: -Gallary_ImgView.bounds.size.height * Gallary_ImgView.layer.anchorPoint.y);
                    
                    // Render the layer hierarchy to the current context
                    Gallary_ImgView.layer.render(in: context!)
                    
                    // Restore the context
                    context!.restoreGState();
                }
            }
        }
        let image = UIGraphicsGetImageFromCurrentImageContext();
        return image!
    }
    
    func cropVisiblePortionOf(_ imageView: UIImageView) -> UIImage? {
        
        let cropedImage: UIImage = screenshot()
        
        return cropedImage
    }

    
}

func imageWithImage(image: UIImage, croppedTo rect: CGRect) -> UIImage {
    
    UIGraphicsBeginImageContext(rect.size)
    let context = UIGraphicsGetCurrentContext()
    
    let drawRect = CGRect(x: -rect.origin.x, y: -rect.origin.y,
                          width: image.size.width, height: image.size.height)
    
    context?.clip(to: CGRect(x: 0, y: 0,
                             width: rect.size.width, height: rect.size.height))
    
    image.draw(in: drawRect)
    
    let subImage = UIGraphicsGetImageFromCurrentImageContext()
    
    UIGraphicsEndImageContext()
    return subImage!
    
}


extension ViewController: VideoCaptureDelegate {
    func videoCapture(_ capture: VideoCapture, didCaptureVideoTexture texture: MTLTexture?, timestamp: CMTime)
    {
        if texture != nil
        {
        }
    }
    
    func videoCapture(_ capture: VideoCapture, didCapturePhotoTexture texture: MTLTexture?, previewImage: UIImage?)
    {
        
        DispatchQueue.main.async
            {
                if self.isRecording && !self.isGallary
                {
                    self.videoPreview.isHidden = false
                    self.predictionContainer.isHidden = false
                    self.Gallary_ImgView.isHidden = true
                }
                self.Tabbar_LogoutBtn.backgroundColor = self.userId.trim() != "-1" ? .clear : hexStringToUIColor(hex: "202020").withAlphaComponent(0.75)
                self.Tabbar_FeedbackBtn.backgroundColor = self.userId.trim() != "-1" ? .clear : hexStringToUIColor(hex: "202020").withAlphaComponent(0.75)
                self.Tabbar_FlashBtn.backgroundColor = self.Gallary_ImgView.isHidden ? .clear : hexStringToUIColor(hex: "202020").withAlphaComponent(0.75)
                self.Tabbar_FlashBtn.isEnabled = self.Gallary_ImgView.isHidden ? true : false
                self.tabicon_camera.image = self.Gallary_ImgView.isHidden ? UIImage(named: "ic_picture") : UIImage(named: "ic_photo_camera")
                if let device = AVCaptureDevice.default(for: AVMediaType.video)
                {
                    if (device.hasTorch)
                    {
                        do
                        {
                            try device.lockForConfiguration()
                            let torchOn = !device.isTorchActive
                            self.tabicon_flash.image = torchOn ? UIImage(named: "ic_flash") : UIImage(named: "ic_flash_on")
                        }
                        catch
                        {
                            print(error.localizedDescription)
                        }
                    }
                }
        }
        
        lastimage = previewImage!
        callAPi(image: previewImage!, tag: "GeneralApi")
        
    }
}

