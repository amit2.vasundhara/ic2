//
//  tblChatReceiverCell.swift
//  Prontopegno
//
//  Created by V3 on 26/12/18.
//  Copyright © 2018 Vasundhara. All rights reserved.
//

import UIKit

class tblChatReceiverCell: UITableViewCell {

    @IBOutlet var lblMessage: UILabel!
    
    @IBOutlet var PercentageLbl: UILabel!
    
    @IBOutlet var PercentageSuperViewConst: NSLayoutConstraint!
    
    @IBOutlet var logoView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        lblMessage.sizeToFit()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
