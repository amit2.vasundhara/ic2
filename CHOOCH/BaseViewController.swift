//
//  AMBaseViewController.swift
//  Amzan
//
//  Created by AlphaVed Mac on 12/09/18.
//  Copyright © 2018 AlphaVed. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import AVKit
import AVFoundation

//var seelectedIndexTabaar = 0

class BaseViewController: UIViewController , NVActivityIndicatorViewable , UITextFieldDelegate , UIGestureRecognizerDelegate{
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var nodataLbl : UILabel!

    //MARK:- ********************** VIEW LIFECYCLE **********************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nodataLbl = ConfigureNodataLbl(viewController: self)
        self.view.addSubview(nodataLbl)
        
    }
    
    func showLoaderwithMessage(message : String ,type : NVActivityIndicatorType ,color : UIColor) {
        
        startAnimating(CGSize(width: 60, height:60),
                       message: message,
                       messageFont: UIFont.boldSystemFont(ofSize: 12),
                       type: type,
                       color: UIColor.white,
                       padding: 10,
                       displayTimeThreshold: nil,
                       minimumDisplayTime: nil,
                       backgroundColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.6),
                       textColor: UIColor.white)
    }
    
    func removeLoaderWithMessage(message : String)  {
        
        NVActivityIndicatorPresenter.sharedInstance.setMessage(message)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            self.stopAnimating()
        }
    }
    
    func RemoveLoader() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            self.stopAnimating()
        }
        
    }
    
    func configureTF(textfield : UITextField) {
        textfield.delegate = self
        textfield.enablesReturnKeyAutomatically = false
        textfield.layer.cornerRadius = 20.0
        textfield.layer.borderColor = hexStringToUIColor(hex: "CCCCCC").cgColor
        textfield.layer.borderWidth = 1.0
        textfield.setLeftPaddingPoints(12)
    }
    
    
    //MARK:- Navigation Controller Animation based Transition
    
    func pushViewController(_ viewController: UIViewController) {
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func presentViewController(_ viewController: UIViewController) {
        
        self.navigationController?.present(viewController, animated: true, completion: nil)
    }
    
    func popViewController() {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func dismissViewController() {
        
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- **********************  REGISTER XIB **********************
    
    //Register TableViewCell
    func registerTableViewCell(cellName : String , to tableView: UITableView) {
        
        let cellNIB = UINib(nibName: cellName, bundle: nil)
        tableView.register(cellNIB, forCellReuseIdentifier: cellName)
    }
    
    //Register CollectionViewCell
    func registerCollectionViewCell(cellName: String , to collectionView: UICollectionView)  {
        
        let cellNIB = UINib(nibName: cellName, bundle: nil)
        collectionView.register(cellNIB, forCellWithReuseIdentifier: cellName)
    }

}
