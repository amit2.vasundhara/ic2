//
//  SecondVc.swift
//  TutorialForAmzan
//
//  Created by Vasundhara Vision on 15/12/18.
//  Copyright © 2018 Vasundhara Vision. All rights reserved.
//

import UIKit

class SecondVc: UIViewController
{

    @IBOutlet var TopConstraint: NSLayoutConstraint!
    
    @IBOutlet var ImageViewwidthConstraint: NSLayoutConstraint!
    @IBOutlet var TitleLbl: UILabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()

        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        TopConstraint.constant = screenStatusBarHeight
        
        TitleLbl.text = "Visual recognition\nin real time."
        TitleLbl.font = UIFont.init(name: CustomFontWeight.bold, size: TitleLbl.Getwidth / 16)
        
        if DeviceType.IS_IPAD
        {
            ImageViewwidthConstraint = ImageViewwidthConstraint.setMultiplier(multiplier: 0.75)
            TitleLbl.font = UIFont.init(name: CustomFontWeight.bold, size: TitleLbl.Getwidth / 20)
        }
        
        self.view.layoutIfNeeded()
        

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

