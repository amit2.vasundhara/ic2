//
//  FifthVC.swift
//  TutorialForAmzan
//
//  Created by Vasundhara Vision on 17/12/18.
//  Copyright © 2018 Vasundhara Vision. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class FifthVC: UIViewController ,TTTAttributedLabelDelegate
{

    @IBOutlet var TopConstraint: NSLayoutConstraint!
    
    @IBOutlet var ImageViewwidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var TitleLbl: TTTAttributedLabel!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
  
        
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        TopConstraint.constant = screenStatusBarHeight
        
        
        TitleLbl.font = UIFont.init(name: CustomFontWeight.bold, size: TitleLbl.Getwidth / 16)
        
        if DeviceType.IS_IPAD
        {
            ImageViewwidthConstraint = ImageViewwidthConstraint.setMultiplier(multiplier: 0.75)
            TitleLbl.font = UIFont.init(name: CustomFontWeight.bold, size: TitleLbl.Getwidth / 20)
        }
        self.view.layoutIfNeeded()
        
        let linkAttributes: [String: Any] = [
            NSAttributedString.Key.foregroundColor.rawValue: hexStringToUIColor(hex: "3754C9").cgColor,
            NSAttributedString.Key.underlineStyle.rawValue: false,
        ]
        
        let str1 : NSString = "Developers!\nGive Your Apps Visual AI\napp.chooch.ai"
        TitleLbl.delegate = self
        TitleLbl.linkAttributes = linkAttributes
        TitleLbl.text = str1 as String
        let range2 : NSRange = str1.range(of: "app.chooch.ai")
        TitleLbl.addLink(to: URL(string: "rgggsg"), with: range2)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        TitleLbl.addGestureRecognizer(tap)
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        if let url = NSURL(string: "https://app.chooch.ai"){
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
    }
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        
        if label == TitleLbl
        {
          print("Helloworld")
        }
        
    }

   

}
