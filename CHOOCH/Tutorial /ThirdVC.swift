//
//  ThirdVC.swift
//  TutorialForAmzan
//
//  Created by Vasundhara Vision on 17/12/18.
//  Copyright © 2018 Vasundhara Vision. All rights reserved.
//

import UIKit

class ThirdVC: UIViewController
{
    
    @IBOutlet var TopConstraint: NSLayoutConstraint!
 
    @IBOutlet var ImageViewwidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var TitleLbl: UILabel!
    override func viewDidLoad()
    {
        super.viewDidLoad()
      

        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        TopConstraint.constant = screenStatusBarHeight
        
        TitleLbl.text = "Train Chooch to recognize\n objects faces and more."
        TitleLbl.font = UIFont.init(name: CustomFontWeight.bold, size: TitleLbl.Getwidth / 16)
        
        if DeviceType.IS_IPAD
        {
            ImageViewwidthConstraint = ImageViewwidthConstraint.setMultiplier(multiplier: 0.75)
            TitleLbl.font = UIFont.init(name: CustomFontWeight.bold, size: TitleLbl.Getwidth / 20)
        }
        self.view.layoutIfNeeded()
        
      
    }
   

}
