//
//  TutorialViewController.swift
//  UIPageViewController Post
//
//  Created by Jeffrey Burt on 2/3/16.
//  Copyright © 2016 Seven Even. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    
    var tutorialPageViewController: TutorialPageViewController? {
        didSet {
            tutorialPageViewController?.tutorialDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        appDelegate.restrictRotation = .portrait
        pageControl.addTarget(self, action: Selector(("didChangePageControlValue")), for: .valueChanged)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let tutorialPageViewController = segue.destination as? TutorialPageViewController {
            self.tutorialPageViewController = tutorialPageViewController
        }
    }
    
    @IBAction func didTapNextButton(_ sender: Any) {
//        tutorialPageViewController?.scrollToNextViewController()
        
        userDefault.set(false, forKey: "FirstTime")
        
        if userDefault.value(forKey: custUserame) != nil
        {
            appDelegate.restrictRotation = .all
            let Viewc = mainStoryBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            
            appDelegate.window?.rootViewController = Viewc
            appDelegate.window?.makeKeyAndVisible()
        }
        else
        {
            let loginNC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginNavigationController") as! UINavigationController
            
            window?.rootViewController = loginNC
            window?.makeKeyAndVisible()
        }
        
    }
    
    /**
     Fired when the user taps on the pageControl to change its current page.
     */
    func didChangePageControlValue() {
        
        
    }
}

extension TutorialViewController: TutorialPageViewControllerDelegate {
    
    func tutorialPageViewController(tutorialPageViewController: TutorialPageViewController, didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func tutorialPageViewController(tutorialPageViewController: TutorialPageViewController, didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
    

    
    
}
