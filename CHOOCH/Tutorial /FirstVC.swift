//
//  ViewController.swift
//  TutorialForAmzan
//
//  Created by Vasundhara Vision on 15/12/18.
//  Copyright © 2018 Vasundhara Vision. All rights reserved.
//

import UIKit

class FirstVC: UIViewController {
    
//    @IBOutlet var TopConstraint: NSLayoutConstraint!
    @IBOutlet var TitleLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
   
    
    override func viewDidLayoutSubviews() {
//        TopConstraint.constant = screenStatusBarHeight
//        self.view.layoutIfNeeded()
        TitleLbl.text = "Visual AI That's Better\nThan The Human Eye."
        TitleLbl.font = UIFont.init(name: CustomFontWeight.bold, size: TitleLbl.Getwidth / 16)

    }
    
    
}
