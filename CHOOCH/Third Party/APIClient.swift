//
//  AMAPIClient.swift
//  
//  Created by AlphaVed Mac on 12/09/18.
//  Copyright © 2018 AlphaVed. All rights reserved.
//

import UIKit
import AFNetworking

//class APIClient: NSObject { //Using URLSession
//
//    class var sharedInstance : APIClient {
//
//        struct Static {
//
//            static var instance : APIClient = APIClient()
//        }
//        return Static.instance
//    }
//
//    //MARK:- Get API Call
//    
//    func getAPICall(urlString: String!, completionHandler:@escaping (Any?, URLResponse?, Error?)->()) ->() {
//
//        print("Calling API: \(urlString ?? "NOURL")")
//        
//        
//        var request = URLRequest(url: URL(string: urlString)!)
//        request.httpMethod = "GET"
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        
//        let urlSession = URLSession.shared
//        
//        let dataTask = urlSession.dataTask(with: request) { (data,response,error) in
//            
//            if let httpResponse = response as? HTTPURLResponse {
//                print(httpResponse.statusCode)
//            }
//            
//            if error != nil {
//                
//                print(error!.localizedDescription)
//            }
//            else{
//
//                do {
//                                        print(String(data: data!, encoding: String.Encoding.utf8)!)
//                    let resultJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
//                    completionHandler(resultJson, response, nil)
//                    
//                }
//                catch {
//
//                    print("Error -> \(error)")
//                    completionHandler(nil, response, error)
//                }
//            }
//
//        }
//        dataTask.resume()
//    }
//
//    
//    func getAPICall(urlString: String!, parameters: Any!, completionHandler:@escaping (Any?, URLResponse?, Error?)->()) ->() {
//
//        print("Calling API: \(urlString ?? "NOURL")")
//        print("Params : \(parameters)")
//
//        var request = URLRequest(url: URL(string: urlString)!)
//        
//        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
//        
//        request.httpMethod = "GET"
//        
//        request.httpBody = (parameters as! String).data(using: .utf8)
//        
//        let urlSession = URLSession.shared
//        let dataTask = urlSession.dataTask(with: request) { (data,response,error) in
//
//            if let httpResponse = response as? HTTPURLResponse {
//                print(httpResponse.statusCode)
//            }
//
//            if error != nil {
//
//                print(error!.localizedDescription)
//            }
//            else{
//
//                do {
//                    print(String(data: data!, encoding: String.Encoding.utf8)!)
//                    let resultJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
//                    completionHandler(resultJson, response, nil)
//
//                }
//                catch {
//
//                    print("Error -> \(error)")
//                    completionHandler(nil, response, error)
//                }
//            }
//
//        }
//        dataTask.resume()
//    }
//
//
//    //MARK:- POST API Call
//    
//    func postAPICall(urlString: String!, parameters: Any!, completionHandler:@escaping (Any?, URLResponse?, Error?)->()) ->() {
//
//        print("Calling API: \(urlString ?? "NOURL")")
//        print("Params : \(parameters)")
//        
//        var request = URLRequest(url: URL(string: urlString)!)
//        
//        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//        request.setValue("application/json", forHTTPHeaderField: "Accept")
//        
//        request.httpMethod = "POST"
//
//        request.httpBody = (parameters as! String).data(using: .utf8)
//        
//        let urlSession = URLSession.shared
//        let dataTask = urlSession.dataTask(with: request) { (data,response,error) in
//            
//            if error != nil {
//                
//                completionHandler(nil, response, error)
//            }
//            else{
//                do {
//                    
//                    print(String(data: data!, encoding: String.Encoding.utf8)!)
//                    let resultJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
//                    completionHandler(resultJson, response, nil)
//                    
//                }
//                catch {
//                    completionHandler(nil, response, error)
//                }
//            }
//            
//        }
//
//        dataTask.resume()
//    }
//
//}



class AFNetworkingAPIClient: NSObject,NSURLConnectionDataDelegate { //Using AFNetworking

    let manager : AFHTTPSessionManager = AFHTTPSessionManager()

    //MARK:- Create Instance of Class
    class var sharedInstance : AFNetworkingAPIClient {

        struct Static {

            static let instance : AFNetworkingAPIClient = AFNetworkingAPIClient()
        }
        return Static.instance
    }

    //MARK:- GET API Call
    func getAPICall(url: String, parameters: NSDictionary!, completionHandler:@escaping (URLSessionDataTask, NSDictionary?, Error?)->()) ->() {

        print("Calling API: \(url)")

        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments) as AFJSONResponseSerializer
        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as? Set<String>

        manager.requestSerializer = AFJSONRequestSerializer() as AFJSONRequestSerializer
        manager.requestSerializer.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData

        manager.get(url as String, parameters: parameters, progress: nil, success: { (operation, responseObject) in

//            print("Response: \(responseObject!)")
            completionHandler(operation, responseObject! as? NSDictionary , Error?.self as? Error)

        }) {(operation, error) in

            print("Error: " + error.localizedDescription)
            completionHandler(operation!, nil ,error)
        }
    }
    
    //MARK:- GET API Call Token
    
    func getAPICallWithToken(url: String, token : String, parameters: NSDictionary!, completionHandler:@escaping (URLSessionDataTask, NSDictionary?, Error?)->()) ->() {
        
        print("Calling API: \(url)")
        
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments) as AFJSONResponseSerializer
        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as? Set<String>
        
        manager.requestSerializer = AFJSONRequestSerializer() as AFJSONRequestSerializer
        manager.requestSerializer.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        manager.requestSerializer.setValue("Token \(token)", forHTTPHeaderField: "Authorization")
        
        manager.get(url as String, parameters: parameters, progress: nil, success: { (operation, responseObject) in
            
            //            print("Response: \(responseObject!)")
            completionHandler(operation, responseObject! as? NSDictionary , Error?.self as? Error)
            
        }) {(operation, error) in
            
            print("Error: " + error.localizedDescription)
            completionHandler(operation!, nil ,error)
        }
    }

    //MARK:- POST API Call

    //POST API
    func postAPICall(url: String, parameters: NSDictionary!, completionHandler:@escaping (URLSessionDataTask, Any?, Error?)->()) ->() {

        print("Calling API: \(url)")

        manager.securityPolicy.allowInvalidCertificates = true
        manager.securityPolicy.validatesDomainName = false
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments) as AFJSONResponseSerializer
        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as? Set<String>

        manager.post(url as String, parameters: parameters, progress: nil, success: { (operation, responseObject) in

//            print("Response: \(responseObject!)")

            completionHandler(operation,responseObject, Error?.self as? Error)

        }) { (operation, error) in

            print("Error: " + error.localizedDescription)
            completionHandler(operation!, nil ,error)
        }
    }
    
    //POST API WITH TOKEN
    
    func postAPICallWithToken(url: String, parameters: NSDictionary!, token : String, completionHandler:@escaping (URLSessionDataTask, Any?, Error?)->()) ->() {
        
        print("Calling API: \(url)")
        
        manager.securityPolicy.allowInvalidCertificates = true
        manager.securityPolicy.validatesDomainName = false
        
        manager.requestSerializer.setValue("Token \(token)", forHTTPHeaderField: "Authorization")
        
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments) as AFJSONResponseSerializer
        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as? Set<String>
        
        manager.post(url as String, parameters: parameters, progress: nil, success: { (operation, responseObject) in
            
            //            print("Response: \(responseObject!)")
            
            completionHandler(operation,responseObject, Error?.self as? Error)
            
        }) { (operation, error) in
            
            print("Error: " + error.localizedDescription)
            completionHandler(operation!, nil ,error)
        }
    }
    
    //PATCH API WITH TOKEN
    
    func patchAPICallWithToken(url: String, parameters: NSDictionary!, token : String, completionHandler:@escaping (URLSessionDataTask, Any?, Error?)->()) ->() {
        
        print("Calling API: \(url)")
        
        manager.securityPolicy.allowInvalidCertificates = true
        manager.securityPolicy.validatesDomainName = false
        
        manager.requestSerializer.setValue("Token \(token)", forHTTPHeaderField: "Authorization")
        
        manager.responseSerializer = AFJSONResponseSerializer(readingOptions: JSONSerialization.ReadingOptions.allowFragments) as AFJSONResponseSerializer
        manager.responseSerializer.acceptableContentTypes = NSSet(objects:"application/json", "text/html", "text/plain", "text/json", "text/javascript", "audio/wav") as? Set<String>
    
        manager.patch(url as String, parameters: parameters, success: { (operation, responseObject) in
            //            print("Response: \(responseObject!)")
            
            completionHandler(operation,responseObject, Error?.self as? Error)
            
        }) { (operation, error) in
            
            print("Error: " + error.localizedDescription)
            completionHandler(operation!, nil ,error)
        }
    }

}
