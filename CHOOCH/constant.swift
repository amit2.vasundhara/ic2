//
//  constant.swift
//  CHOOCH
//
//  Created by Vasundhara Vision on 27/09/17.
//  Copyright © 2017 Vasundhara Vision. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration

var accessKey = "..."
var secretKey = "..."

var usingFrontCamera = false

var window = UIApplication.shared.keyWindow
var appIsOpen = true

class MyButton: UIButton {
    var id : String?
    var params : NSDictionary?
    var userName : String?
    
}

//old api key = c6a1693b-5e33-48f5-ac66-6220f0387296
//new api key = 69c8f610-76b5-451e-81d6-3d92862d18d8
let apikey = "69c8f610-76b5-451e-81d6-3d92862d18d8"

//********************** USER DEFAULT CONSTANT **********************
let userDefault                        = UserDefaults.standard

//********************** APP CONSTANT STRING **********************
let appDelegate                        = UIApplication.shared.delegate as! AppDelegate

//MARK:-********************** UI CONSTANT **********************

let mainStoryBoard                     = UIStoryboard(name: "Main", bundle: nil)

//MARK:- ********************** DEVICE INFORMATION **********************
struct ScreenSize {
    
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    
    static let IS_IPHONE            = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_7          = IS_IPHONE_6
    static let IS_IPHONE_7P         = IS_IPHONE_6P
    static let IS_IPHONE_8          = IS_IPHONE_7
    static let IS_IPHONE_8P         = IS_IPHONE_7P
    static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPAD_PRO_10_5     = IS_IPAD && ScreenSize.SCREEN_MAX_LENGTH == 1112.0
    static let IS_IPAD_PRO_11       = IS_IPAD && ScreenSize.SCREEN_MAX_LENGTH == 1194.0
    static let IS_IPAD_PRO_12_9     = IS_IPAD && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
    static let IS_TV                = UIDevice.current.userInterfaceIdiom == .tv
    static let IS_CAR_PLAY          = UIDevice.current.userInterfaceIdiom == .carPlay
}

var counter = true

public struct CustomFontWeight {
    
    static let reguler    = "Quicksand-Regular"
    static let medium     = "Quicksand-Medium"
    static let bold       = "Quicksand-Bold"
    static let light       = "Quicksand-Light"
    static let MyriadItalic = "Myriad Pro"
}

let appURL = "https://itunes.apple.com/us/app/chooch-ic2/id1304120928?mt=8"

let custUserame = "custUserame"
let custToken = "custToken"
let custId = "custId"


open class Reachability123 {
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}

func CompareImage (lhs: UIImage, rhs: UIImage) -> Bool
{
    guard let data1 = UIImagePNGRepresentation(lhs),
        let data2 = UIImagePNGRepresentation(rhs)
        else { return false }
    
    return data1 == data2
}

func ConfigureNodataLbl(viewController : UIViewController) -> UILabel {
    
    let nodataLbl = UILabel(frame: CGRect(x: 0, y: 100, width: viewController.view.Getwidth, height: 50))
    nodataLbl.text = "No data available"
    nodataLbl.font = UIFont.init(name: CustomFontWeight.bold, size: 20.0)
    nodataLbl.textColor = UIColor.black
    nodataLbl.textAlignment = .center
    nodataLbl.center = viewController.view.center
    nodataLbl.isHidden = true
    
    return nodataLbl
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

var screenStatusBarHeight: CGFloat {
    return UIApplication.shared.statusBarFrame.height
}

//var isHiddenToast = false

extension UIView {
    
    func fadeIn(_ duration: TimeInterval = 0.1, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        DispatchQueue.main.async {
        self.layer.removeAllAnimations()
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
                self.alpha = 1.0
        }, completion: completion)  }
    }
    
    func fadeInoutAnimation(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        
//        UIView.animate(withDuration: duration, delay: delay, options: .allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat, animations: {
//                self.alpha = !isHiddenToast ? 1.0 : 0.0
//            print("1")
//        }) { (yes) in
//            UIView.animate(withDuration: duration, delay: delay, options: .curveEaseIn, animations: {
//                self.alpha = !isHiddenToast ? 0.5 : 0.0
//                print("0.5")
//            }) { (yes) in
//                    if !isHiddenToast {
//                        self.fadeInoutAnimation()
//                        print("Repeat")
//                    }
//                }
//            }
        
        DispatchQueue.main.async {
          
            UIView.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
                self.alpha = 1.0
            }) { (yes) in
                UIView.animate(withDuration: duration,
                               delay: duration,
                               options:[.beginFromCurrentState , .allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                               animations: { self.alpha = 0.5 },
                               completion: nil)
                
            }
        
            
        
            
        }
        
        
    }
    
    func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
            self.alpha = 0.0
        }) { (yes) in
            self.removeFromSuperview()
        }
    }
   
    func fadeOutPoorConnection(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        
        DispatchQueue.main.async {
            self.layer.removeAllAnimations()
            UIView.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
                self.alpha = 0.0
            }) { (yes) in
                self.layer.removeAllAnimations()
                self.alpha = 0.0
            }
        }
    }
    
    func dropShadow1(cornerRadius : CGFloat) {
        
        var shadowLayer: CAShapeLayer!
        let fillColor: UIColor = .white
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = fillColor.cgColor
            
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: -2.0, height: 2.0)
            shadowLayer.shadowOpacity = 0.8
            shadowLayer.shadowRadius = 2
            
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    @IBInspectable var shadow11: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow11()
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            
            // Don't touch the masksToBound property if a shadow is needed in addition to the cornerRadius
            if shadow == false {
                self.layer.masksToBounds = true
            }
        }
    }
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 1.0, height: 2.0),
                   shadowOpacity: Float = 0.7,
                   shadowRadius: CGFloat = 3.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    func addShadow11(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 0.0, height: 0.0),
                   shadowOpacity: Float = 0.2,
                   shadowRadius: CGFloat = 10.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
    }
    
    func addShadow(color: UIColor, opacity: Float = 0.6, offSet: CGSize, radius: CGFloat = 5.0, scale: Bool = true) {

        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOpacity = opacity
        self.layer.shadowOffset = offSet
        self.layer.shadowRadius = radius

        self.layer.cornerRadius = 2.0
    }
    
    func setRoundCornerRadius() -> CGFloat {
        
        return self.Getwidth / 2
    }
    
    func Set_Corner_image(width:CGFloat,height:CGFloat,Corners:UIRectCorner) {
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: Corners, cornerRadii: CGSize(width: width, height: height)).cgPath
        self.layer.mask = rectShape
        
    }
    
    func Set_Corner_image_withborder(width:CGFloat,height:CGFloat,Corners:UIRectCorner) {
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: Corners, cornerRadii: CGSize(width: width, height: height)).cgPath
        self.layer.mask = rectShape
        self.layer.borderWidth = 1.0
        
    }
    
    func scale(by scale: CGFloat) {
        self.contentScaleFactor = scale
        for subview in self.subviews {
            subview.scale(by: scale)
        }
    }
    
    func getImage(scale: CGFloat? = nil) -> UIImage
    {
        
        self.layer.mask = nil
        let newScale = scale ?? UIScreen.main.scale
        self.scale(by: newScale)
        let format = UIGraphicsImageRendererFormat()
        format.scale = newScale
        
        let renderer = UIGraphicsImageRenderer(size: self.bounds.size, format: format)
        
        let image = renderer.image { rendererContext in
            self.layer.render(in: rendererContext.cgContext)
        }
        
        return image
    }
    
    var Getwidth : CGFloat {
        get { return self.frame.size.width  }
        set { self.frame.size.width = newValue }
    }
    
    var Getheight : CGFloat {
        get { return self.frame.size.height  }
        set { self.frame.size.height = newValue }
    }
    
    var Getsize:       CGSize  { return self.frame.size}
    
    var Getorigin:     CGPoint { return self.frame.origin }
    var Getx : CGFloat {
        get { return self.frame.origin.x  }
        set { self.frame.origin.x = newValue }
    }
    
    var Gety : CGFloat {
        get { return self.frame.origin.y  }
        set { self.frame.origin.y = newValue }
    }
    
    
    var Getleft:       CGFloat { return self.frame.origin.x }
    var Getright:      CGFloat { return self.frame.origin.x + self.frame.size.width }
    var Gettop:        CGFloat { return self.frame.origin.y }
    var Getbottom:     CGFloat { return self.frame.origin.y + self.frame.size.height }
    
    
}



extension Int {
    static func parse(from string: String) -> Int? {
        return Int(string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }
}

extension UIViewController {
    
    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
    
    var topbarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
}

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable override var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

extension Dictionary {
    
    var json: String {
        
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    var jsonwithRawdata : String {
        
        let cookieHeader = (self.compactMap({ (key, value) -> String in
            return "\(key):\(value)"
        }) as Array).joined(separator: ",")
        
        let contentString = "{" + cookieHeader + "}"
        
        return contentString
    }
    
    
    func printJson() {
        
        print(jsonwithRawdata)
    }
    
    func jsonString() -> NSString? {
        let jsonData = try? JSONSerialization.data(withJSONObject: self, options: [])
        guard jsonData != nil else {return nil}
        let jsonString = String(data: jsonData!, encoding: .utf8)
        guard jsonString != nil else {return nil}
        return jsonString! as NSString
    }
    
}

extension UIImage {
    
        func resize(targetSize: CGSize) -> UIImage {
            return UIGraphicsImageRenderer(size:targetSize).image { _ in
                self.draw(in: CGRect(origin: .zero, size: targetSize))
            }
        }
    
    func saveToDocuments(filename:String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let documentDirectoryFileUrl = documentsDirectory.appendingPathComponent("MakeZip")
        if !FileManager.default.fileExists(atPath: documentDirectoryFileUrl.path) {
            do {
                try FileManager.default.createDirectory(atPath: documentDirectoryFileUrl.path, withIntermediateDirectories: false, attributes: nil)
            } catch {
                print("Could not copy file: \(error)")
            }
        }
        
        let fileURL = documentsDirectory.appendingPathComponent(filename)
        if let data = UIImageJPEGRepresentation(self, 1.0) {
            do {
                try data.write(to: fileURL)
            } catch {
                print("error saving file to documents:", error)
            }
        }
    }
    
    func saveToDocumentsforGallaryZip(filename:String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let documentDirectoryFileUrl = documentsDirectory.appendingPathComponent("MakeZipGallary")
        if !FileManager.default.fileExists(atPath: documentDirectoryFileUrl.path) {
            do {
                try FileManager.default.createDirectory(atPath: documentDirectoryFileUrl.path, withIntermediateDirectories: false, attributes: nil)
            } catch {
                print("Could not copy file: \(error)")
            }
        }
        
        let fileURL = documentsDirectory.appendingPathComponent(filename)
        if let data = UIImageJPEGRepresentation(self, 1.0) {
            do {
                try data.write(to: fileURL)
            } catch {
                print("error saving file to documents:", error)
            }
        }
    }
    
        func resizeWithPercent(percentage: CGFloat) -> UIImage? {
            let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
            imageView.contentMode = .scaleAspectFit
            imageView.image = self
            UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
            guard let context = UIGraphicsGetCurrentContext() else { return nil }
            imageView.layer.render(in: context)
            guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
            UIGraphicsEndImageContext()
            return result
        }
        func resizeWithWidth(width: CGFloat) -> UIImage? {
            let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
            imageView.contentMode = .scaleAspectFit
            imageView.image = self
            UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
            guard let context = UIGraphicsGetCurrentContext() else { return nil }
            imageView.layer.render(in: context)
            guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
            UIGraphicsEndImageContext()
            return result
        }
    
        
//        func compressImage() -> UIImage? {
//            // Reducing file size to a 10th
//            var actualHeight: CGFloat = self.size.height
//            var actualWidth: CGFloat = self.size.width
//            let maxHeight: CGFloat = 1136.0
//            let maxWidth: CGFloat = 640.0
//            var imgRatio: CGFloat = actualWidth/actualHeight
//            let maxRatio: CGFloat = maxWidth/maxHeight
//            var compressionQuality: CGFloat = 0.5
//
//            if actualHeight > maxHeight || actualWidth > maxWidth {
//                if imgRatio < maxRatio {
//                    //adjust width according to maxHeight
//                    imgRatio = maxHeight / actualHeight
//                    actualWidth = imgRatio * actualWidth
//                    actualHeight = maxHeight
//                } else if imgRatio > maxRatio {
//                    //adjust height according to maxWidth
//                    imgRatio = maxWidth / actualWidth
//                    actualHeight = imgRatio * actualHeight
//                    actualWidth = maxWidth
//                } else {
//                    actualHeight = maxHeight
//                    actualWidth = maxWidth
//                    compressionQuality = 1
//                }
//            }
//            let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
//            UIGraphicsBeginImageContext(rect.size)
//            self.draw(in: rect)
//            guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
//                return nil
//            }
//            UIGraphicsEndImageContext()
//            guard let imageData = img.jpegData(compressionQuality: compressionQuality) else {
//                return nil
//            }
//            return UIImage(data: imageData)
//        }

    func compressImage() -> UIImage? {
        // Reducing file size to a 10th
        var actualHeight: CGFloat = self.size.height
        var actualWidth: CGFloat = self.size.width
        let maxHeight: CGFloat = 1024.0
        let maxWidth: CGFloat = 768.0
        var imgRatio: CGFloat = actualWidth/actualHeight
        let maxRatio: CGFloat = maxWidth/maxHeight
        var compressionQuality: CGFloat = 0.5
        
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
                compressionQuality = 1
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        self.draw(in: rect)
        guard let img = UIGraphicsGetImageFromCurrentImageContext() else {
            return nil
        }
        UIGraphicsEndImageContext()
        guard let imageData = UIImageJPEGRepresentation(img, compressionQuality) else {
            return nil
        }
        return UIImage(data: imageData)
    }
    
    func imageWithBorder(width: CGFloat, color: UIColor , cornorRadius : CGFloat) -> UIImage? {
        let square = CGSize(width: min(size.width, size.height) + width * 2, height: min(size.width, size.height) + width * 2)
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = .center
        imageView.image = self
        imageView.layer.borderWidth = width
        imageView.layer.borderColor = color.cgColor
        imageView.layer.cornerRadius = cornorRadius
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
    
    
    func crop(to:CGSize) -> UIImage
    {
        guard let cgimage = self.cgImage else { return self }
        
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        
        let contextSize: CGSize = contextImage.size
        
        //Set to square
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        let cropAspect: CGFloat = to.width / to.height
        
        var cropWidth: CGFloat = to.width
        var cropHeight: CGFloat = to.height
        
        if to.width > to.height { //Landscape
            cropWidth = contextSize.width
            cropHeight = contextSize.width / cropAspect
            posY = (contextSize.height - cropHeight) / 2
        } else if to.width < to.height { //Portrait
            cropHeight = contextSize.height
            cropWidth = contextSize.height * cropAspect
            posX = (contextSize.width - cropWidth) / 2
        } else { //Square
            if contextSize.width >= contextSize.height { //Square on landscape (or square)
                cropHeight = contextSize.height
                cropWidth = contextSize.height * cropAspect
                posX = (contextSize.width - cropWidth) / 2
            }else{ //Square on portrait
                cropWidth = contextSize.width
                cropHeight = contextSize.width / cropAspect
                posY = (contextSize.height - cropHeight) / 2
            }
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cropWidth, height: cropHeight)
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let cropped: UIImage = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
        
        UIGraphicsBeginImageContextWithOptions(to, true, self.scale)
        cropped.draw(in: CGRect(x: 0, y: 0, width: to.width, height: to.height))
        let resized = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return resized!
    }
    
    func imageByMakingWhiteBackgroundTransparent() -> UIImage? {
        
        let image = UIImage(data: UIImageJPEGRepresentation(self, 1.0)!)!
        let rawImageRef: CGImage = image.cgImage!
        
        let colorMasking: [CGFloat] = [222, 255, 222, 255, 222, 255]
        UIGraphicsBeginImageContext(image.size);
        
        let maskedImageRef = rawImageRef.copy(maskingColorComponents: colorMasking)
        UIGraphicsGetCurrentContext()?.translateBy(x: 0.0,y: image.size.height)
        UIGraphicsGetCurrentContext()?.scaleBy(x: 1.0, y: -1.0)
        UIGraphicsGetCurrentContext()?.draw(maskedImageRef!, in: CGRect.init(x: 0, y: 0, width: image.size.width, height: image.size.height))
        let result = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return result
        
    }

    
}

//extension NSLayoutConstraint {
//    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
//        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
//    }
//}

extension NSLayoutConstraint {
    
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {
        
        NSLayoutConstraint.deactivate([self])
        
        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)
        
        newConstraint.priority = priority
        newConstraint.shouldBeArchived = shouldBeArchived
        newConstraint.identifier = identifier
        
        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

extension Data {
    
    func saveToDocumentsforGZip(filename:String) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let fileURL = documentsDirectory.appendingPathComponent(filename)
        do {
            try self.write(to: fileURL)
        } catch {
            print("error saving file to documents:", error)
        }
    }
}


extension UIApplication {
    
    func applicationVersion() -> String {
        
        return Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
    }
    
    func applicationBuild() -> String {
        
        return Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as! String
    }
    
    func versionBuild() -> String {
        
        let version = self.applicationVersion()
        let build = self.applicationBuild()
        
        return "v\(version)(\(build))"
    }
    
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}

func getCurrentViewController()-> UIViewController{
    
    return UIApplication.shared.topMostViewController()!
}


extension String {
    func stringByAddingPercentEncodingForRFC3986() -> String? {
//        let unreserved = "-._~/?"
//        let allowed = NSMutableCharacterSet.alphanumeric()
//        allowed.addCharacters(in: unreserved)
//        return stringByAddingPercentEncodingWithAllowedCharacters(allowed)
        
        
        var allowed = CharacterSet.alphanumerics
        allowed.insert(charactersIn: "-._~") // as per RFC 3986
        let encoded = self.addingPercentEncoding(withAllowedCharacters: allowed)
        return encoded!
    }
    
    func chopPrefix(_ count: Int = 1) -> String {
        return substring(from: index(startIndex, offsetBy: count))
    }
        
    func chopSuffix(_ count: Int = 1) -> String {
        return substring(to: index(endIndex, offsetBy: -count))
    }
    
    /// Create `Data` from hexadecimal string representation
    ///
    /// This creates a `Data` object from hex string. Note, if the string has any spaces or non-hex characters (e.g. starts with '<' and with a '>'), those are ignored and only hex characters are processed.
    ///
    /// - returns: Data represented by this hexadecimal string.
    
    var hexadecimal: Data? {
        var data = Data(capacity: characters.count / 2)
        
        let regex = try! NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
        regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
            let byteString = (self as NSString).substring(with: match!.range)
            let num = UInt8(byteString, radix: 16)!
            data.append(num)
        }
        
        guard data.count > 0 else { return nil }
        
        return data
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }

    
}


extension NSTextAttachment {
    static func getCenteredImageAttachment(with imageName: UIImage, and
        font: UIFont) -> NSTextAttachment? {
        let imageAttachment = NSTextAttachment()
        
        imageAttachment.bounds = CGRect(x: 0, y: (font.capHeight - imageName.size.height).rounded() / 2, width: imageName.size.width, height: imageName.size.height)
        imageAttachment.image = imageName
        
        return imageAttachment
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

