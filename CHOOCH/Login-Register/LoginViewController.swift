//
//  LoginViewController.swift
//  CHOOCH
//
//  Created by des on 17/05/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    
    // MARK: - Outlates
    
    @IBOutlet var emai_TF: UITextField!
    @IBOutlet var password_TF: UITextField!
    
    @IBOutlet var imgView_ChoochLogo: UIImageView!
    // MARK: - Variables
    
    
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTF(textfield: emai_TF)
        configureTF(textfield: password_TF)
        

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(sender:)))
        self.imgView_ChoochLogo.addGestureRecognizer(tap)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                self.popViewController()
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        
    }
    

    @IBAction func Login_Pressed(_ sender: UIButton) {

        if isReadyToLogin()
        {
            showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
            
            let ApiURL = "https://app.chooch.ai/app/login"
            
            let ParamDict = ["email": "\(self.emai_TF.text ?? "")",
                            "password": self.password_TF.text ?? ""] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: ApiURL, parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                        let responceDict = Responce as! [String:Any]
                        
                        print(responceDict)
                    
                    if responceDict["success"] as! Int == 1
                    {
                        let dataDict = responceDict["data"] as? [String : Any] ?? [String : Any]()
                    
                        let username = "\(dataDict["username"] ?? "")"
                        userDefault.set(username, forKey: custUserame)
                        
                        let token = "\(dataDict["token"] ?? "")"
                        userDefault.set(token, forKey: custToken)
                        
                        let userid = "\(dataDict["id"] ?? "")"
                        userDefault.set(userid, forKey: custId)
                        
                        appDelegate.restrictRotation = .all
                        let Viewc = mainStoryBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        
                        appDelegate.window?.rootViewController = Viewc
                        appDelegate.window?.makeKeyAndVisible()
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK", completion: { (Index) in
                        })
                    }
                    
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (Index) in
                    })
                }
                
            }
        }
        
    }
    
    @IBAction func ForgotPass_Pressed(_ sender: UIButton) {
        
        self.emai_TF.text = nil
        self.password_TF.text = nil
        
        let forgetpassVC = mainStoryBoard.instantiateViewController(withIdentifier: "ForgetPasswordViewController") as! ForgetPasswordViewController
        self.pushViewController(forgetpassVC)
        
    }
    
    
    //MARK:- ********************** VALIDATION **********************
    
    func isReadyToLogin()-> Bool {
        
        if Validation.isStringEmpty(self.password_TF.text!) {
            presentAlertWithTitle(title: "Please fill in all fields", message: "", options: "OK") { (Int) in
            }
        }
        else if !Validation.isValidEmail(emailString: self.emai_TF.text!) {
            presentAlertWithTitle(title: "Please enter a valid email address", message: "", options: "OK") { (Int) in
            }
        }
        else{
            return true
        }
        
        return false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
