//
//  OTPViewController.swift
//  CHOOCH
//
//  Created by des on 28/05/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit
import SVPinView

class OTPViewController: BaseViewController {
    
    @IBOutlet var pinView: SVPinView!
    var emailStr = String()

    @IBOutlet var choochLogo_imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pinView.font = UIFont.init(name: CustomFontWeight.medium, size: 23)!

        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        // Do any additional setup after loading the view.
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                self.popViewController()
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    @IBAction func Submit_Pressed(_ sender: UIButton) {
        
        if pinView.getPin().count == 6
        {
            showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
            
            let ApiURL = "https://app.chooch.ai/app/verify-forgot-password-otp"
            
            let ParamDict = ["email": "\(emailStr)",
                "otp": pinView.getPin()] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: ApiURL, parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    print(responceDict)
                    
                    if responceDict["success"] as! Int == 1
                    {
                        let changePassVC = mainStoryBoard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                        changePassVC.dataDict = responceDict["data"] as? [String:Any] ?? [String:Any]()
                        self.pushViewController(changePassVC)
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK") { (ButtonIndex) in
                        }
                    }
                    
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (ButtonIndex) in
                        self.popViewController()
                    })
                }
                
            }
        
            
        }
        else
        {
            self.presentAlertWithTitle(title: "Please fill One Time Password first", message: "", options: "OK", completion: { (Index) in
            })
        }
        
        
    }
    
    @IBAction func Resend_Pressed(_ sender: UIButton) {
        
        showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
        
        let ApiURL = "https://app.chooch.ai/app/forgot-password"
        
        let ParamDict = ["email": "\(emailStr)"] as NSDictionary
        
        AFNetworkingAPIClient.sharedInstance.postAPICall(url: ApiURL, parameters: ParamDict) { (APIResponce, Responce, error1) in
            
            self.RemoveLoader()
            
            if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
            {
                let responceDict = Responce as! [String:Any]
                
                print(responceDict)
                
                    self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK", completion: { (Index) in

                    })
                
            }
            else
            {
                self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (ButtonIndex) in
                })
            }
            
        }

        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
