//
//  ForgetPasswordViewController.swift
//  CHOOCH
//
//  Created by des on 21/05/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: BaseViewController {

    @IBOutlet var email_TF: UITextField!
    
    @IBOutlet var choochLogo_imgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTF(textfield: email_TF)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        choochLogo_imgView.addGestureRecognizer(tap)
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                self.popViewController()
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }

    

    @IBAction func Submit_Pressed(_ sender: UIButton) {
        
        if !Validation.isValidEmail(emailString: email_TF.text!) {
            
                self.presentAlertWithTitle(title: "Please enter valid email address", message: "", options: "OK") { (Int) in
                    self.email_TF.text = nil
                }
        }
        else
        {
            showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
            
            let ApiURL = "https://app.chooch.ai/app/forgot-password"
            
            let ParamDict = ["email": "\(self.email_TF.text ?? "")"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: ApiURL, parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    print(responceDict)
     
                    if responceDict["success"] as! Int == 1
                    {
//                        let dataDict = responceDict["data"] as? [String : Any] ?? [String : Any]()
//
//                        let OTPVC = mainStoryBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
//                        OTPVC.dataDict = dataDict
//                        self.pushViewController(OTPVC)
                        
                        self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK", completion: { (Index) in
                            let OTPVC = mainStoryBoard.instantiateViewController(withIdentifier: "OTPViewController") as! OTPViewController
                            OTPVC.emailStr = "\(self.email_TF.text ?? "")"
                            self.pushViewController(OTPVC)
                        })
                        
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK", completion: { (Index) in
                        })
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }
                
            }
        }
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
