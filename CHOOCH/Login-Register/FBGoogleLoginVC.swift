//
//  FBGoogleLoginVC.swift
//  CHOOCH
//
//  Created by des on 18/05/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit
import FacebookCore
import GoogleSignIn
import TTTAttributedLabel

let someErrorMSG = "Some error occured"

class FBGoogleLoginVC: BaseViewController , GIDSignInDelegate , GIDSignInUIDelegate , TTTAttributedLabelDelegate {

    @IBOutlet var TermsLbl: TTTAttributedLabel!
    
    @IBOutlet weak var withoutSigningWarningLbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDelegate.restrictRotation = .portrait
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signInSilently()

        withoutSigningWarningLbl.text = "*you will not be able to train new objects, faces or concepts without logging in"
        
        let str : NSString = "By creating an account I agree to Chooch's Terms"
        TermsLbl.delegate = self
        
        let linkAttributes: [String: Any] = [
            NSAttributedString.Key.foregroundColor.rawValue: hexStringToUIColor(hex: "F67B00").cgColor,
            NSAttributedString.Key.underlineStyle.rawValue: false,
        ]
        
        TermsLbl.linkAttributes = linkAttributes
        
        TermsLbl.text = str as String
        let range : NSRange = str.range(of: "Terms")
        TermsLbl.addLink(to: URL(string: "https://google.com"), with: range)
        
        // Do any additional setup after loading the view.
    }
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        
        let TermsVC = mainStoryBoard.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
        pushViewController(TermsVC)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    override func viewDidLayoutSubviews() {
        
        withoutSigningWarningLbl.adjustsFontSizeToFitWidth = true
        
    }
    
    // MARK: - Google Login

    @IBAction func Google_Pressed(_ sender: UIButton) {
        
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.signIn()
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            
            self.callAPIAfterSuccess(email: user.profile.email,
                                     providerName: "google",
                                     providerId: user.userID,
                                     accessToken: user.authentication.idToken,
                                     firstName: user.profile.givenName,
                                     lastName: user.profile.familyName)
        
        }
    }
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
    
    // MARK: - FB Login
    
    @IBAction func facebook_Pressed(_ sender: UIButton) {
        
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.email], viewController: self) { (loginResult) in
            
            switch loginResult
            {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(granted: _, declined: _,let token):
                print("Logged in!")
                self.getFBUserInfo(Accesstoken: token.tokenString)
            }
            
            print(loginResult)
        }
        
    }
    
    func getFBUserInfo(Accesstoken : String)
    {
        
        let request = GraphRequest(graphPath: "me", parameters: ["fields":"id, name, first_name, last_name, email"], httpMethod: .get)
        
        request.start { (connection, result, error) in
            
            let repoDict = result as! [String:Any]
            print(repoDict)
            
            self.callAPIAfterSuccess(email: "\(repoDict["email"] ?? "")",
                                providerName: "facebook",
                                providerId: "\(repoDict["id"] ?? "")",
                                accessToken: Accesstoken,
                                firstName: "\(repoDict["first_name"] ?? "")",
                                lastName: "\(repoDict["last_name"] ?? "")")
            
        }
        
    }
    
    
    func callAPIAfterSuccess(email : String ,
                             providerName : String ,
                             providerId : String ,
                             accessToken : String ,
                             firstName : String ,
                             lastName : String) {
        
        showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
        
        let ApiURL = "https://app.chooch.ai/app/login/social"
        
        let ParamDict = ["email": email,
                         "provider_name": providerName,
                         "provider_id": providerId,
                         "access_token": accessToken,
                         "first_name": firstName,
                         "last_name": lastName] as NSDictionary
        
        AFNetworkingAPIClient.sharedInstance.postAPICall(url: ApiURL, parameters: ParamDict) { (APIResponce, Responce, error1) in
            
            self.RemoveLoader()
            
            if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
            {
                let responceDict = Responce as! [String:Any]
                
                print(responceDict)
               
                if responceDict["success"] as? Int == 1
                {
                    let dataDict = responceDict["data"] as? [String : Any] ?? [String : Any]()
                    
                    let username = "\(dataDict["username"] ?? "")"
                    userDefault.set(username, forKey: custUserame)
                    
                    let token = "\(dataDict["token"] ?? "")"
                    userDefault.set(token, forKey: custToken)
                    
                    let userid = "\(dataDict["id"] ?? "")"
                    userDefault.set(userid, forKey: custId)
                    
                    appDelegate.restrictRotation = .all
                    let Viewc = mainStoryBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    
                    appDelegate.window?.rootViewController = Viewc
                    appDelegate.window?.makeKeyAndVisible()
                }
                else
                {
                    self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK", completion: { (Index) in
                    })
                }
                
                
            }
            else
            {
                self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (Index) in
                })
            }
            
        }
        
    }
    
    @IBAction func WithoutSign_Pressed(_ sender: UIButton) {
        
        appDelegate.restrictRotation = .all
        let Viewc = mainStoryBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        
        appDelegate.window?.rootViewController = Viewc
        appDelegate.window?.makeKeyAndVisible()
        
    }
    
    @IBAction func Email_Pressed(_ sender: UIButton) {
        
        let regVC = mainStoryBoard.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        pushViewController(regVC)
        
    }
    
    @IBAction func Signing_Pressed(_ sender: UIButton) {
        
        let loginVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        pushViewController(loginVC)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
