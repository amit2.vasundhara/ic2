//
//  TermsViewController.swift
//  CHOOCH
//
//  Created by des on 21/05/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class TermsViewController: BaseViewController , UIWebViewDelegate {

    @IBOutlet var TermsWebView: UIWebView!
    
    @IBOutlet var loaderActIndicator: UIActivityIndicatorView!
    
    var type = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = type == "Terms" ? "Terms" : "Privacy Policy"
        
        let backBTN = UIBarButtonItem(image: UIImage(named: "ic_back"),
                                      style: .plain,
                                      target: navigationController,
                                      action: #selector(UINavigationController.popViewController(animated:)))
        backBTN.tintColor = .black
        navigationItem.leftBarButtonItem = backBTN
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        let str = type == "Terms" ? "https://app.chooch.ai/feed/terms" : "https://app.chooch.ai/feed/privacy"
        let url = URL (string: str)
        let requestObj = URLRequest(url: url!)
        TermsWebView.loadRequest(requestObj)
        TermsWebView.scalesPageToFit = true
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        TermsWebView.scrollView.panGestureRecognizer.require(toFail: swipeRight)
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                self.popViewController()
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
        loaderActIndicator.startAnimating()
        loaderActIndicator.isHidden = false
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        loaderActIndicator.stopAnimating()
        loaderActIndicator.isHidden = true
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webView.reload()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
