//
//  ChangePasswordViewController.swift
//  CHOOCH
//
//  Created by des on 27/05/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseViewController {

    @IBOutlet var password_TF: UITextField!
    @IBOutlet var confPassword_TF: UITextField!
    
    @IBOutlet var choochLogo_imgView: UIImageView!
    
    var dataDict = [String:Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTF(textfield: password_TF)
        configureTF(textfield: confPassword_TF)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        choochLogo_imgView.addGestureRecognizer(tap)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
   
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                self.popViewController()
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    @IBAction func changePassword_Pressed(_ sender: UIButton) {
        
        if !Validation.isPasswordHasMinimum(self.password_TF.text!) {
            
            presentAlertWithTitle(title: "Your password must be at least 6 characters", message: "", options: "OK") { (Int) in
            }
        }
        else if !Validation.isPassWordMatch(pass: self.password_TF.text!, confPass: self.confPassword_TF.text!) {
            
            presentAlertWithTitle(title: "Passwords don’t match", message: "", options: "OK") { (Int) in
            }
        }
        else
        {
            showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)

            let ApiURL = "https://app.chooch.ai/app/reset-password"

            let ParamDict = ["new_password": "\(self.password_TF.text ?? "")",
                             "confirm_password": "\(self.confPassword_TF.text ?? "")",
                             "email": "\(dataDict["email"] ?? "")",
                             "otp": "\(dataDict["otp"] ?? "")"] as NSDictionary

            AFNetworkingAPIClient.sharedInstance.postAPICall(url: ApiURL, parameters: ParamDict) { (APIResponce, Responce, error1) in

                self.RemoveLoader()

                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]

                    print(responceDict)
                    
                    if responceDict["success"] as! Int == 1
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK") { (ButtonIndex) in
                            
                            for controller in self.navigationController!.viewControllers as Array {
                            
                                if controller.isKind(of: LoginViewController.self) {
                                    self.navigationController?.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                            
                        }
                        
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK") { (ButtonIndex) in
                        }
                    }
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (ButtonIndex) in
                    })
                }

            }
        }
        
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
