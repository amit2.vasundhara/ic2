//
//  RegisterViewController.swift
//  CHOOCH
//
//  Created by des on 17/05/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit
import TTTAttributedLabel

class RegisterViewController: BaseViewController ,TTTAttributedLabelDelegate{

    // MARK: - Outlates
    
    @IBOutlet var firstname_TF: UITextField!
    @IBOutlet var lastname_TF: UITextField!
    @IBOutlet var email_TF: UITextField!
    @IBOutlet var password_TF: UITextField!
    @IBOutlet var confPassword_TF: UITextField!
    
    @IBOutlet var TermsLbl: TTTAttributedLabel!
    @IBOutlet var alreadyLbl: TTTAttributedLabel!
    
    @IBOutlet var TermsTickBtn: UIButton!
    @IBOutlet var choochLogo_imgView: UIImageView!
    // MARK: - Variables
    
    
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTF(textfield: firstname_TF)
        configureTF(textfield: lastname_TF)
        configureTF(textfield: email_TF)
        configureTF(textfield: password_TF)
        configureTF(textfield: confPassword_TF)
        
        
        
        let linkAttributes: [String: Any] = [
            NSAttributedString.Key.foregroundColor.rawValue: hexStringToUIColor(hex: "F67B00").cgColor,
            NSAttributedString.Key.underlineStyle.rawValue: false,
        ]
        
        let str : NSString = "By creating an account I agree to Chooch's Terms"
        TermsLbl.delegate = self
        TermsLbl.linkAttributes = linkAttributes
        TermsLbl.text = str as String
        let range : NSRange = str.range(of: "Terms")
        TermsLbl.addLink(to: URL(string: "Hello World"), with: range)
        
        let ActivelinkAttributes: [String: Any] = [
            NSAttributedString.Key.foregroundColor.rawValue: UIColor.black.cgColor,
            NSAttributedString.Key.underlineStyle.rawValue: false,
        ]
        TermsLbl.linkAttributes = ActivelinkAttributes
        let range1 : NSRange = str.range(of: "By creating an account I agree to Chooch's")
        TermsLbl.addLink(to: URL(string: "Google"), with: range1)
        
        
        let str1 : NSString = "Already a member? Sign In"
        alreadyLbl.delegate = self
        alreadyLbl.linkAttributes = linkAttributes
        alreadyLbl.text = str1 as String
        let range2 : NSRange = str1.range(of: "Sign In")
        alreadyLbl.addLink(to: URL(string: ""), with: range2)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        choochLogo_imgView.addGestureRecognizer(tap)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer? = nil) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                self.popViewController()
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        
        if label == alreadyLbl
        {
            let loginVC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            pushViewController(loginVC)
        }
        else
        {
            if url == nil
            {
                let TermsVC = mainStoryBoard.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
            
                TermsVC.type = "Terms"
                pushViewController(TermsVC)
            }
            else
            {
                TermsTickBtn.isSelected = TermsTickBtn.isSelected ? false : true
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
    

    @IBAction func Register_Pressed(_ sender: UIButton) {
        
        if isReadyToLogin()
        {
            
            showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
            
            let ApiURL = "https://app.chooch.ai/app/sign_up"
            
            let ParamDict = ["first_name": "\(self.firstname_TF.text ?? "")",
                "last_name": "\(self.lastname_TF.text ?? "")",
                "email": "\(self.email_TF.text ?? "")",
                "password": "\(self.password_TF.text ?? "")",
                "confirm_password": "\(self.confPassword_TF.text ?? "")"] as NSDictionary
            
            AFNetworkingAPIClient.sharedInstance.postAPICall(url: ApiURL, parameters: ParamDict) { (APIResponce, Responce, error1) in
                
                self.RemoveLoader()
                
                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                {
                    let responceDict = Responce as! [String:Any]
                    
                    print(responceDict)
                    
                    if responceDict["success"] as! Int == 1
                    {
                        self.presentAlertWithTitle(title: "Please check your mail. We sent you a verification link.", message: "", options: "OK") { (Int) in
                            
                            self.popViewController()
                        }
                    }
                    else
                    {
                        self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK") { (Int) in
                            
                            self.popViewController()
                            
                        }
                    }
                    
                    
                }
                else
                {
                    self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (Index) in
                    })
                }
                
            }
        }
        
    }
    
    
    @IBAction func Terms_Pressed(_ sender: UIButton) {
        
        sender.isSelected = sender.isSelected ? false : true
        
    }
    
    //MARK:- ********************** VALIDATION **********************
    
    func isReadyToLogin()-> Bool {
        
        if  Validation.isStringEmpty(self.firstname_TF.text!) ||
            Validation.isStringEmpty(self.lastname_TF.text!) ||
            Validation.isStringEmpty(self.password_TF.text!) ||
            Validation.isStringEmpty(self.confPassword_TF.text!) {
            
            presentAlertWithTitle(title: "Please fill in all fields", message: "", options: "OK") { (Int) in
            }
            
        }
        else if !Validation.isValidEmail(emailString: self.email_TF.text!) {
            
            presentAlertWithTitle(title: "Please enter a valid email address", message: "", options: "OK") { (Int) in
            }
        }
        else if !Validation.isPasswordHasMinimum(self.password_TF.text!) {
            
            presentAlertWithTitle(title: "Your password must be at least 6 characters", message: "", options: "OK") { (Int) in
            }
        }
        else if !Validation.isPassWordMatch(pass: self.password_TF.text!, confPass: self.confPassword_TF.text!) {
            
            presentAlertWithTitle(title: "Passwords don’t match", message: "", options: "OK") { (Int) in
            }
        }
        else if !TermsTickBtn.isSelected {
            
            presentAlertWithTitle(title: "Please accept the terms", message: "", options: "OK") { (Int) in
            }
        }
        else{
            return true
        }
        
        return false
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
