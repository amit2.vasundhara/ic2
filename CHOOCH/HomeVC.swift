//
//  HomeVC.swift
//  colorpixelphoto
//
//  Created by Vasundhara Vision on 2/22/17.
//  Copyright © 2017 Vasundhara Vision. All rights reserved.
//

import UIKit

class HomeVC: UIViewController
{
    //MARK:- ********************** OUTLATE DECLARATION **********************

    @IBOutlet weak var img: UIImageView!
    
    //MARK:- ********************** VARIABLE DECLARATION **********************
    
    var animatedChoochArr = [UIImage(named: "00"),
    UIImage(named: "01"),
    UIImage(named: "02"),
    UIImage(named: "03"),
    UIImage(named: "04"),
    UIImage(named: "05"),
    UIImage(named: "06"),
    UIImage(named: "07"),
    UIImage(named: "08"),
    UIImage(named: "09"),
    UIImage(named: "010"),
    UIImage(named: "011"),
    UIImage(named: "012"),
    UIImage(named: "013"),
    UIImage(named: "014"),
    UIImage(named: "015"),
    UIImage(named: "016"),
    UIImage(named: "017"),
    UIImage(named: "018"),
    UIImage(named: "019"),
    UIImage(named: "020"),
    UIImage(named: "021"),
    UIImage(named: "022"),
    UIImage(named: "023"),
    UIImage(named: "024"),
    UIImage(named: "025"),
    UIImage(named: "026"),
    UIImage(named: "027"),
    UIImage(named: "028"),
    UIImage(named: "029"),
    UIImage(named: "030"),
    UIImage(named: "031"),
    UIImage(named: "032"),
    UIImage(named: "033"),
    UIImage(named: "034"),
    UIImage(named: "035"),
    UIImage(named: "036"),
    UIImage(named: "037"),
    UIImage(named: "038"),
    UIImage(named: "039"),
    UIImage(named: "040"),
    UIImage(named: "041"),
    UIImage(named: "042"),
    UIImage(named: "043"),
    UIImage(named: "044"),
    UIImage(named: "045"),
    UIImage(named: "046"),
    UIImage(named: "047"),
    UIImage(named: "048"),
    UIImage(named: "049"),
    UIImage(named: "050"),
    UIImage(named: "051"),
    UIImage(named: "052"),
    UIImage(named: "053"),
    UIImage(named: "054"),
    UIImage(named: "055"),
    UIImage(named: "056"),
    UIImage(named: "057"),
    UIImage(named: "058"),
    UIImage(named: "059"),
    UIImage(named: "060")] as! [UIImage]

    //MARK:- ********************** VIEW LIFECYLE **********************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        appDelegate.restrictRotation = .all
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        img.animationImages = animatedChoochArr
        img.animationDuration = 2.5
        img.animationRepeatCount = 1
        img.startAnimating()
        
        self.img.startAnimatingWithCompletionBlock {
            self.PasstoNextView()
        }
    }
    
    func PasstoNextView()
    {
        if userDefault.value(forKey: "FirstTime") == nil {
            
            let tutorialVC = mainStoryBoard.instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
            
            appDelegate.window?.rootViewController = tutorialVC
            appDelegate.window?.makeKeyAndVisible()
            
        }
        else
        {
            if userDefault.value(forKey: custUserame) == nil
            {
                let loginNC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginNavigationController") as! UINavigationController
                
                appDelegate.window?.rootViewController = loginNC
                appDelegate.window?.makeKeyAndVisible()
            }
            else
            {
                let vc = mainStoryBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                
                appDelegate.window?.rootViewController = vc
                appDelegate.window?.makeKeyAndVisible()
            }
            
        }
        
//        let InstVC = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
//        UIApplication.shared.delegate!.window!!.rootViewController = InstVC
//        UIApplication.shared.delegate!.window!!.makeKeyAndVisible()
        
    }
    
}


