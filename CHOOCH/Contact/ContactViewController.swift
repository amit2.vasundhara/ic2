//
//  ContactViewController.swift
//  CHOOCH
//
//  Created by des on 03/06/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class ContactViewController: BaseViewController , UITableViewDataSource , UITableViewDelegate {
    
    // MARK: - Outlate Declaration
    
    @IBOutlet var contact_TblVw: UITableView!
    
    // MARK: - variable Declaration
    
    var mainArr = NSMutableArray()
//    var userMessageArr = NSArray()
    var headerArr = ["WRITE TO US","MESSAGES"]
    
    // MARK: - view Declaration
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Messages"
        
        let backBTN = UIBarButtonItem(image: UIImage(named: "ic_back"),
                                      style: .plain,
                                      target: navigationController,
                                      action: #selector(UINavigationController.popViewController(animated:)))
        backBTN.tintColor = .black
        navigationItem.leftBarButtonItem = backBTN
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.callMessageAPI()
        
    }
    
    func callMessageAPI() {
        
        showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
        
        let ApiURL = "https://app.chooch.ai/app/messages"
        
        let userToken = "\(userDefault.value(forKey: custToken) ?? String())"
        
        AFNetworkingAPIClient.sharedInstance.getAPICallWithToken(url: ApiURL,
                                                                 token: userToken,
                                                                 parameters: nil,
                                                                 completionHandler: { (APIResponce, Responce, error1) in
                                                                    
                                                                    self.RemoveLoader()
                                                                    
                                                                    if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                                                                    {
                                                                        let responceDict = Responce as! [String:Any]
                                                                        
                                                                        print(responceDict)
                                                                        
                                                                        self.mainArr.removeAllObjects()
                                                                        
                                                                        if (responceDict["data"] as! NSArray).count > 0
                                                                        {
                                                                            self.mainArr.addObjects(from: responceDict["data"] as! [Any])
                                                                        }
                                                                        if (responceDict["user_message"] as! NSArray).count > 0
                                                                        {
                                                                            self.mainArr.addObjects(from: responceDict["user_message"] as! [Any])
                                                                        }
                                                                        self.contact_TblVw.reloadData()
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (Index) in
                                                                        })
                                                                    }
                                                                    
        })
    }
    
    
    // MARK: - Tableview methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerArr.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headerArr[section]
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return (mainArr.count <= 0) ? 0 : 48
        default:
            return 48
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let header = view as! UITableViewHeaderFooterView
        header.backgroundColor = hexStringToUIColor(hex: "EBEFF0")
        header.textLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: view.Getheight * 0.4)
        header.textLabel?.textColor = hexStringToUIColor(hex: "B0B1B2")
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return mainArr.count
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsTableViewCell") as! ContactUsTableViewCell
            
            cell.send_Btn.addTarget(self, action: #selector(Send_btnPressed(sender:)), for: .touchUpInside)
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sysMessTableViewCell") as! sysMessTableViewCell
            
            let dict = (mainArr.object(at: indexPath.row) as! [String:Any])
            
            cell.mess_Lbl.text = (dict["subject"] != nil) ? dict["subject"] as? String : dict["message"] as? String
            
            cell.status_imgView.backgroundColor = (dict["is_read"] as? Bool ?? false) ? hexStringToUIColor(hex: "0CCB0D") : hexStringToUIColor(hex: "B0B1B2")
            
            if (dict["answer"] as? String == "") { cell.status_imgView.backgroundColor = .yellow }
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section != 0
        {
            
            let sysmessVC = mainStoryBoard.instantiateViewController(withIdentifier: "SysMessViewcontroller") as! SysMessViewcontroller
            
            switch indexPath.section {
            case 1:
                let dict = (self.mainArr[indexPath.row] as! NSDictionary)
                sysmessVC.msgDict = dict
                sysmessVC.is_sysMsg = (dict["subject"] != nil) ? false : true
                
            default:
                print("HelloWorld")
            }
            
            self.pushViewController(sysmessVC)
            
        }
        
    }
    
    @objc func Send_btnPressed(sender : UIButton) {
        
        let cell = sender.superview!.superview as! ContactUsTableViewCell
        
        if Validation.isStringEmpty(cell.subject_TF.text!) {
            presentAlertWithTitle(title: "Please fill Subject", message: "", options: "OK") { (Int) in
            }
        }
        else if Validation.isStringEmpty(cell.message_TV.text!) {
            presentAlertWithTitle(title: "Please enter a message", message: "", options: "OK") { (Int) in
            }
        }
        else {
            showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
            
            let ApiURL = "https://app.chooch.ai/app/contact"
            
            let ParamDict = ["subject": cell.subject_TF.text ?? "",
                             "message": cell.message_TV.text ?? ""] as NSDictionary
            
            let userToken = "\(userDefault.value(forKey: custToken) ?? String())"
            
            AFNetworkingAPIClient.sharedInstance.postAPICallWithToken(url: ApiURL,
                                                                      parameters: ParamDict,
                                                                      token: userToken,
                                                                      completionHandler: { (APIResponce, Responce, error1) in
                                                                        
                                                                        self.RemoveLoader()
                                                                        
                                                                        if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                                                                        {
                                                                            let responceDict = Responce as! [String:Any]
                                                                            
                                                                            print(responceDict)
                                                                            
                                                                            self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK", completion: { (Index) in
                                                                                
                                                                                cell.message_TV.text = ""
                                                                                cell.subject_TF.text = ""
                                                                                 self.callMessageAPI()
                                                                            })
                                                                            
                                                                        }
                                                                        else
                                                                        {
                                                                            self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (Index) in
                                                                            })
                                                                        }
                                                                        
            })
        }
        
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}








// MARK: - ***********OLD**************


//import UIKit
//
//class ContactViewController: BaseViewController , UITableViewDataSource , UITableViewDelegate {
//
//    // MARK: - Outlate Declaration
//
//    @IBOutlet var contact_TblVw: UITableView!
//
//    // MARK: - variable Declaration
//
//    var mainArr = NSArray()
//    var userMessageArr = NSArray()
//    var headerArr = ["WRITE US","SYSTEM MESSAGES","USER MESSAGES"]
//
//    // MARK: - view Declaration
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        let backBTN = UIBarButtonItem(image: UIImage(named: "ic_back"),
//                                      style: .plain,
//                                      target: navigationController,
//                                      action: #selector(UINavigationController.popViewController(animated:)))
//        backBTN.tintColor = .black
//        navigationItem.leftBarButtonItem = backBTN
//        navigationController?.interactivePopGestureRecognizer?.delegate = self
//
//
//
//        // Do any additional setup after loading the view.
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//
//        showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
//
//        let ApiURL = "https://app.chooch.ai/app/messages"
//
//        let userToken = "\(userDefault.value(forKey: custToken) ?? String())"
//
//        AFNetworkingAPIClient.sharedInstance.getAPICallWithToken(url: ApiURL,
//                                                                 token: userToken,
//                                                                 parameters: nil,
//                                                                 completionHandler: { (APIResponce, Responce, error1) in
//
//                                                                    self.RemoveLoader()
//
//                                                                    if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
//                                                                    {
//                                                                        let responceDict = Responce as! [String:Any]
//
//                                                                        print(responceDict)
//
//
//                                                                        if (responceDict["data"] as! NSArray).count > 0
//                                                                        {
//                                                                            self.mainArr = responceDict["data"] as! NSArray
//                                                                        }
//                                                                        if (responceDict["user_message"] as! NSArray).count > 0
//                                                                        {
//                                                                            self.userMessageArr = responceDict["user_message"] as! NSArray
//                                                                        }
//                                                                        self.contact_TblVw.reloadData()
//
//                                                                    }
//                                                                    else
//                                                                    {
//                                                                        self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (Index) in
//                                                                        })
//                                                                    }
//
//        })
//    }
//
//
//    // MARK: - Tableview methods
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return headerArr.count
//    }
//
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return headerArr[section]
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        switch section {
//        case 1:
//            return (mainArr.count <= 0) ? 0 : 48
//        case 2:
//            return (userMessageArr.count <= 0) ? 0 : 48
//        default:
//            return 48
//        }
//    }
//
//    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
//
//        let header = view as! UITableViewHeaderFooterView
//        header.backgroundColor = hexStringToUIColor(hex: "EBEFF0")
//        header.textLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: view.Getheight * 0.4)
//        header.textLabel?.textColor = hexStringToUIColor(hex: "B0B1B2")
//
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch section {
//        case 1:
//            return mainArr.count
//        case 2:
//           return userMessageArr.count
//        default:
//           return 1
//        }
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        if indexPath.section == 0
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "ContactUsTableViewCell") as! ContactUsTableViewCell
//
//            cell.send_Btn.addTarget(self, action: #selector(Send_btnPressed(sender:)), for: .touchUpInside)
//
//            return cell
//        }
//        else if indexPath.section == 1
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "sysMessTableViewCell") as! sysMessTableViewCell
//
//            cell.mess_Lbl.text = (mainArr.object(at: indexPath.row) as! [String:Any])["message"] as? String
//
//            cell.status_imgView.backgroundColor = ((mainArr.object(at: indexPath.row) as! [String:Any])["is_read"] as? Bool ?? false) ? hexStringToUIColor(hex: "0CCB0D") : hexStringToUIColor(hex: "B0B1B2")
//
//            return cell
//        }
//        else
//        {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "sysMessTableViewCell") as! sysMessTableViewCell
//
//            cell.mess_Lbl.text = (userMessageArr.object(at: indexPath.row) as! [String:Any])["subject"] as? String
//
//            cell.status_imgView.backgroundColor = ((userMessageArr.object(at: indexPath.row) as! [String:Any])["is_read"] as? Bool ?? false) ? hexStringToUIColor(hex: "0CCB0D") : hexStringToUIColor(hex: "B0B1B2")
//
//            return cell
//        }
//
//
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        if indexPath.section != 0
//        {
//
//            let sysmessVC = mainStoryBoard.instantiateViewController(withIdentifier: "SysMessViewcontroller") as! SysMessViewcontroller
//
//            switch indexPath.section {
//            case 1:
//                sysmessVC.msgDict = (self.mainArr[indexPath.row] as! NSDictionary)
//                sysmessVC.is_sysMsg = true
//
//            case 2:
//                sysmessVC.msgDict = (self.userMessageArr[indexPath.row] as! NSDictionary)
//                sysmessVC.is_sysMsg = false
//
//            default:
//                print("HelloWorld")
//            }
//
//            self.pushViewController(sysmessVC)
//
//        }
//
//    }
//
//    @objc func Send_btnPressed(sender : UIButton) {
//
//        let cell = sender.superview!.superview as! ContactUsTableViewCell
//
//        if Validation.isStringEmpty(cell.subject_TF.text!) {
//            presentAlertWithTitle(title: "Please fill Subject", message: "", options: "OK") { (Int) in
//            }
//        }
//        else if Validation.isStringEmpty(cell.message_TV.text!) {
//            presentAlertWithTitle(title: "Please enter a message", message: "", options: "OK") { (Int) in
//            }
//        }
//        else {
//            showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
//
//            let ApiURL = "https://app.chooch.ai/app/contact"
//
//            let ParamDict = ["subject": cell.subject_TF.text ?? "",
//                             "message": cell.message_TV.text ?? ""] as NSDictionary
//
//            let userToken = "\(userDefault.value(forKey: custToken) ?? String())"
//
//            AFNetworkingAPIClient.sharedInstance.postAPICallWithToken(url: ApiURL,
//                                                                      parameters: ParamDict,
//                                                                      token: userToken,
//                                                                      completionHandler: { (APIResponce, Responce, error1) in
//
//                self.RemoveLoader()
//
//                if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
//                {
//                    let responceDict = Responce as! [String:Any]
//
//                    print(responceDict)
//
//                    self.presentAlertWithTitle(title: "\(responceDict["message"] ?? someErrorMSG)", message: "", options: "OK", completion: { (Index) in
//                    })
//
//                }
//                else
//                {
//                    self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (Index) in
//                    })
//                }
//
//            })
//        }
//
//
//    }
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
