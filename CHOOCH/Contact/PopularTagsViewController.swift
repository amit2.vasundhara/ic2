//
//  PopularTagsViewController.swift
//  CHOOCH
//
//  Created by des on 03/06/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class PopularTagsViewController: BaseViewController , UITableViewDelegate , UITableViewDataSource {
   
    // MARK: - Outlate Declaration
    
    @IBOutlet var popTableView: UITableView!
    
    // MARK: - variable Declaration
    
    var mainArr = NSArray()
//    var mainArr = ["Hello World"]
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Popular tags"
        
        // Do any additional setup after loading the view.
        let backBTN = UIBarButtonItem(image: UIImage(named: "ic_back"),
                                      style: .plain,
                                      target: navigationController,
                                      action: #selector(UINavigationController.popViewController(animated:)))
        backBTN.tintColor = .black
        navigationItem.leftBarButtonItem = backBTN
        navigationController?.interactivePopGestureRecognizer?.delegate = self
       
        self.popTableView.isHidden = true

        showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)

        let ApiURL = "https://app.chooch.ai/app/popular-tags"

        let userToken = "\(userDefault.value(forKey: custToken) ?? String())"

        AFNetworkingAPIClient.sharedInstance.getAPICallWithToken(url: ApiURL,
                                                                 token: userToken,
                                                                 parameters: nil,
                                                                 completionHandler: { (APIResponce, Responce, error1) in

                                                                    self.RemoveLoader()

                                                                    if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                                                                    {
                                                                        let responceDict = Responce as! [String:Any]

                                                                        print(responceDict)


                                                                        if (responceDict["data"] as! NSArray).count > 0
                                                                        {
                                                                            self.mainArr = responceDict["data"] as? NSArray ?? NSArray()
                                                                            self.popTableView.reloadData()
                                                                            self.nodataLbl.isHidden = true
                                                                            self.popTableView.isHidden = false
                                                                        }
                                                                        else
                                                                        {
                                                                            self.nodataLbl.isHidden = false
                                                                            self.popTableView.isHidden = true
                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (Index) in
                                                                        })
                                                                    }

        })

        
    }
    
    
    // MARK: - Tableview methods
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "YOUR MOST POPULAR \(mainArr.count) TAGS"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 52
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let header = view as! UITableViewHeaderFooterView
        header.backgroundColor = hexStringToUIColor(hex: "EBEFF0")
        header.textLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: view.Getheight * 0.4)
        header.textLabel?.textColor = hexStringToUIColor(hex: "B0B1B2")
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "popularTagsTableViewCell") as! popularTagsTableViewCell
        
        let dict = mainArr[indexPath.row] as? [String : Any]
        cell.titleLbl.text = "\(dict?["class_value"] ?? "Default")"
        cell.countLbl.text = "\(dict?["prediction_count"] ?? "1")"
        
//        cell.titleLbl.text = mainArr[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("sfdsgdsfgxcvds")
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
