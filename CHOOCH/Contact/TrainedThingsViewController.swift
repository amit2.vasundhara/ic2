//
//  TrainedThingsViewController.swift
//  CHOOCH
//
//  Created by Mac Os on 31/07/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController
import SDWebImage

class TrainedThingsViewController: BaseViewController {
    
    // MARK: - Outlate Declaration
    
    @IBOutlet weak var ThingsScrollView: UIScrollView!
    @IBOutlet weak var ViewforScroll: UIView!
    
    // MARK: - variable Declaration
    
    var mainArr : NSArray!
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Popular Tags"
    
        let backBTN = UIBarButtonItem(image: UIImage(named: "ic_back"),
                                      style: .plain,
                                      target: navigationController,
                                      action: #selector(UINavigationController.popViewController(animated:)))
        backBTN.tintColor = .black
        navigationItem.leftBarButtonItem = backBTN
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        self.ThingsScrollView.isHidden = true
        
        showLoaderwithMessage(message: "Please wait...", type: .ballSpinFadeLoader, color: .white)
        
        let ApiURL = "https://app.chooch.ai/app/things-you-trained"
        
        let userToken = "\(userDefault.value(forKey: custToken) ?? String())"
        
        AFNetworkingAPIClient.sharedInstance.getAPICallWithToken(url: ApiURL,
                                                                 token: userToken,
                                                                 parameters: nil,
                                                                 completionHandler: { (APIResponce, Responce, error1) in
                                                                    
                                                                    self.RemoveLoader()
                                                                    
                                                                    if (APIResponce.response as? HTTPURLResponse)?.statusCode == 200 && error1 == nil
                                                                    {
                                                                        let responceDict = Responce as! [String:Any]
                                                                        
                                                                        print(responceDict)
                                                                        
                                                                        if (responceDict["data"] as! NSArray).count > 0
                                                                        {
                                                                            self.mainArr = responceDict["data"] as? NSArray
                                                                            self.setScrollView()
                                                                            self.nodataLbl.isHidden = true
                                                                            self.ThingsScrollView.isHidden = false
                                                                        }
                                                                        else
                                                                        {
                                                                            self.nodataLbl.isHidden = false
                                                                            self.ThingsScrollView.isHidden = true
                                                                        }
                                                                        
                                                                    }
                                                                    else
                                                                    {
                                                                        self.presentAlertWithTitle(title: someErrorMSG, message: "", options: "OK", completion: { (Index) in
                                                                        })
                                                                    }
                                                                    
        })
        
        
    }
    
    
    func setScrollView() {
        
        var BtnY : CGFloat = 70.0
        var Btnx : CGFloat = 15.0
        
        for i in ViewforScroll.subviews
        {
            i.removeFromSuperview()
        }

        if mainArr.count != 0 {
            
            let Predcount = mainArr.count
            
            let Hederlbl = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: ThingsScrollView.Getwidth, height: 60.0))
            Hederlbl.backgroundColor = hexStringToUIColor(hex: "EBEFF0")
            Hederlbl.textAlignment = .left
            Hederlbl.font = UIFont(name: CustomFontWeight.bold, size: Hederlbl.Getheight * 0.4)
            Hederlbl.textColor = hexStringToUIColor(hex: "B0B1B2")
            Hederlbl.text = "   THINGS YOU'VE TRAINED"
            ThingsScrollView.addSubview(Hederlbl)
            
            for predic in 0..<Predcount {
                
                let dict = mainArr[predic] as! NSDictionary
                
                if (dict.object(forKey: "labeltitle") as? String) != "" {
                    
                    let classValue = dict.value(forKey: "labeltitle") as? String
                    
                    let array = classValue?.components(separatedBy: "-")
                    
                    if array!.count > 1
                    {
                        var strTitle = String()
                        
                        for j in 1..<array!.count
                        {
                            strTitle.append(contentsOf: array?[j] ?? "")
                            strTitle.append("-")
                        }
                        
                        strTitle = strTitle.chopSuffix(1)
                        
                        var widthStr : CGFloat = 0.0
                        let font = UIFont.init(name: CustomFontWeight.bold, size: 15)!
                        
                        if strTitle.last == " " {
                            strTitle = strTitle.chopSuffix(1)
                        }
                        if strTitle.first == " " {
                            strTitle = strTitle.chopPrefix(1)
                        }
                        
                        widthStr = strTitle.width(withConstrainedHeight: 46.0, font: font)
                        
                        if widthStr < 100.0
                        {
                            widthStr = 100
                        }
                        
                        if ((Btnx + widthStr + 16.0) > (ThingsScrollView.Getwidth - 10.0)) {
                            Btnx = 15.0
                            BtnY += 53.0
                        }
                        
                        let button = MyButton.init(frame: CGRect.init(x: Btnx, y: BtnY, width: widthStr + 16.0, height: 46.0))
                        button.backgroundColor = hexStringToUIColor(hex: "C70221")
                        button.params = dict
                        button.setTitle(strTitle, for: .normal)
                        button.titleLabel?.textAlignment = .center
                        button.titleLabel?.textColor = .white
                        button.titleLabel?.font = font
                        button.contentEdgeInsets = UIEdgeInsetsMake(8, 8, 8, 8)
                        button.layer.cornerRadius = 23.0
                        
                        button.addTarget(self, action: #selector(self.openPopup(_:)), for: .touchUpInside)
                        
                        //                                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPress(gesture:)))
                        //                                longPress.minimumPressDuration = 1.0
                        //                                button.addGestureRecognizer(longPress)
                        
                        ViewforScroll.addSubview(button)
                        
                        Btnx += button.Getwidth + 10.0
                        
                        button.transform =  CGAffineTransform(scaleX: 0.0, y: 0.0)
                        
                        UIView.animate(withDuration: 1.0,
                                       delay: 0,
                                       usingSpringWithDamping: 0.5,
                                       initialSpringVelocity: 1.0,
                                       options: .allowUserInteraction,
                                       animations: { [weak self] in
                                        
                                        button.transform = .identity
                            },
                                       completion: nil)
                        
                    }
                    
                    
                }
                
                
                
            }
            
            
        }
        
        ViewforScroll.frame = CGRect(x: 0.0, y: 0.0, width: ThingsScrollView.Getwidth, height: CGFloat(BtnY + 53.0))
        
        ThingsScrollView.contentSize = ViewforScroll.Getsize
        
    }
    
    
    @objc func openPopup(_ sender: MyButton) {
        
        //        print(sender.params)
        
        if let imageUrl = sender.params!["image_url"] as? String
        {
            
            let viewforPopup = UIView()
            
            viewforPopup.backgroundColor = .clear
            viewforPopup.tag = 558
            
            let removegest = UITapGestureRecognizer(target: self, action: #selector(removetapped(gesture:)))
            removegest.delegate = self
            viewforPopup.addGestureRecognizer(removegest)
            
            let newImageView = UIImageView()
            
            
            if UIDevice.current.orientation.isLandscape
            {
                viewforPopup.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getheight * 0.8, height: self.view.Getheight * 0.8)
                newImageView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getheight * 0.8, height: self.view.Getheight * 0.8)
            }
            else
            {
                viewforPopup.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getwidth * 0.8, height: self.view.Getwidth * 0.8)
                newImageView.frame = CGRect(x: 0.0, y: 0.0, width: self.view.Getwidth * 0.8, height: self.view.Getwidth * 0.8)
            }
            
            newImageView.backgroundColor = .white
            newImageView.layer.cornerRadius = 15.0
            newImageView.layer.borderColor = UIColor.white.cgColor
            newImageView.layer.borderWidth = 1.0
            newImageView.clipsToBounds = true
            newImageView.isUserInteractionEnabled = true
            newImageView.tag = 525
            newImageView.contentMode = .scaleAspectFit
            viewforPopup.addSubview(newImageView)
            
            let TFPopup = MZFormSheetPresentationViewController.init(contentView: viewforPopup)
            //        TFPopup.presentationController?.contentViewSize = CGSize(width: self.view.Getwidth*0.8, height: self.view.Getwidth*0.8 + 50)
            TFPopup.presentationController?.shouldCenterVertically = true
            TFPopup.presentationController?.shouldDismissOnBackgroundViewTap = true
            TFPopup.contentViewControllerTransitionStyle = .bounce
            TFPopup.presentationController?.backgroundColor = UIColor.black.withAlphaComponent(0.7)
            //            TFPopup.didDismissContentViewControllerHandler = {(view) in
            //                self.scroll1.isHidden = false
            //            }
            
            self.present(TFPopup, animated: true)
            
            newImageView.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            newImageView.sd_setImage(with: URL(string: imageUrl)) { (image, error, cacheType, urls) in
                
                newImageView.backgroundColor = .clear
                
                if let image = newImageView.image {
                    let ratio = image.size.width / image.size.height
                    if image.size.width > image.size.height {
                        let newHeight = newImageView.frame.width / ratio
                        newImageView.frame.size = CGSize(width: newImageView.frame.width, height: newHeight)
                    }
                    else{
                        let newWidth = newImageView.frame.height * ratio
                        newImageView.frame.size = CGSize(width: newWidth, height: newImageView.frame.height)
                    }
                }
                
                newImageView.center = viewforPopup.center
                newImageView.Gety = 0.0
                
                viewforPopup.Getheight = newImageView.Getbottom
                
            }
        }
        
        
        
    }
    
    @objc func removetapped(gesture: UITapGestureRecognizer) {
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
