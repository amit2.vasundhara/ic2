//
//  ProfileViewController.swift
//  CHOOCH
//
//  Created by des on 03/06/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit
import FacebookCore
import GoogleSignIn

class ProfileViewController: BaseViewController , UITableViewDelegate , UITableViewDataSource {

    // MARK: - Outlate Declaration
    
    @IBOutlet var ProfileTableView: UITableView!
    
    // MARK: - variable Declaration
    
    var mainArr = [["Your popular tags","Things you've trained"],["Messages","Privacy Policy","Logout"]]
    var headerArr = ["GENERAL","SUPPORT"]
    
    
// MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Profile"
        
        // Do any additional setup after loading the view.
        let backBTN = UIBarButtonItem(image: UIImage(named: "ic_back"),
                                      style: .plain,
                                      target: self,
                                      action: #selector(Menu_Pressed(_:)))
        backBTN.tintColor = .black
        navigationItem.leftBarButtonItem = backBTN
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        appDelegate.restrictRotation = .portrait
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
        // Do any additional setup after loading the view.
    }
    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            print("Screen edge swiped!")
            let btn = UIButton()
            self.Menu_Pressed(btn)
        }
    }
  
    
// MARK: - Tableview methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return mainArr.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headerArr[section]
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainArr[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 && indexPath.section == 0
        {
            return 0
        }
        return 48
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let header = view as! UITableViewHeaderFooterView
        header.backgroundColor = hexStringToUIColor(hex: "EBEFF0")
        header.textLabel?.font = UIFont.init(name: CustomFontWeight.bold, size: view.Getheight * 0.4)
        header.textLabel?.textColor = hexStringToUIColor(hex: "B0B1B2")
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as! ProfileTableViewCell
        
        cell.titleLbl.text = (mainArr[indexPath.section])[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 && indexPath.row == 0
        {
            let popularTagsVC = mainStoryBoard.instantiateViewController(withIdentifier: "PopularTagsViewController") as! PopularTagsViewController
            self.pushViewController(popularTagsVC)
        }
        else if indexPath.section == 0 && indexPath.row == 1
        {
            let trainedTagsVC = mainStoryBoard.instantiateViewController(withIdentifier: "TrainedThingsViewController") as! TrainedThingsViewController
            self.pushViewController(trainedTagsVC)
        }
        else if indexPath.section == 1 && indexPath.row == 0
        {
            let contactUsVC = mainStoryBoard.instantiateViewController(withIdentifier: "ContactViewController") as! ContactViewController
            self.pushViewController(contactUsVC)
        }
        else if indexPath.section == 1 && indexPath.row == 1
        {
             let TermsVC = mainStoryBoard.instantiateViewController(withIdentifier: "TermsViewController") as! TermsViewController
             TermsVC.type = "Privacy"
             pushViewController(TermsVC)
        }
        else if indexPath.section == 1 && indexPath.row == 2
        {
                self.presentAlertWithTitle(title: "Do you want to log out?", message: "", options: "Yes","No", completion: { (Index) in
        
                    if Index == 0
                    {
                        userDefault.removeObject(forKey: custToken)
                        userDefault.removeObject(forKey: custUserame)
                        userDefault.removeObject(forKey: custId)
        
                        let manager = LoginManager()
                        manager.logOut()
        
                        GIDSignIn.sharedInstance()?.signOut()
        
                        let loginNC = mainStoryBoard.instantiateViewController(withIdentifier: "LoginNavigationController") as! UINavigationController
        
                        window?.rootViewController = loginNC
                        window?.makeKeyAndVisible()
        
                    }
        
                })
        }
        
        
        
        
    }
    
    
    @IBAction func Gallary_Pressed(_ sender: UIButton) {
        
        
        startSession = false
        self.dismiss(animated: true, completion: nil)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
        
    }
    
    @IBAction func Menu_Pressed(_ sender: UIButton) {
        
        
        startSession = false
        self.dismiss(animated: true, completion: nil)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReturnImagePicker"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ChangeOrientation"), object: nil)
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
