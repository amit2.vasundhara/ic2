//
//  SysMessViewcontroller.swift
//  CHOOCH
//
//  Created by des on 14/06/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class SysMessViewcontroller: BaseViewController , UITableViewDelegate , UITableViewDataSource {
    
    // MARK: - Outlate Declaration
    
    @IBOutlet var message_TV: UITableView!
    
    // MARK: - variable Declaration
    
    var mainArr : NSArray!
    var msgDict : NSDictionary!
    var is_sysMsg : Bool!
    
    let myAttribute = [ NSAttributedString.Key.font: UIFont.init(name: CustomFontWeight.reguler, size: 18)!]
    let myAttributeBold = [ NSAttributedString.Key.font: UIFont.init(name: CustomFontWeight.bold, size: 18)!,
                            NSAttributedStringKey.underlineStyle: NSUnderlineStyle.styleSingle.rawValue] as [NSAttributedStringKey : Any]
    
    // MARK: - view Declaration
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "System Messages"
        
        let backBTN = UIBarButtonItem(image: UIImage(named: "ic_back"),
                                      style: .plain,
                                      target: navigationController,
                                      action: #selector(UINavigationController.popViewController(animated:)))
        backBTN.tintColor = .black
        navigationItem.leftBarButtonItem = backBTN
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        if (msgDict["is_read"] as? Bool) == false
        {
            call_Api_isRead(isSystmMsg: is_sysMsg)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func call_Api_isRead(isSystmMsg : Bool) {
    
        let ParamDict = ["is_read": true] as NSDictionary

        let userToken = "\(userDefault.value(forKey: custToken) ?? String())"
        let id = "\(msgDict["id"] ?? String())"
        
        let ApiURL = "https://app.chooch.ai/app/\(isSystmMsg ? "update-messages" : "update-contact")/\(id)"
        
        AFNetworkingAPIClient.sharedInstance.patchAPICallWithToken(url: ApiURL,
                                                                  parameters: ParamDict,
                                                                  token: userToken,
                                                                  completionHandler: { (APIResponce, Responce, error1) in

        })
    }
    
    // MARK: - Tableview Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return is_sysMsg ? 1 : 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell") as! ProfileTableViewCell
        
        if !is_sysMsg
        {
            switch indexPath.row {
            case 0:
                let myattrString2 = NSMutableAttributedString(string: "\nHello, we got your ", attributes: self.myAttribute)
                myattrString2.append(NSMutableAttributedString(string: "''\(msgDict["subject"] ?? "")''", attributes: self.myAttributeBold))
                myattrString2.append(NSMutableAttributedString(string: " subjected message.\n\nWe are inspecting this problem. We will solve this as soon as possible.\n\n\nBest Regards,\nChooch Team\n", attributes: self.myAttribute))
                
                cell.titleLbl.attributedText = myattrString2
            case 1:
                let myattrString2 = NSMutableAttributedString(string: "\nReply : ", attributes: self.myAttribute)
                
                let replystr = (msgDict["answer"] as? String != "") ? (msgDict["answer"] as! String) : "Pending"
                
                myattrString2.append(NSMutableAttributedString(string: "\(replystr)\n", attributes: self.myAttributeBold))
                
                cell.titleLbl.attributedText = myattrString2
            default:
                print("Hello")
            }
            
        }else
        {
            cell.titleLbl.text = "\n\(msgDict["message"] ?? "")\n"
        }
        
        return cell
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}


class VerticallyCenteredTextView: UITextView {
    override var contentSize: CGSize {
        didSet {
            var topCorrection = (bounds.size.height - contentSize.height * zoomScale) / 2.0
            topCorrection = max(0, topCorrection)
            contentInset = UIEdgeInsets(top: topCorrection, left: 0, bottom: 0, right: 0)
        }
    }
}
