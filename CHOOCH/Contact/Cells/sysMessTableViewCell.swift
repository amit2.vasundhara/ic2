//
//  sysMessTableViewCell.swift
//  CHOOCH
//
//  Created by des on 05/06/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class sysMessTableViewCell: UITableViewCell {

    @IBOutlet var status_imgView: UIImageView!
    @IBOutlet var mess_Lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
