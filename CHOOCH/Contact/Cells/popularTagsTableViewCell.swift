//
//  popularTagsTableViewCell.swift
//  CHOOCH
//
//  Created by des on 03/06/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class popularTagsTableViewCell: UITableViewCell {

    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var countLbl: UILabel!
    @IBOutlet var eyeBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
