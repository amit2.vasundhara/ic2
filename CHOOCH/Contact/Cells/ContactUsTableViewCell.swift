//
//  ContactUsTableViewCell.swift
//  CHOOCH
//
//  Created by des on 05/06/19.
//  Copyright © 2019 Vasundhara Vision. All rights reserved.
//

import UIKit

class ContactUsTableViewCell: UITableViewCell {

    @IBOutlet var subject_TF: UITextField!
    
    @IBOutlet var message_TV: UITextView!
    
    @IBOutlet var send_Btn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
